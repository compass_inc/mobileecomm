﻿using System;
using System.Collections.Generic;
using Domain.Enums;
using Domain.Interfaces;

namespace Domain
{
    public class Product : BaseObject, IFieldsContainer<Product>
    {
        public virtual ProductType Type { get; set; }
        public virtual string Name { get; set; }
        public virtual string ShortDescription { get; set; }
        public virtual string LongDescription { get; set; }
        public virtual ulong Rate { get; set; }
        public virtual DateTime? EndActivityDate { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<ShopOrderLine> ShopOrderLines { get; set; }
        public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Place> Places { get; set; }
        public virtual ICollection<Customer> FavoritesCustomers { get; set; }
        public virtual ICollection<Product> Featured { get; set; }
        public virtual ICollection<Product> FeaturedOwners { get; set; }
        public virtual ICollection<Banner> Labels { get; set; }
        public virtual ICollection<Coupon> Coupons { get; set; }

        public virtual ICollection<ProductToCategoryOrder> CategoriesOrders { get; set; }

        public virtual ICollection<ObjectField<Product>> Fields { get; set; }

        public virtual ProductState Status { get; set; }

        public Product()
        {
            Prices = new List<Price>();
            CategoriesOrders = new List<ProductToCategoryOrder>();
        }


    }
}