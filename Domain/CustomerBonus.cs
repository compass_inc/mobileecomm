﻿namespace Domain
{
    public class CustomerBonus : BaseObject
    {
        public virtual Customer Customer { get; set; }
        public virtual BonusProgram Program { get; set; }
        public virtual decimal Sum { get; set; }
    }
}