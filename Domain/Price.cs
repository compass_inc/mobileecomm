﻿using System.Collections.Generic;

namespace Domain
{
    public class Price: BaseObject
    {
        public virtual Product Product { get; set; }
        public virtual ICollection<ShopOrderLine> ShopOrderLines { get; set; } 
        public virtual decimal Value { get; set; }
        public virtual decimal Value2 { get; set; }
        public virtual decimal ValueCalc { get; set; }
        public virtual string Description { get; set; }
        public override bool Equals(object obj)
        {
            if (!(obj is Price))
                return base.Equals(obj);

            return (obj as Price).Id == Id;
        }

        public override int GetHashCode()
        {
            return (int)Id;
        }
    }
}
