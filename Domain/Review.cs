﻿using System;
using System.Collections.Generic;
using Domain.Enums;

namespace Domain
{
    public class Review : BaseObject
    {
        public virtual DateTime CreationDate { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
        public virtual Place Place { get; set; }
        public virtual string CustomerName { get; set; }
        public virtual string CustomerEMail { get; set; }
        public virtual ulong Rate { get; set; }
        public virtual bool IsOwnerAnswer { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }

        public virtual ReviewState Status { get; set; }

        public virtual string DisplayName
        {
            get { return Customer != null ? Customer.Name : CustomerName; }
            set { CustomerName = value; }
        }

        public virtual string DisplayEMail
        {
            get { return Customer != null ? Customer.Login : CustomerEMail; }
            set { CustomerEMail = value; }
        }

        public virtual Review Parent { get; set; }
        public virtual ICollection<Review> Childs { get; set; } 
    }
}