﻿namespace Domain
{
    public class LocaleStringValue : BaseObject
    {
        public virtual LocaleString LocaleString { get; set; }
        public virtual string Locale { get; set; }
        public virtual string Value { get; set; }
    }
}