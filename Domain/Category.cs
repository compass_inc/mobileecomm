﻿using System.Collections.Generic;

namespace Domain
{
    public class Category : BaseObject
    {
        public virtual string Name { get; set; }
        public virtual Image Image { get; set; }
        public virtual Category Parent { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ProductToCategoryOrder> ProdToCatOrders { get; set; }

        public virtual ICollection<CategoryColor> Colors { get; set; }

        public Category()
        {
            Products = new List<Product>();
        }
    }
}