﻿namespace Domain
{
    public class ObjectField<T> : BaseObject where T : BaseObject
    {
        public virtual ObjectFieldType<T> FieldType { get; set; }
        public virtual string Value { get; set; }
    }
}