﻿using System.Collections.Generic;

namespace Domain
{
	public class CustomObject: BaseObject
	{
		public virtual CustomObjectType Type { get; set; }
		public virtual ICollection<ObjectField<CustomObject>> Fields { get; set; }
	}
}
