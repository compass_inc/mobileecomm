﻿using System;
using System.Collections.Generic;
using Domain.Enums;

namespace Domain
{
    public interface IObjectFieldType
    {
        ulong Id { get; set; }
        ulong SortOrder { get; set; }
        string Name { get; set; }
        FieldType TypeName { get; set; }
        string CultureSpecTypeName { get; set; }
        Type Type { get; set; }
        bool IsRequired { get; set; }
    }

    public class ObjectFieldType<T> : BaseObject, IObjectFieldType where T : BaseObject
    {
        public virtual string Name { get; set; }
        
        public virtual FieldType TypeName { get; set; }

        public virtual string CultureSpecTypeName
        {
            get { return DescribedEnum<FieldType>.GetDescription(TypeName); }
            set { TypeName = DescribedEnum<FieldType>.GetValue(value); }
        }

        public virtual Type Type
        {
            get { return Type.GetType(TypeName.ToString()) ?? Type.GetType("System." + TypeName); }
            set
            {
                if (value != null)
                {
                    FieldType res;
                    Enum.TryParse(value.Name, out res);
                    TypeName = res;
                }
            }
        }

        public virtual ICollection<ObjectField<T>> Fields { get; set; }

        public virtual bool IsRequired { get; set; }
    }

}