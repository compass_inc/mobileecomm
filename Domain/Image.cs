﻿using System.Collections.Generic;

namespace Domain
{
    public class Image : BaseObject
    {
        public virtual string Description { get; set; }
        public virtual string Path { get; set; }
        public virtual string SavePath { get; set; }
        public virtual ImageType Type { get; set; }
        public virtual ICollection<Banner> Banners { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Product> Products { get; set; } 
    }
}