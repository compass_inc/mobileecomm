﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Place : BaseObject
    {
        public virtual string City { get; set; }
        public virtual string Street { get; set; }
        public virtual string House { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }

        public virtual Image HeaderLogo { get; set; }
        public virtual Image MainLogo { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual string Name { get; set; }

        public virtual string AddressName
        {
            get { return City + " " + Street + " " + House; }
        }

        public virtual DateTime DeliveryStartTime { get; set; }
        public virtual DateTime DeliveryEndTime { get; set; }
        public virtual decimal MinOrderSum { get; set; }
        public virtual decimal DeliveryFreeSum { get; set; }
        public virtual DateTime WorkStartTime { get; set; }
        public virtual DateTime WorkEndTime { get; set; }
        public virtual decimal MinDeliverySum { get; set; }
        public virtual int MinDeliveryTime { get; set; }

        public virtual string Coordinates { get; set; }
    }
}