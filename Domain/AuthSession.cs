﻿using System;

namespace Domain
{
    public class AuthSession
    {
        public virtual ulong Id { get; set; }
        public virtual Guid Token { get; set; }
        public virtual Customer Customer { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is AuthSession))
                return false;
            if ((obj as AuthSession).Customer == null || Customer == null)
                return false;

            return Customer.Id == (obj as AuthSession).Customer.Id;
        }

        public override int GetHashCode()
        {
            if (Customer == null)
                return 0;
            return (int)Customer.Id;
        }
    }
}