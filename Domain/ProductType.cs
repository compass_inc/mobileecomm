﻿namespace Domain
{
    public class ProductType : BaseObject
    {
        public virtual string Name { get; set; }
    }
}