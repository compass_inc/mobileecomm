﻿using System.Collections.Generic;

namespace Domain
{
    public class Settings : BaseObject
    {
        public virtual string ShopOrderEmail { get; set; }
        public virtual string HeaderFooterColor { get; set; }
        public virtual string BackGroundColor { get; set; }
        public virtual string FreeDeliveryPercentNotify { get; set; }
        public virtual string ShopName { get; set; }
        public virtual string BgFontColor { get; set; }
        public virtual string HfFontColor { get; set; }
        public virtual Image LogoImage { get; set; }
        public virtual Image ReviewDefaultImage { get; set; }
        public virtual ICollection<CategoryColor> CategoriesColors { get; set; }
        public virtual bool UseGravatar { get; set; }
        public virtual string FromEmail { get; set; }
        public virtual bool NoComments { get; set; }
        public virtual bool NoOrderMode { get; set; }
        public virtual bool OnlyCity { get; set; }
        public virtual bool ShowFilialInfoInProduct { get; set; }
        public virtual bool AuthRequired { get; set; }
        public virtual bool OnePageCatalog { get; set; }
        public virtual bool SideMainMenu { get; set; }
        public virtual string StartPageBannerCode { get; set; }
    }

    public class CategoryColor : BaseObject
    {
        public virtual Category Category { get; set; }
        public virtual string HeaderFooterColor { get; set; }
        public virtual string BackGroundColor { get; set; }
        public virtual string BgFontColor { get; set; }
        public virtual string HfFontColor { get; set; }
    }
}