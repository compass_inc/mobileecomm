﻿namespace Domain.Enums
{
    public enum ProductState
    {
        Active,
        Archived
    }
}