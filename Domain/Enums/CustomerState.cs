﻿namespace Domain.Enums
{
    public enum CustomerState
    {
        Active,
        Blocked
    }
}