﻿namespace Domain.Enums
{
    public enum UserState
    {
        Active,
        Blocked
    }
}