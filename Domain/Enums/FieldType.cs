﻿namespace Domain.Enums
{
    
    public enum FieldType
    {
        String,
        Html,
        Int32,
        Decimal,
        DateTime
    }
}