﻿namespace Domain.Enums
{
    public enum CouponState
    {
        Active,
        Closed
    }
}