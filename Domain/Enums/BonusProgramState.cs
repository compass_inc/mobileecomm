﻿namespace Domain.Enums
{
    public enum BonusProgramState
    {
        Active,
        Blocked
    }
}