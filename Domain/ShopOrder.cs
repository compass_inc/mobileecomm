﻿using System;
using System.Collections.Generic;
using Domain.Interfaces;

namespace Domain
{
    public class ShopOrder : BaseObject, IFieldsContainer<ShopOrder>
    {
        public virtual DateTime CreationDate { get; set; }

        public virtual string CustomerName { get; set; }
        public virtual string CustomerMail { get; set; }
        public virtual string CustomerPhone { get; set; }
        public virtual string CustomerAddress { get; set; }
        public virtual Customer Customer { get; set; }

        public virtual DateTime? DeliveryTime { get; set; }

        public virtual bool IsPickup { get; set; }

        public virtual decimal OrderSum { get; set; }

        public virtual BonusProgram BonusProgram { get; set; }

        public virtual decimal WriteOffBonusSum { get; set; }

        public virtual decimal ResultSum
        {
            get { return OrderSum - WriteOffBonusSum; }
        }

        public virtual ShopOrderState Status { get; set; }

        public virtual Coupon Coupon { get; set; }

        public virtual ICollection<ShopOrderLine> Lines { get; set; }

        public virtual ICollection<ObjectField<ShopOrder>> Fields { get; set; }
    }
}