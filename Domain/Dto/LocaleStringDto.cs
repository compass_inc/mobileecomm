﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class LocaleStringDto : BaseObject
    {
        public string Name { get; set; }
        public ulong TypeId { get; set; }
        public string TypeName { get; set; }
        public string TypeCode { get; set; }
        public ICollection<LocaleStringValueDto> Values { get; set; }
    }
}