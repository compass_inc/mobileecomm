﻿namespace Domain.Dto
{
    public class PlaceDto : BaseObject
    {
        public string City { get; set; }
        public string CityTranslit { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string AddressName { get; set; }
        
        public ulong HeaderLogoId { get; set; }
        public string HeaderLogoPath { get; set; }

        public ulong MainLogoId { get; set; }
        public string MainLogoPath { get; set; }

        public string DeliveryStartTime { get; set; }
        public string DeliveryEndTime { get; set; }
        public decimal MinOrderSum { get; set; }
        public decimal DeliveryFreeSum { get; set; }
        public string WorkStartTime { get; set; }
        public string WorkEndTime { get; set; }
        public decimal MinDeliverySum { get; set; }
        public int MinDeliveryTime { get; set; }

        public string Coordinates { get; set; }
    }
}