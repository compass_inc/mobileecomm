﻿namespace Domain.Dto
{
    public class PriceDto : BaseObject
    {
        public decimal Value { get; set; }
        public decimal Value2 { get; set; }
        public string Description { get; set; }
        public ulong ProductId { get; set; }
    }
}