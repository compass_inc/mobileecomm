﻿namespace Domain.Dto
{
    public class CategoryDto : BaseObject
    {
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public ulong ImageId { get; set; }
        public ulong? ParentId { get; set; }
    }
}