﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class CustomerDto : BaseObject
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public ICollection<CustomerAddressDto> Addresses { get; set; }
        public string Status { get; set; }
        public ulong AvatarImageId { get; set; }
        public string AvatarImagePath { get; set; }
        public ICollection<ulong> FavoritesIds { get; set; }

        public IDictionary<string, decimal> BonusSums { get; set; }
    }

    public class CustomerAddressDto : BaseObject
    {
        public string Description { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string FrontDoor { get; set; }
    }
}