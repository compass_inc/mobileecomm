﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class UserDto : BaseObject
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public virtual ICollection<RoleDto> Roles { get; set; }
    }
}