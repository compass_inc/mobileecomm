﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class ProductDto : BaseObject
    {
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }

        public string EndActivityDate { get; set; }

        public ICollection<CategoryDto> Categories { get; set; }
        public ICollection<BannerDto> Labels { get; set; }
        public ICollection<ImageDto> Images { get; set; } 
        public IDictionary<ulong,string> Places { get; set; }
        public IDictionary<ulong,string> Featured { get; set; }
        public ICollection<PriceDto> Prices { get; set; }

        public ulong TypeId { get; set; }
        public string TypeName { get; set; }
        public string TypeCode { get; set; }

        public string Status { get; set; }

        public ICollection<ProductToCategoryOrderDto> CategoryOrders { get; set; }

        public ICollection<ObjectFieldDto> Fields { get; set; }
    }
}