﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class RoleDto :BaseObject
    {
        public string Name { get; set; }

        public ICollection<ulong> UsersIds { get; set; } 
    }
}