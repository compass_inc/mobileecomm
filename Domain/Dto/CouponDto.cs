﻿using System;
using System.Collections.Generic;

namespace Domain.Dto
{
    public class CouponDto : BaseObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal DiscountValue { get; set; }
        public virtual IDictionary<ulong, string> Products { get; set; }
        public virtual string StatusName { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual int UseCount { get; set; }
        public virtual int MaxUseCount { get; set; }
    }
}