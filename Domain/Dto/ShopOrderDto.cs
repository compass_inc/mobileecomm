﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class ShopOrderDto : BaseObject
    {   
        public string CreationDate { get; set; }
        public ulong CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public decimal OrderSum { get; set; }

        public ulong CouponId { get; set; }
        public string CouponCode { get; set; }

        public string DeliveryTime { get; set; }

        public decimal WriteOffBonusSum { get; set; }
        public ulong BonusProgramId { get; set; }
        public string BonusProgramName { get; set; }

        public decimal ResultSum { get; set; }

        public bool IsPickup { get; set; }

        public string StatusName { get; set; }
        public ulong StatusId { get; set; }

        public ICollection<ShopOrderLineDto> Lines { get; set; }

        public ICollection<ObjectFieldDto> Fields { get; set; }
    }
}