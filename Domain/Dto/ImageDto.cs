﻿namespace Domain.Dto
{
    public class ImageDto : BaseObject
    {
        public string Description { get; set; }
        public string Path { get; set; }
        public ulong TypeId { get; set; }
        public string TypeName { get; set; }
    }
}