﻿namespace Domain.Dto
{
    public class LocaleStringValueDto : BaseObject
    {
        public string Locale { get; set; }
        public string Value { get; set; }
        public ulong LocaleStringId { get; set; }
    }
}