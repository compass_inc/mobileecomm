﻿namespace Domain.Dto
{
    public class ReviewDto : BaseObject
    {
        public ulong ProductId { get; set; }
        public string ProductName { get; set; }

        public ulong PlaceId { get; set; }
        public string PlaceName { get; set; }

        public ulong CustomerId { get; set; }
        public string CustomerAvatarPath { get; set; }
        public string DisplayName { get; set; }
        public string DisplayEMail { get; set; }
        public ulong Rate { get; set; }
        public bool IsOwnerAnswer { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        public string CreationDate { get; set; }

        public ulong ParentId { get; set; }
        public string ParentTitle { get; set; }
    }
}