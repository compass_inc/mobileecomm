﻿namespace Domain.Dto
{
    public class ProductToCategoryOrderDto : BaseObject
    {
        public ulong ProductId { get; set; }
        public ulong CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}