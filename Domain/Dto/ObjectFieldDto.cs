﻿using Domain.Enums;

namespace Domain.Dto
{
    public class ObjectFieldDto : BaseObject
    {
        public bool IsRequired { get; set; }
        public string Value { get; set; }
        public ulong FieldTypeId { get; set; }
        public FieldType FieldTypeName { get; set; }
        public string FieldTypeDescription { get; set; }

    }
}