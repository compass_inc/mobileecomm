﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class BannerDto: BaseObject
    {
        public string Name { get; set; }
        public Dictionary<ulong,string> Images { get; set; }
        public ulong TypeId { get; set; }
        public string TypeName { get; set; }
        public string TypeCode { get; set; }

        public BannerDto()
        {
            Images = new Dictionary<ulong, string>();
        }
    }
}