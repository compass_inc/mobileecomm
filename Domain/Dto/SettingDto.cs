﻿using System.Collections.Generic;

namespace Domain.Dto
{
    public class SettingsDto: BaseObject
    {
        public string ShopOrderEmail { get; set; }
        public string FromEmail { get; set; }
        public string HeaderFooterColor { get; set; }
        public string BackGroundColor { get; set; }
        public string FreeDeliveryPercentNotify { get; set; }
        public string BgFontColor { get; set; }
        public string HfFontColor { get; set; }
        public ulong LogoImageId { get; set; }
        public string LogoImagePath { get; set; }

        public ulong ReviewDefaultImageId { get; set; }
        public string ReviewDefaultImagePath { get; set; }
        public bool UseGravatar { get; set; }

        public bool NoOrderMode { get; set; }
        public bool OnlyCity { get; set; }
        public bool ShowFilialInfoInProduct { get; set; }
        public bool AuthRequired { get; set; }
        public bool OnePageCatalog { get; set; }
        public bool SideMainMenu { get; set; }
        public bool NoComments { get; set; }

        public virtual string StartPageBannerCode { get; set; }

        public string ShopName { get; set; }
        public ICollection<CategoryColorDto> CategoriesColors { get; set; } 

    }

    public class CategoryColorDto : BaseObject
    {
        public ulong CategoryId { get;set; }
        public string CategoryName { get; set; }
        public string HeaderFooterColor { get; set; }
        public string BackGroundColor { get; set; }
        public string BgFontColor { get; set; }
        public string HfFontColor { get; set; }
    }
}