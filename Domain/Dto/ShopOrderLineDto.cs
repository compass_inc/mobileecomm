﻿namespace Domain.Dto
{
    public class ShopOrderLineDto : BaseObject
    {
        public ulong ProductId { get; set; }
        public string ProductName { get; set; }

        public ulong ProductPriceId { get; set; }
        public string ProductPriceDescription { get; set; }
        public decimal ProductPriceValue { get; set; }
        public decimal ProductAmount { get; set; }

        public string DiscountDescription { get; set; }
        public decimal DiscountValue { get; set; }

        public string DeliveryDescription { get; set; }
        public decimal DeliveryValue { get; set; }

        public decimal LineSum { get; set; }

        public ulong ShopOrderId { get; set; }
    }
}