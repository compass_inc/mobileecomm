﻿using System.Collections.Generic;

namespace Domain
{
    public class Banner: BaseObject
    {
        public virtual string Name { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual BannerType Type { get; set; }
    }
}