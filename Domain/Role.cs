﻿using System.Collections.Generic;

namespace Domain
{
    public class Role: BaseObject
    {
        public virtual string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}