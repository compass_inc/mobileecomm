﻿namespace Domain
{
    public class ShopOrderState : BaseObject
    {
        public virtual bool IsDefault { get; set; }
        public virtual string Name { get; set; }
    }
}