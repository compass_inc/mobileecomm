﻿using System.Collections.Generic;
using Domain.Enums;
using Domain.Interfaces;

namespace Domain
{
    public class Customer : BaseObject, IAuthObject
    {
        public virtual string Name { get; set; }
        public virtual string Login { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
        public virtual Image Avatar { get; set; }
        public virtual ICollection<AuthSession> AuthSessions { get; set; }
        public virtual ICollection<CustomerAddress> Addresses { get; set; } 
        public virtual ICollection<ShopOrder> Orders { get; set; }
        public virtual ICollection<Product> Favorites { get; set; } 
        public virtual string Salt { get; set; }
        public virtual CustomerState Status { get; set; }
    }

    public class CustomerAddress : BaseObject
    {
        public virtual string Description { get; set; }
        public virtual string City { get; set; }
        public virtual string Street { get; set; }
        public virtual string House { get; set; }
        public virtual string FrontDoor { get; set; }
        public virtual Customer Customer { get; set; }

    }
}