﻿using System.Collections.Generic;

namespace Domain
{
    public class LocaleString : BaseObject
    {
        public virtual string Name { get; set; }
        public virtual LocaleStringType Type { get; set; }
        public virtual ICollection<LocaleStringValue> LocaleValues { get; set; }
    }
}