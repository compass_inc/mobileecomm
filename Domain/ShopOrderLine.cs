﻿namespace Domain
{
    public class ShopOrderLine : BaseObject
    {
        public virtual Product Product { get; set; }
        public virtual string ProductName { get; set; }

        public virtual Price ProductPrice { get; set; }
        public virtual string ProductPriceDescription { get; set; }
        public virtual decimal ProductPriceValue { get; set; }
        public virtual decimal ProductAmount { get; set; }

        public virtual string DiscountDescription { get; set; }
        public virtual decimal DiscountValue { get; set; }

        public virtual string DeliveryDescription { get; set; }
        public virtual decimal DeliveryValue { get; set; }

        public virtual decimal LineSum
        {
            get { return (ProductAmount*ProductPriceValue - DiscountValue) + DeliveryValue; }
        }

        public virtual ShopOrder ShopOrder { get; set; }
    }
}