﻿using System;

namespace Domain
{
    public class BaseObject : IEquatable<BaseObject>
    {
        public virtual ulong Id { get; set; }
        public virtual ulong SortOrder { get; set; }
        public virtual string Code { get; set; }

        public override int GetHashCode()
        {
            return (int)Id;
        }

        public override bool Equals(object obj)
        {
            if(obj is BaseObject)
                return Equals((BaseObject) obj);

            return base.Equals(obj);
        }

        public virtual bool Equals(BaseObject other)
        {
            return Id == (other == null ? 0 : other.Id);
        }
    }
}