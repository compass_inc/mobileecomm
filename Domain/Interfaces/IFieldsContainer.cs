﻿using System.Collections.Generic;

namespace Domain.Interfaces
{
    public interface IFieldsContainer<T> where T: BaseObject
    {
        ICollection<ObjectField<T>> Fields { get; set; }
    }
}