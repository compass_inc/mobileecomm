﻿namespace Domain.Interfaces
{
    public interface IAuthObject
    {
        string Login { get; set; }
        string Salt { get; set; }
    }
}