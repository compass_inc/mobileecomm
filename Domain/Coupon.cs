﻿
using System;
using System.Collections.Generic;
using Domain.Enums;

namespace Domain
{
    public class Coupon : BaseObject
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal DiscountValue { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ShopOrder> ShopOrdersUsedIn { get; set; }
        public virtual CouponState Status { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual int UseCount { get; set; }
        public virtual int MaxUseCount { get; set; }
    }
}