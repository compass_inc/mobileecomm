﻿namespace Domain
{
    public class ProductToCategoryOrder : BaseObject
    {
        public virtual Product SourceProduct { get; set; }
        public virtual Category KeyCategory { get; set; }
    }
}