﻿using System.Collections.Generic;
using Domain.Enums;
using Domain.Interfaces;

namespace Domain
{
    public class User : BaseObject, IAuthObject
    {
        public virtual string Login { get; set; }
        public virtual string Email { get; set; }
        public virtual string Name { get; set; }
        public virtual string Salt { get; set; }
        public virtual UserState Status { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}