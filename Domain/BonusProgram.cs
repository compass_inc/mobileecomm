﻿using System.Collections.Generic;
using Domain.Enums;

namespace Domain
{
    public class BonusProgram : BaseObject
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal MaxDiscountPercent { get; set; }
        public virtual ICollection<BonusProgramRule> Rules { get; set; }
        public virtual BonusProgramState Status { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is BonusProgram)
                return Id == ((BonusProgram) obj).Id;
            
            return base.Equals(obj);
        }


        public override int GetHashCode()
        {
            return (int)Id;
        }
    }

    public class BonusProgramRule : BaseObject
    {
        public virtual decimal ChargeInterest { get; set; }
        public virtual decimal? MinShopOrderSum { get; set; }
        public virtual decimal? MaxShopOrderSum { get; set; }
        public virtual decimal? StartBonusSum { get; set; }
        public virtual decimal? EndBonusSum { get; set; }
    }
}