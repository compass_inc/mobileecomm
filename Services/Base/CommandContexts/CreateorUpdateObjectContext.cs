﻿using Domain;
using Services.Infrastructure.Interfaces;

namespace Services.Base.CommandContexts
{
    public class CreateOrUpdateObjectContext<T> : ICommandContext where T: BaseObject
    {
        public T Object { get; set; }

        public CreateOrUpdateObjectContext(T obj)
        {
            Object = obj;
        }
    }
}