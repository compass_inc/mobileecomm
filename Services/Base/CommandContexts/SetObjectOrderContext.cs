﻿using Domain;
using Services.Infrastructure.Interfaces;

namespace Services.Base.CommandContexts
{
    public class SetObjectOrderContext<T> : ICommandContext where T:BaseObject
    {
        public ulong OrderIndex { get; set; }
        public ulong ObjectId { get; set; }

        public SetObjectOrderContext(ulong objectId, ulong orderIndex)
        {
            ObjectId = objectId;
            OrderIndex = orderIndex;
        }
    }
}