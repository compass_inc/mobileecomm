﻿using System.Collections.Generic;
using Domain;
using Services.Infrastructure.Interfaces;

namespace Services.Base.CommandContexts
{
    public class DeleteObjectContext<T> : ICommandContext where T : BaseObject
    {
        public ICollection<ulong> ObjectIds { get; set; }
    }
}