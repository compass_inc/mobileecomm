﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using Services.Infrastructure.Interfaces;

namespace Services.Base.CommandContexts
{
    public class SendMailContext : ICommandContext
    {
        public string Receiver { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public IDictionary<string, IDictionary<ContentType, Stream>> Attachments;
    }
}