﻿using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Base.Commands
{
    public class DeleteObjectCommand<T> : ICommand<DeleteObjectContext<T>> where T:BaseObject
    {
        public void Execute(DeleteObjectContext<T> commandContext)
        {
            var session = SessionManager.GetSession();
            session.Query<T>().Where(o => commandContext.ObjectIds.Contains(o.Id)).ToList().ForEach(session.Delete);
            session.Flush();
        }
    }
}