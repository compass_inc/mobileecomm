﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Services.Base.CommandContexts;
using Services.Infrastructure.Interfaces;
using Services.Settings.Criteria;
using Services.Settings.Queries;

namespace Services.Base.Commands
{
    public class SendMailCommand : ICommand<SendMailContext>
    {
        public void Execute(SendMailContext commandContext)
        {
            var to = new MailAddress(commandContext.Receiver);
            var message = new MailMessage
            {
                Subject = commandContext.Subject,
                Body = commandContext.Message
            };
            message.To.Add(to);
            var settings = new SettingsQuery().Ask(new SettingsCriterion());
            message.From = new MailAddress(message.From.Address, settings.FromEmail);
            if (commandContext.Attachments != null)
            {
                foreach (var attachment in commandContext.Attachments)
                {
                    var attachInfo = attachment.Value;
                    if (attachInfo != null && attachInfo.Any())
                    {
                        message.Attachments.Add(new Attachment(attachInfo.First().Value, attachInfo.First().Key)
                        {
                            Name = attachment.Key
                        });
                    }
                }
            }
            var client = new SmtpClient();
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                //TODO Make Log
                throw ex;
            }
        }
    }
}