﻿using System;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Base.Commands
{
    public class CreateOrUpdateObjectCommand<T> : ICommand<CreateOrUpdateObjectContext<T>> where T : BaseObject
    {
        public virtual void Execute(CreateOrUpdateObjectContext<T> commandContext)
        {
            if (commandContext.Object == null)
                throw new ArgumentNullException("commandContext", @"не задан объект для сохранения");

            var session = SessionManager.GetSession();

            if(commandContext.Object.SortOrder == 0)
            {
                commandContext.Object.SortOrder = session.Query<T>().Any()
                    ? session.Query<T>().Max(o => o.SortOrder) + 10
                    : 10;
            }

            commandContext.Object = session.Merge(commandContext.Object);
            session.SaveOrUpdate(commandContext.Object);
            session.Flush();
        }
    }
}