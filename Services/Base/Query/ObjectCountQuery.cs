﻿using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Base.Query
{
    public class ObjectCountQuery<T> : IQuery<ObjectCountCriterion<T>, int> where T : BaseObject
    {
        public int Ask(ObjectCountCriterion<T> criterion)
        {
            return SessionManager.GetSession().Query<T>().Count();
        }
    }
}