﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Base.Query
{
    public class BaseObjectQuery<T> : IQuery<BaseObjectCriterion,ICollection<T>> where T : BaseObject
    {
        public ICollection<T> Ask(BaseObjectCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<T>();

            return Ask(criterion, q).ToList();
        }

        public static IQueryable<T> Ask(BaseObjectCriterion criterion, IQueryable<T> query, bool applyPagination = true)
        {
            if (criterion.Ids != null && criterion.Ids.Any())
                query = query.Where(p => criterion.Ids.Contains(p.Id));

            if (criterion.Codes != null && criterion.Codes.Any())
                query = query.Where(p => criterion.Codes.Contains(p.Code));
            
            if (applyPagination && (criterion.PageSize > 0 && criterion.PageNumber >= 0))
                query = query.Skip(criterion.PageNumber*criterion.PageSize).Take(criterion.PageSize);

            return query;
        }

        public static IQueryable<T> ApplyPagination(BaseObjectCriterion criterion, IQueryable<T> query)
        {
            if (criterion.PageSize > 0 && criterion.PageNumber >= 0)
                query = query.Skip(criterion.PageNumber * criterion.PageSize).Take(criterion.PageSize);
            return query;
        }
    }

    public class BaseObjectQueryQueryable<T> : IQuery<BaseObjectCriterion, IQueryable<T>> where T : BaseObject
    {
        public IQueryable<T> Ask(BaseObjectCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<T>();

            return BaseObjectQuery<T>.Ask(criterion, q);
        }
    }
}