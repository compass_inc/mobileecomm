﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Base.Query
{
    public class GenerateObjectFieldsQuery<T> : IQuery<GenerateObjectFieldsCriterion<T>,ICollection<ObjectField<T>>> where T: BaseObject
    {
        public ICollection<ObjectField<T>> Ask(GenerateObjectFieldsCriterion<T> criterion)
        {
            var types = SessionManager.GetSession().Query<ObjectFieldType<T>>().ToList();
            return types.Select(oft => new ObjectField<T> {FieldType = oft}).ToList();
        }
    }
}