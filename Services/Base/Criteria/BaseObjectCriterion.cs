﻿using System.Collections.Generic;
using Services.Infrastructure.Interfaces;

namespace Services.Base.Criteria
{
    public class BaseObjectCriterion : ICriterion
    {
        public ICollection<ulong> Ids { get; set; }
        public ICollection<string> Codes { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}