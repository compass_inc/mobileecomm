﻿using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Product.Criteria;

namespace Services.Product.Queries
{
    public class GetNextCategoryOrderQuery : IQuery<GetNextCategoryOrderCriterion, ulong>
    {
        public ulong Ask(GetNextCategoryOrderCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<ProductToCategoryOrder>();
            return
                q.Any(pc => pc.KeyCategory.Id == criterion.CategoryId)
                    ? q.Where(
                        pc => pc.KeyCategory.Id == criterion.CategoryId)
                        .Max(pc => pc.SortOrder) + 10
                    : 10;
        }
    }
}