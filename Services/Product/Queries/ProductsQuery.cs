﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Base.Query;
using Services.Enums;
using Services.Infrastructure.Interfaces;
using Services.Product.Criteria;

namespace Services.Product.Queries
{
    public class ProductsQuery : BaseObjectQuery<Domain.Product>, IQuery<ProductsCriterion,ICollection<Domain.Product>>
    {
        public IQueryable<Domain.Product> ApplyProductsFilters(IQueryable<Domain.Product> q, ProductsCriterion criterion)
        {
            if (criterion.CategoryId == null)
                q = q.Where(p => !p.Categories.Any());
            else if (criterion.CategoryId.Any() && criterion.CategoryId.All(c=>c > 0))
                q = q.Where(p => p.Categories.Any(c => criterion.CategoryId.Contains(c.Id)));


            if (!string.IsNullOrEmpty(criterion.Name))
                q = q.Where(p => p.Name.Contains(criterion.Name));

            if (criterion.StartPrice != null)
                q = q.Where(p => p.Prices.Any(pr => pr.Value >= criterion.StartPrice));
            if (criterion.EndPrice != null)
                q = q.Where(p => p.Prices.Any(pr => pr.Value <= criterion.EndPrice));

            if (criterion.PlaceId > 0)
                q = q.Where(p => !p.Places.Any() || p.Places.Any(pl => pl.Id == criterion.PlaceId));
           else if (!string.IsNullOrEmpty(criterion.CityName))
                q = q.Where(p => !p.Places.Any() || p.Places.Any(pl => pl.City == criterion.CityName));

            return q;
        }

        public ICollection<Domain.Product> Ask(ProductsCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Domain.Product>();

            q = ApplyProductsFilters(q, criterion);

            q = Ask(criterion, q, false);

            // -- применяем сортировку
            if (criterion.SortByName)
                q = criterion.SortDirection == SortDirection.Asc
                    ? q.OrderBy(p => p.Name)
                    : q.OrderByDescending(p => p.Name);
            else if (criterion.SortByPrice)
                q = criterion.SortDirection == SortDirection.Asc
                    ? q.OrderBy(p => p.Prices.Select(price => price.ValueCalc).Min())
                    : q.OrderByDescending(p => p.Prices.Select(price => price.ValueCalc).Min());
            else if (criterion.CategoryId != null && criterion.CategoryId.Any() && criterion.CategoryId.Count == 1)
                q = criterion.SortDirection == SortDirection.Asc
                    ? q.OrderBy(
                        p =>
                            p.CategoriesOrders.Where(c => c.KeyCategory.Id == criterion.CategoryId.First())
                                .Select(c => c.SortOrder))
                    : q.OrderByDescending(
                        p =>
                            p.CategoriesOrders.Where(c => c.KeyCategory.Id == criterion.CategoryId.First())
                                .Select(c => c.SortOrder));


            return ApplyPagination(criterion, q).ToList();
        }
    }
}