﻿using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Product.Criteria;

namespace Services.Product.Queries
{
    public class CountProductsQuery: IQuery<ProductsCriterion, int>
    {
        public int Ask(ProductsCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Domain.Product>();

            q = new ProductsQuery().ApplyProductsFilters(q, criterion);

            return q.Count();
        }
    }
}