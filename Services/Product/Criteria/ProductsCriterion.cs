﻿using System.Collections.Generic;
using Services.Base.Criteria;
using Services.Enums;

namespace Services.Product.Criteria
{
    public class ProductsCriterion : BaseObjectCriterion
    {
        public List<ulong?> CategoryId { get; set; }
        public string Name { get; set; }

        public decimal? StartPrice { get; set; }
        public decimal? EndPrice { get; set; }

        public ulong PlaceId { get; set; }
        public string CityName { get; set; }

        public SortDirection SortDirection { get; set; }

        public bool SortByName { get; set; }
        public bool SortByPrice { get; set; }
    }
}