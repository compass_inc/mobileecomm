﻿using Services.Infrastructure.Interfaces;

namespace Services.Product.Criteria
{
    public class GetNextCategoryOrderCriterion : ICriterion
    {
        public ulong CategoryId;
    }
}