﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Base.Query;
using Services.Infrastructure.Interfaces;
using Services.ShopOrder.Criteria;

namespace Services.ShopOrder.Queries
{
    public class ShopOrdersQuery : BaseObjectQuery<Domain.ShopOrder>, IQuery<ShopOrdersCriterion,ICollection<Domain.ShopOrder>>
    {
        public IQueryable<Domain.ShopOrder> ApplyShopOrdersFilter(IQueryable<Domain.ShopOrder> q,
            ShopOrdersCriterion criterion)
        {
            if (criterion.CustomerId > 0)
                q = q.Where(so => so.Customer.Id == criterion.CustomerId);

            if (criterion.CreationDateStart != null)
            {
                var date = criterion.CreationDateStart.Value.Date;
                q = q.Where(so => so.CreationDate >= date /*&& so.CreationDate < date.AddDays(1)*/);
            }
            if (criterion.CreationDateEnd != null)
            {
                var date = criterion.CreationDateEnd.Value.Date;
                q = q.Where(so => so.CreationDate <= date /*&& so.CreationDate < date.AddDays(1)*/);
            }

            return q;
        }

        public ICollection<Domain.ShopOrder> Ask(ShopOrdersCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Domain.ShopOrder>();

            q = ApplyShopOrdersFilter(q, criterion);

            return Ask(criterion, q).ToList();
        }
    }
}