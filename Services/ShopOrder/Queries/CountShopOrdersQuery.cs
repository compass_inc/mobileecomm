﻿using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.ShopOrder.Criteria;

namespace Services.ShopOrder.Queries
{
    public class CountShopOrdersQuery : IQuery<ShopOrdersCriterion,int>
    {
        public int Ask(ShopOrdersCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Domain.ShopOrder>();

            q = new ShopOrdersQuery().ApplyShopOrdersFilter(q, criterion);

            return q.Count();
        }
    }
}