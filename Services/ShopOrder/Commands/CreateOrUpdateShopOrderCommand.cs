﻿using System;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using NHibernate.Util;
using Services.Base.CommandContexts;
using Services.Base.Commands;
using Services.Base.Criteria;
using Services.Base.Query;
using Services.BonusProgram.CommandContexts;
using Services.BonusProgram.Commands;
using Services.Coupon.commandContexts;
using Services.Coupon.Commands;
using Services.Coupon.Criteria;
using Services.Coupon.Queries;
using Services.Infrastructure.Interfaces;
using Services.Settings.Criteria;
using Services.Settings.Queries;
using Services.ShopOrder.CommandContext;

namespace Services.ShopOrder.Commands
{
    public class CreateOrUpdateShopOrderCommand : CreateOrUpdateObjectCommand<Domain.ShopOrder>, ICommand<CreateOrUpdateShopOrderContext>
    {
        public void Execute(CreateOrUpdateShopOrderContext commandContext)
        {
            var session = SessionManager.GetSession();

            var transaction = session.BeginTransaction();

            if (commandContext.Object.Id > 0)
            {
                //commandContext.Object = session.Merge(commandContext.Object);
                // var props = session.GetDirtyProperties(commandContext.Object);
                //TODO сделать проверку на возможность редактирования полей
            }
            else
            {
                commandContext.Object.Status = session.Query<ShopOrderState>().FirstOrDefault(q => q.IsDefault);
                commandContext.Object.CreationDate = DateTime.Now;

                var fieldTypes = session.Query<ObjectFieldType<Domain.ShopOrder>>().ToList();
                if (fieldTypes.Any(f => f.IsRequired) && (commandContext.Object.Fields == null ||
                                                          (commandContext.Object.Fields.Count <
                                                           fieldTypes.Count(ft => ft.IsRequired))))
                    throw new Exception("не все обязательные поля заполнены");

                if (commandContext.Object.Fields != null)
                {
                    foreach (var fieldType in fieldTypes)
                    {
                        var field = commandContext.Object.Fields.FirstOrDefault(f => f.FieldType.Id == fieldType.Id);

                        if ((field == null || string.IsNullOrEmpty(field.Value)) && fieldType.IsRequired)
                            throw new Exception("не все обязательные поля заполнены");

                        if (field == null || string.IsNullOrEmpty(field.Value))
                            continue;
                        try
                        {
                            var parseMethod = fieldType.Type.GetMethod("Parse", new[] {field.Value.GetType()});
                            if (parseMethod != null)
                                parseMethod.Invoke(null, new object[] {field.Value});
                        }
                        catch
                        {
                            throw new Exception("Введен не корректное значение");
                        }
                    }
                }

                //Применяем бонусную программу и списываем бонусы
                if (commandContext.Object.Customer != null)
                {
                    new ApplyBonusesForCustomerCommand().Execute(new ApplyBonusesForCustomerContext
                    {
                        CustomerId = commandContext.Object.Customer.Id,
                        ShopOrderSum = commandContext.Object.OrderSum
                    });

                    if (commandContext.Object.WriteOffBonusSum > 0)
                    {
                        var writeOffBonusContext = new WriteOffBonusesForCustomerContext()
                        {
                            CustomerId = commandContext.Object.Customer.Id,
                            ShopOrderSum = commandContext.Object.OrderSum,
                            WriteOffSum = commandContext.Object.WriteOffBonusSum
                        };
                        new WriteOffBonusesForCustomerCommand().Execute(writeOffBonusContext);

                        commandContext.Object.WriteOffBonusSum = writeOffBonusContext.WriteOffSum;
                    }
                }
                //Применяем скидку по купону
                if (!string.IsNullOrEmpty(commandContext.CouponCode))
                {
                    new GetCouponDiscountQuery().Ask(new GetCouponDiscountCriterion
                    {
                        Code = commandContext.CouponCode,
                        ProductIds = commandContext.Object.Lines.Select(l => l.Product.Id).ToList()
                    }).ForEach(cd =>
                    {
                        var line = commandContext.Object.Lines.First(l => l.Product.Id == cd.Key);
                        line.DiscountValue = line.LineSum/100*cd.Value;
                        line.DiscountDescription = "Скидка по купону:" + commandContext.CouponCode;
                    });
                    var closeCouponContext = new TryToCloseCouponContext
                    {
                        Code = commandContext.CouponCode,
                        IncrementUseCount = true
                    };
                    new TryToCloseCouponCommand().Execute(closeCouponContext);

                    commandContext.Object.Coupon = closeCouponContext.Coupon;
                }

                base.Execute(commandContext);

                transaction.Commit();

                //Отправляем оповещение о заказе, если настроена почта
                var settings = new SettingsQuery().Ask(new SettingsCriterion());
                var receiver = settings.ShopOrderEmail;
                if (!string.IsNullOrEmpty(receiver))
                {
                    var message = "Поступил новый заказ от " + commandContext.Object.CreationDate + " :" +
                                  Environment.NewLine;
                    message +=
                        commandContext.Object.Lines.Select(
                                l =>
                                    l.ProductName + " " + l.ProductAmount + " " + l.ProductPrice.ValueCalc.ToString("F") +
                                    " " + l.LineSum.ToString("F"))
                            .Aggregate((s1, s2) => s1 + Environment.NewLine + s2);

                    new SendMailCommand().Execute(new SendMailContext
                    {
                        Message = message,
                        Receiver = receiver,
                        Subject = "Новый заказ"
                    });
                }
            }
        }
    }
}