﻿using Services.Base.CommandContexts;

namespace Services.ShopOrder.CommandContext
{
    public class CreateOrUpdateShopOrderContext : CreateOrUpdateObjectContext<Domain.ShopOrder>
    {
        public CreateOrUpdateShopOrderContext(Domain.ShopOrder obj) : base(obj)
        {
        }

        public string CouponCode { get; set; }
    }
}