﻿using System;
using Services.Base.Criteria;

namespace Services.ShopOrder.Criteria
{
    public class ShopOrdersCriterion : BaseObjectCriterion
    {
        public ulong CustomerId{ get; set; }
        public DateTime? CreationDateStart { get; set; }
        public DateTime? CreationDateEnd { get; set; }
    }
}