﻿using System;
using System.Linq;
using DataAccess;
using Domain.Enums;
using NHibernate.Linq;
using Services.Coupon.commandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Coupon.Commands
{
    public class TryToCloseCouponCommand : ICommand<TryToCloseCouponContext>
    {
        public void Execute(TryToCloseCouponContext commandContext)
        {
            var coupon = SessionManager.GetSession()
                .Query<Domain.Coupon>()
                .FirstOrDefault(
                    c =>
                        c.Code == commandContext.Code && c.Status == CouponState.Active);

            if (coupon != null)
            {
                commandContext.Coupon = coupon;

                if (commandContext.IncrementUseCount)
                    coupon.UseCount ++;

                if(coupon.MaxUseCount > 0 && coupon.MaxUseCount <= coupon.UseCount)
                    coupon.Status = CouponState.Closed;

                if (coupon.EndDate != null && coupon.EndDate <= DateTime.Now)
                    coupon.Status = CouponState.Closed;
            }

        }
    }
}