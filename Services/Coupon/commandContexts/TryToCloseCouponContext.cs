﻿using Services.Infrastructure.Interfaces;

namespace Services.Coupon.commandContexts
{
    public class TryToCloseCouponContext : ICommandContext
    {
        public bool IncrementUseCount { get; set; }
        public string Code { get; set; }
        public Domain.Coupon Coupon { get; set; }
    }
}