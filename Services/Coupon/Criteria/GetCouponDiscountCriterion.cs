﻿using System.Collections.Generic;
using Services.Infrastructure.Interfaces;

namespace Services.Coupon.Criteria
{
    public class GetCouponDiscountCriterion : ICriterion
    {
        public string Code { get; set; }
        public ICollection<ulong> ProductIds { get; set; } 
    }
}