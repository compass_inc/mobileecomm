﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Enums;
using NHibernate.Linq;
using Services.Coupon.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Coupon.Queries
{
    public class GetCouponDiscountQuery: IQuery<GetCouponDiscountCriterion,IDictionary<ulong, decimal>>
    {
        public IDictionary<ulong, decimal> Ask(GetCouponDiscountCriterion criterion)
        {
            var coupon = SessionManager.GetSession()
                .Query<Domain.Coupon>()
                .FirstOrDefault(
                    c =>
                        c.Code == criterion.Code && (c.EndDate == null || c.EndDate >= DateTime.Now) &&
                        c.Status == CouponState.Active);

            if (coupon!=null && criterion.ProductIds != null && criterion.ProductIds.Any())
            {
                return coupon.Products != null && coupon.Products.Any()
                    ? coupon.Products.Select(p => p.Id)
                        .Intersect(criterion.ProductIds)
                        .ToDictionary(p => p, p => coupon.DiscountValue)
                    : criterion.ProductIds.ToDictionary(p => p, p => coupon.DiscountValue);
            }

            return null;
        }
    }
}