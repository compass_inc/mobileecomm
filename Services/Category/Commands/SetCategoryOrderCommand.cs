﻿using System;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Base.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Category.Commands
{
    public class SetCategoryOrderCommand : ICommand<SetObjectOrderContext<Domain.Category>>
    {
        public void Execute(SetObjectOrderContext<Domain.Category> commandContext)
        {
            if (commandContext.ObjectId == 0)
                throw new ArgumentException("Не задан Id объекта");
            
            var session = SessionManager.GetSession();

            var cat = session.Get<Domain.Category>(commandContext.ObjectId);
            
            var max =
                session.Query<Domain.Category>()
                    .Where(c => c.Parent == cat.Parent)
                    .Max(c=>c.SortOrder);
            var min =
                session.Query<Domain.Category>()
                    .Where(c => c.Parent == cat.Parent)
                    .Min(c => c.SortOrder);

            var orderIndex = commandContext.OrderIndex > max
                ? max
                : commandContext.OrderIndex < min ? min : commandContext.OrderIndex;

            if (cat.SortOrder > orderIndex)
            {
                session.Query<Domain.Category>()
                    .Where(c => c.Parent == cat.Parent && (c.SortOrder >= orderIndex && c.SortOrder < cat.SortOrder))
                    .ToList()
                    .ForEach(c => c.SortOrder++);
            }
            else
            {
                session.Query<Domain.Category>()
                   .Where(c => c.Parent == cat.Parent && (c.SortOrder <= orderIndex && c.SortOrder > cat.SortOrder))
                   .ToList()
                   .ForEach(c => c.SortOrder--);
            }

            cat.SortOrder = orderIndex;

            session.Flush();
        }
    }
}