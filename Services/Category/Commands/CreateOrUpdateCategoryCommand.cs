﻿using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Base.CommandContexts;
using Services.Base.Commands;

namespace Services.Category.Commands
{
    public class CreateOrUpdateCategoryCommand : CreateOrUpdateObjectCommand<Domain.Category>
    {
        public override void Execute(CreateOrUpdateObjectContext<Domain.Category> commandContext)
        {
            if (commandContext.Object.Parent != null && commandContext.Object.Parent.Id == commandContext.Object.Id)
                commandContext.Object.Parent = null;

            if(commandContext.Object.Id == 0)
            {
                var catQ = SessionManager.GetSession()
                    .Query<Domain.Category>();
                commandContext.Object.SortOrder =
                    catQ.Any(c => c.Parent == commandContext.Object.Parent)
                        ? catQ.Where(c => c.Parent == commandContext.Object.Parent)
                            .Max(s => s.SortOrder) + 1
                        : 1;
            }
            base.Execute(commandContext);
        }
    }
}