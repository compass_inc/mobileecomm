﻿using Services.Base.Criteria;

namespace Services.Category.Criteria
{
    public class CategoriesCriterion : BaseObjectCriterion
    {
        public bool WithChildren { get; set; }
        public ulong StartCategoryId { get; set; }
        public string StartCategoryCode { get; set; }
        public string Name { get; set; }
        public bool NoEmpty { get; set; }
        public ulong PlaceId { get; set; }
        public string CityName { get; set; }
    }
}