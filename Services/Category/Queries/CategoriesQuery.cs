﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Base.Query;
using Services.Category.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Category.Queries
{
    public class CategoryQuery : BaseObjectQuery<Domain.Category>, IQuery<CategoriesCriterion, ICollection<Domain.Category>>
    {
        public ICollection<Domain.Category> Ask(CategoriesCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Domain.Category>();

            if (criterion.Ids != null && criterion.Ids.Any())
                return q.Where(c => criterion.Ids.Contains(c.Id)).ToList();

            var qP = !string.IsNullOrEmpty(criterion.StartCategoryCode)
                ? q.Where(c => c.Parent.Code == criterion.StartCategoryCode)
                : criterion.StartCategoryId > 0
                    ? q.Where(c => c.Parent.Id == criterion.StartCategoryId)
                    : q.Where(c => c.Parent == null);

            if (!string.IsNullOrEmpty(criterion.Name))
                qP = qP.Where(c => c.Name == criterion.Name);

            var result = Ask(criterion, qP).ToList();

            if (criterion.WithChildren)
            {
                var childRes = result;
                while (childRes.Any())
                {
                    childRes = q.Where(c => childRes.Contains(c.Parent)).ToList();
                    result = result.Concat(childRes).ToList();
                }
            }

            if (criterion.PlaceId > 0)
                result =
                    result.Where(
                        c => !c.Products.Any() || c.Products.Any(p => !p.Places.Any() || p.Places.Any(pl => pl.Id == criterion.PlaceId)))
                        .ToList();
            else if (!string.IsNullOrEmpty(criterion.CityName))
                result =
                  result.Where(
                      c => !c.Products.Any() || c.Products.Any(p => !p.Places.Any() || p.Places.Any(pl => pl.City == criterion.CityName)))
                      .ToList();
            

            if (criterion.NoEmpty)
                result = result.Where(c => c.Products.Count > 0).ToList();

            return result;
        }
    }
}