﻿using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Settings.Criteria;

namespace Services.Settings.Queries
{
    public class SettingsQuery : IQuery<SettingsCriterion,Domain.Settings>
    {
        public Domain.Settings Ask(SettingsCriterion criterion)
        {
            return SessionManager.GetSession().Query<Domain.Settings>().FirstOrDefault() ?? new Domain.Settings();
        }
    }
}