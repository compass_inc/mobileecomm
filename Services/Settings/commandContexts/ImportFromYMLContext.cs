﻿using Services.Infrastructure.Interfaces;

namespace Services.Settings.commandContexts
{
    public class ImportFromYmlContext : ICommandContext
    {
        public bool ImportImages { get; set; }
        public string FileContent { get; set; }
    }
}