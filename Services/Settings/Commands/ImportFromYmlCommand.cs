﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Settings.commandContexts;

namespace Services.Settings.Commands
{
    public class ImportFromYmlCommand : ICommand<ImportFromYmlContext>
    {
        public void Execute(ImportFromYmlContext commandContext)
        {
            var session = SessionManager.GetSession();

            var catsAndOffers = (XDocument.Parse(commandContext.FileContent).Root.Nodes().First() as XContainer).Nodes()
                .Where(n => (n as XElement).Name == "categories" || (n as XElement).Name == "offers")
                .ToList();

            var cats = catsAndOffers.First(co => (co as XElement).Name == "categories") as XContainer;
            var offers = catsAndOffers.First(co => (co as XElement).Name == "offers") as XContainer;

            var categories = new List<Domain.Category>();
            var defferedParents = new Dictionary<string, List<string>>();

            if (cats != null)
                foreach (var cat in cats.Nodes().Cast<XElement>())
                {
                    var code = cat.Attributes().First(a => a.Name == "id").Value;
                    var name = cat.Value.Replace("\n", "").Replace("\r","");
                    var parentCode = cat.Attributes().Any(a => a.Name == "parentId")
                        ? cat.Attributes().First(a => a.Name == "parentId").Value
                        : "";

                    var category = session.Query<Domain.Category>().FirstOrDefault(c => c.Code == code) ??
                                   new Domain.Category {Code = code};
                   
                    category.Name = name;

                    if (!string.IsNullOrEmpty(parentCode))
                    {
                        var parentCategory = categories.FirstOrDefault(c => c.Code == parentCode);
                        if (parentCategory != null)
                            category.Parent = parentCategory;
                        else
                        {
                            if (defferedParents.ContainsKey(parentCode))
                                defferedParents[parentCode].Add(category.Code);
                            else
                                defferedParents.Add(parentCode, new List<string> {category.Code});
                        }
                    }

                    if (defferedParents.ContainsKey(category.Code))
                    {
                        defferedParents[category.Code].ForEach(child =>
                        {
                            categories.First(c => c.Code == child).Parent = category;
                        });

                        defferedParents.Remove(category.Code);
                    }

                    categories.Add(category);
                }

            categories.ForEach(session.SaveOrUpdate);

            var products = new List<Domain.Product>();

            if(offers!=null)
                foreach (var offer in offers.Nodes().Cast<XElement>())
                {
                    var code = offer.Attributes().First(a => a.Name.LocalName.ToLower() == "id").Value;
                    var name = (offer.Nodes().First(n => (n as XElement).Name.LocalName.ToLower() == "name") as XElement).Value.Replace("\n", "").Replace("\r", "");
                    var description = (offer.Nodes().First(n => (n as XElement).Name.LocalName.ToLower() == "description") as XElement).Value;
                    var price = (offer.Nodes().First(n => (n as XElement).Name.LocalName.ToLower() == "price") as XElement).Value;
                    var categoryCode = offer.Nodes().Any(n => (n as XElement).Name.LocalName.ToLower() == "categoryid")
                        ? (offer.Nodes().First(n => (n as XElement).Name.LocalName.ToLower() == "categoryid") as XElement).Value
                        : "";
                    var pictures = offer.Nodes().Where(n => (n as XElement).Name.LocalName.ToLower() == "picture").ToList();

                    var product = session.Query<Domain.Product>().FirstOrDefault(p => p.Code == code) ??
                                  new Domain.Product {Code = code};

                    product.Name = name;
                    product.ShortDescription = description;

                    var newPrice = new Price { Value = decimal.Parse(price, NumberStyles.Any, CultureInfo.InvariantCulture)};
                    if (product.Prices != null)
                    {
                        if (product.Prices.Count() == 1)
                        {
                            product.Prices.First().Value = newPrice.Value;
                            product.Prices.First().Description = string.Empty;
                        }
                        else
                        {
                            product.Prices.Clear();
                            product.Prices.Add(newPrice);
                        }
                    }
                    else
                        product.Prices = new List<Price> { newPrice };

                    if (string.IsNullOrEmpty(categoryCode))
                        product.Categories = null;
                    else
                    {
                        var cat = categories.First(c => c.Code == categoryCode);
                        product.Categories = new List<Domain.Category> {cat};

                        if (product.CategoriesOrders == null)
                            product.CategoriesOrders = new List<ProductToCategoryOrder>();
                        if (product.CategoriesOrders.All(p => p.KeyCategory.Id != cat.Id))
                            product.CategoriesOrders.Add(new ProductToCategoryOrder
                            {
                                KeyCategory = cat,
                                SortOrder =
                                    products.Any(p => p.Categories.Any(c => c.Id == cat.Id))
                                        ? products.Where(p => p.Categories.Any(c => c.Id == cat.Id))
                                            .Max(
                                                p =>
                                                    p.CategoriesOrders.First(co => co.KeyCategory.Id == cat.Id)
                                                        .SortOrder) + 10
                                        : session.Query<ProductToCategoryOrder>().Any(c => c.KeyCategory.Id == cat.Id)
                                            ? session.Query<ProductToCategoryOrder>()
                                                .Where(c => c.KeyCategory.Id == cat.Id)
                                                .Max(c => c.SortOrder) + 10
                                            : 10
                            });
                    }

                    product.Images =
                        new List<Image>(
                            pictures.Select(
                                picture =>
                                    session.Query<Image>().FirstOrDefault(i => i.Path == (picture as XElement).Value) ??
                                    new Image {Path = (picture as XElement).Value}));
                
                    products.Add(product);
                }
            
            products.ForEach(session.SaveOrUpdate);
            session.Flush();
        }
    }
}