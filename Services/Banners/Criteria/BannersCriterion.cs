﻿using Services.Base.Criteria;

namespace Services.Banners.Criteria
{
    public class BannersCriterion : BaseObjectCriterion
    {
        public string BannerTypeCode { get; set; }
    }
}