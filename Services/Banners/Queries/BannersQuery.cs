﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Banners.Criteria;
using Services.Base.Query;
using Services.Infrastructure.Interfaces;

namespace Services.Banners.Queries
{
    public class BannersQuery : BaseObjectQuery<Domain.Banner>, IQuery<BannersCriterion, ICollection<Domain.Banner>>
    {
        public ICollection<Domain.Banner> Ask(BannersCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Domain.Banner>();

            if (!string.IsNullOrEmpty(criterion.BannerTypeCode))
                q = q.Where(c => c.Type != null && c.Type.Code == criterion.BannerTypeCode);

            return Ask(criterion, q).ToList();
        }
    }
}