﻿using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Images.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Images.Queries
{
    public class CountImagesQuery : IQuery<ImagesCriterion, int>
    {
        public int Ask(ImagesCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Image>();

            q = new ImagesQuery().ApplyImagesFilter(q, criterion);

            return q.Count();
        }
    }
}