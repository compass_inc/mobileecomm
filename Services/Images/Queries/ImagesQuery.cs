﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Query;
using Services.Images.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Images.Queries
{
    public class ImagesQuery : BaseObjectQuery<Image>, IQuery<ImagesCriterion, ICollection<Image>>
    {
        public IQueryable<Image> ApplyImagesFilter(IQueryable<Image> q, ImagesCriterion criterion)
        {
            if (criterion.TypeId > 0)
                q = q.Where(i => i.Type.Id == criterion.TypeId);

            return q;
        }

        public ICollection<Image> Ask(ImagesCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Image>();

            q = ApplyImagesFilter(q, criterion);

            return Ask(criterion, q).ToList();
        }
    }
}