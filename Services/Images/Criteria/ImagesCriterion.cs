﻿using Services.Base.Criteria;

namespace Services.Images.Criteria
{
    public class ImagesCriterion: BaseObjectCriterion
    {
        public ulong TypeId { get; set; }
    }
}