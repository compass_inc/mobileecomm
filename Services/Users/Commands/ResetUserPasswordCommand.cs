﻿using Services.Auth;
using Services.Base.CommandContexts;
using Services.Base.Commands;
using Services.Infrastructure.Interfaces;
using Services.Users.CommandContexts;

namespace Services.Users.Commands
{
    public class ResetUserPasswordCommand : ResetPasswordCommand<Domain.User>,
       ICommand<ResetUserPasswordContext>
    {
        public void Execute(ResetUserPasswordContext commandContext)
        {
            string newPass;
            base.Execute(commandContext, out newPass);
            new SendMailCommand().Execute(new SendMailContext
            {
                Subject = "Восстановление пароля",
                Message = "Ваш новый пароль: " + newPass,
                Receiver = commandContext.Login
            });
        }
    }
}