﻿using System;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Commands;
using Services.Crypt;
using Services.Infrastructure.Interfaces;
using Services.Users.CommandContexts;

namespace Services.Users.Commands
{
    public class CreateOrUpdateUserCommand : CreateOrUpdateObjectCommand<User>, ICommand<CreateOrUpdateUserContext>
    {
        public void Execute(CreateOrUpdateUserContext commandContext)
        {
            var existUser = SessionManager.GetSession()
               .Query<User>()
               .FirstOrDefault(c => c.Login == commandContext.Object.Login);

            var newLoginExists = false;
            if (!string.IsNullOrEmpty(commandContext.NewEmail))
                newLoginExists =
                    SessionManager.GetSession().Query<User>().Any(c => c.Login == commandContext.NewEmail);

            if (commandContext.Object.Id == 0)
            {
                if (string.IsNullOrEmpty(commandContext.Password))
                    throw new ArgumentException("Не задан пароль!");

                if (string.IsNullOrEmpty(commandContext.Object.Login))
                    throw new ArgumentException("Не задана логин!");

                if (string.IsNullOrEmpty(commandContext.Object.Email))
                    throw new ArgumentException("Не задана электронная почта!");

                if (existUser == null)
                    commandContext.Object.Salt =
                        new CreateHashQuery().Ask(new CreateHashCriterion { Data = commandContext.Password });
                else
                    throw new ArgumentException("Указанный логин уже используется!");
            }
            else if ((existUser != null && existUser.Id != commandContext.Object.Id) || newLoginExists)
                throw new ArgumentException("Указанный логин уже используется!");

            if (existUser != null)
            {
                commandContext.Object.Salt = existUser.Salt;
                if (!string.IsNullOrEmpty(commandContext.NewEmail))
                    commandContext.Object.Login = commandContext.NewEmail;
            }

            base.Execute(commandContext);
        }
    }
}