﻿using Domain;
using Services.Auth;
using Services.Infrastructure.Interfaces;
using Services.Users.CommandContexts;

namespace Services.Users.Commands
{
    public class ChangeUserPasswordCommand : ChangePasswordCommand<User>, ICommand<ChangeUserPasswordContext>
    {
        public void Execute(ChangeUserPasswordContext commandContext)
        {
            base.Execute(commandContext);
        }
    }
}