﻿using Domain;
using Services.Base.CommandContexts;

namespace Services.Users.CommandContexts
{
    public class CreateOrUpdateUserContext : CreateOrUpdateObjectContext<User>
    {
        public CreateOrUpdateUserContext(User obj) : base(obj)
        {
        }

        public string NewEmail { get; set; }

        public string Password { get; set; }
    }
}