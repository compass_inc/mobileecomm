﻿using Services.Infrastructure.Interfaces;

namespace Services.Users.Criteria
{
    public class ValidateUserPasswordCriterion : ICriterion
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}