﻿using Services.Infrastructure.Interfaces;

namespace Services.Users.Criteria
{
    public class UsersCriterion : ICriterion
    {
        public string Email { get; set; }
    }
}