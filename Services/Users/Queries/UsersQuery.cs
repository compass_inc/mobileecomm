﻿using System.Linq;
using DataAccess;
using Domain;
using Domain.Enums;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Users.Criteria;

namespace Services.Users.Queries
{
    public class UsersQuery : IQuery<UsersCriterion,User>
    {
        public User Ask(UsersCriterion criterion)
        {
            return
                SessionManager.GetSession()
                    .Query<User>()
                    .FirstOrDefault(u => u.Login == criterion.Email && u.Status == UserState.Active);
        }
    }
}