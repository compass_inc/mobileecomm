﻿using System;
using System.Linq;
using DataAccess;
using Domain.Enums;
using NHibernate.Linq;
using Services.Crypt;
using Services.Infrastructure.Interfaces;
using Services.Users.Criteria;

namespace Services.Users.Queries
{
    public class ValidateUserPasswordQuery : IQuery<ValidateUserPasswordCriterion, bool>
    {
        public bool Ask(ValidateUserPasswordCriterion criterion)
        {
            if (string.IsNullOrEmpty(criterion.Email))
                throw new ArgumentException("Не указан электронный адрес!");

            if (string.IsNullOrEmpty(criterion.Password))
                throw new ArgumentException("Не указан пароль!");

            var user =
                SessionManager.GetSession().Query<Domain.User>().FirstOrDefault(c => c.Login == criterion.Email);

            if (user == null)
                return false;

            if (user.Status == UserState.Blocked)
                throw new ApplicationException("Пользователь заблокирован!");

            return user.Salt == new CreateHashQuery().Ask(new CreateHashCriterion { Data = criterion.Password });
        }
    }
}