﻿using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.BonusProgram.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Commands
{
    public class WriteOffBonusesForCustomerCommand : ICommand<WriteOffBonusesForCustomerContext>
    {
        public void Execute(WriteOffBonusesForCustomerContext commandContext)
        {
            //TODO сделать списание бонусов по всем программам, а не только по одной
            var session = SessionManager.GetSession();
            var customerBonuses =
                session
                    .Query<CustomerBonus>()
                    .FirstOrDefault(c => c.Customer.Id == commandContext.CustomerId);
            if (customerBonuses != null)
            {
                var maxWriteOffSum = commandContext.ShopOrderSum/100*customerBonuses.Program.MaxDiscountPercent;
                if (maxWriteOffSum < commandContext.WriteOffSum)
                    commandContext.WriteOffSum = maxWriteOffSum;

                if (customerBonuses.Sum < commandContext.WriteOffSum)
                    commandContext.WriteOffSum = customerBonuses.Sum;

                customerBonuses.Sum -= commandContext.WriteOffSum;
                
                session.Flush();
            }
        }
    }
}