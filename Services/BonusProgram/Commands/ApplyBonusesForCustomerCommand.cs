﻿using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using NHibernate.Util;
using Services.BonusProgram.CommandContexts;
using Services.BonusProgram.Criteria;
using Services.BonusProgram.Queries;
using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Commands
{
    public class ApplyBonusesForCustomerCommand : ICommand<ApplyBonusesForCustomerContext>
    {
        public void Execute(ApplyBonusesForCustomerContext commandContext)
        {
            var session = SessionManager.GetSession();
            
            var calcBonuses =
                new CalcBonusSumQuery().Ask(new CalcBonusSumCriterion
                {
                    CustomerId = commandContext.CustomerId,
                    ShopOrderSum = commandContext.ShopOrderSum
                });

            var customerBonuses =
                    session
                    .Query<CustomerBonus>()
                    .Where(cb => cb.Customer.Id == commandContext.CustomerId).ToList();

            foreach (var customerBonus in customerBonuses)
            {
                customerBonus.Sum += calcBonuses.ContainsKey(customerBonus.Program) ? calcBonuses[customerBonus.Program] : 0;
                calcBonuses.Remove(customerBonus.Program);
            }
            if (calcBonuses.Any())
            {
                var customer = session.Get<Domain.Customer>(commandContext.CustomerId);
                calcBonuses.ForEach(cb =>
                {
                    var customerBonus = new CustomerBonus {Customer = customer, Program = cb.Key, Sum = cb.Value};
                    session.Save(customerBonus);
                });

            }

            session.Flush();
        }
    }
}