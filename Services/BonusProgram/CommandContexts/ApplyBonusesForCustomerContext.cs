﻿using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.CommandContexts
{
    public class ApplyBonusesForCustomerContext : ICommandContext
    {
        public ulong CustomerId { get; set; }
        public decimal ShopOrderSum { get; set; }
    }
}