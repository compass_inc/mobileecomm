﻿using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.CommandContexts
{
    public class WriteOffBonusesForCustomerContext : ICommandContext
    {
        public ulong CustomerId { get; set; }
        public decimal ShopOrderSum { get; set; }
        public decimal WriteOffSum { get; set; }
    }
}