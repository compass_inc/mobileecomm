﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Query;
using Services.BonusProgram.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Queries
{
    public class CustomerBonusQuery : BaseObjectQuery<CustomerBonus>, IQuery<CustomerBonusCriterion,ICollection<CustomerBonus>>
    {
        public ICollection<CustomerBonus> Ask(CustomerBonusCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<CustomerBonus>();

            if (criterion.CustomerId > 0)
                q = q.Where(cb => cb.Customer.Id == criterion.CustomerId);

            return Ask(criterion, q).ToList();
        }
    }
}