﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using Domain.Enums;
using NHibernate.Linq;
using Services.BonusProgram.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Queries
{
    public class CalcBonusSumQuery :IQuery<CalcBonusSumCriterion,IDictionary<Domain.BonusProgram,decimal>>
    {
        public IDictionary<Domain.BonusProgram, decimal> Ask(CalcBonusSumCriterion criterion)
        {
            var session = SessionManager.GetSession();

            var programs =
                session.Query<Domain.BonusProgram>().Where(bp => bp.Status == BonusProgramState.Active).ToList();

            var res = new Dictionary<Domain.BonusProgram, decimal>();

            foreach (var program in programs)
            {
                var customerProgramBonuses =
                    session.Query<CustomerBonus>()
                        .FirstOrDefault(cb => cb.Customer.Id == criterion.CustomerId && cb.Program == program);

                var customerBonusSum = customerProgramBonuses == null ? 0 : customerProgramBonuses.Sum;

                var rule =
                    program.Rules.FirstOrDefault(
                        pr =>
                            (pr.MinShopOrderSum == null || pr.MinShopOrderSum <= criterion.ShopOrderSum) &&
                            (pr.MaxShopOrderSum == null || pr.MaxShopOrderSum >= criterion.ShopOrderSum) &&
                            (pr.StartBonusSum == null || pr.StartBonusSum <= customerBonusSum) &&
                            (pr.EndBonusSum == null || pr.EndBonusSum >= customerBonusSum));

                if (rule != null)
                    res.Add(program, criterion.ShopOrderSum/100*rule.ChargeInterest);
            }
            return res;
        }
    }
}