﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.BonusProgram.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Queries
{
    public class MaxWriteOffBonusSumQuery : IQuery<MaxWriteOffBonusSumCriterion, IDictionary<ulong, decimal>>
    {
        public IDictionary<ulong, decimal> Ask(MaxWriteOffBonusSumCriterion criterion)
        {
            return SessionManager.GetSession()
                .Query<CustomerBonus>()
                .Where(c => c.Customer.Id == criterion.CustomerId)
                .ToDictionary(cb => cb.Program.Id, cb => criterion.ShopOrderSum/100*cb.Program.MaxDiscountPercent);
        }
    }
}