﻿using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Criteria
{
    public class MaxWriteOffBonusSumCriterion : ICriterion
    {
        public decimal ShopOrderSum { get; set; }
        public ulong CustomerId { get; set; }
    }
}