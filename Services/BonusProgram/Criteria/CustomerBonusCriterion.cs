﻿using Services.Base.Criteria;

namespace Services.BonusProgram.Criteria
{
    public class CustomerBonusCriterion : BaseObjectCriterion
    {
        public ulong CustomerId { get; set; }
    }
}