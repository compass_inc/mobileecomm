﻿using Services.Infrastructure.Interfaces;

namespace Services.BonusProgram.Criteria
{
    public class CalcBonusSumCriterion : ICriterion
    {
        public ulong CustomerId { get; set; }
        public decimal ShopOrderSum { get; set; }
    }
}