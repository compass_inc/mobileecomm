﻿using System.Security.Cryptography;
using System.Text;
using Services.Infrastructure.Interfaces;

namespace Services.Crypt
{
    public class CreateHashQuery : IQuery<CreateHashCriterion,string>
    {
        public string Ask(CreateHashCriterion criterion)
        {
            return
                new UTF8Encoding().GetString(
                    new SHA1CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(criterion.Data)));
        }
    }
}