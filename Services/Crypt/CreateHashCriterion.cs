﻿using Services.Infrastructure.Interfaces;

namespace Services.Crypt
{
    public class CreateHashCriterion : ICriterion
    {
        public string Data { get; set; }
    }
}