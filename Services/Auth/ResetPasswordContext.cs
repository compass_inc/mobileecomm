﻿using Services.Infrastructure.Interfaces;

namespace Services.Auth
{
    public class ResetPasswordContext : ICommandContext
    {
        public string Login { get; set; }
    }
}