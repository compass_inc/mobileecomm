﻿using System;
using System.Linq;
using DataAccess;
using Domain.Interfaces;
using NHibernate.Linq;
using Services.Crypt;

namespace Services.Auth
{
    public class ResetPasswordCommand<T> where T: IAuthObject
    {
        public void Execute(ResetPasswordContext commandContext, out string newPassword)
        {
            if (string.IsNullOrEmpty(commandContext.Login))
                throw new ArgumentException("Не задана логин!");

            var session = SessionManager.GetSession();

            var authObject = session.Query<T>().FirstOrDefault(c => c.Login == commandContext.Login);
            if (authObject == null)
                throw new ArgumentException("Не верный логин!");

            var newPassWord = System.Web.Security.Membership.GeneratePassword(6, 2);

            authObject.Salt =
                new CreateHashQuery().Ask(new CreateHashCriterion
                {
                    Data = newPassWord
                });

            session.Update(authObject);
            session.Flush();

            newPassword = newPassWord;
        }
    }
}