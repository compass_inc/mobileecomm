﻿using Services.Infrastructure.Interfaces;

namespace Services.Auth
{
    public class ChangePasswordContext : ICommandContext
    {
        public ulong AuthObjectId { get; set; }
        public string NewPassword { get; set; }
    }
}