﻿using System;
using DataAccess;
using Domain.Interfaces;
using Services.Crypt;

namespace Services.Auth
{
    public class ChangePasswordCommand<T> where T : IAuthObject
    {
        public void Execute(ChangePasswordContext commandContext)
        {
            if (string.IsNullOrEmpty(commandContext.NewPassword))
                throw new Exception("Не задан новый пароль!");

            var session = SessionManager.GetSession();

            var authObj = session.Get<T>(commandContext.AuthObjectId);

            authObj.Salt = new CreateHashQuery().Ask(new CreateHashCriterion { Data = commandContext.NewPassword });

            session.Update(authObj);
            session.Flush();
        }
    }
}