﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Services.Base.Criteria;
using Services.Base.Query;
using Services.Infrastructure.Interfaces;

namespace Services.Infrastructure
{
    public class QueryForExtended<TResult> : IQueryFor<TResult>
    {
        private static readonly Dictionary<Type, Type> QueriesAndCriteriaDict = new Dictionary<Type, Type>
        {
            {typeof (BaseObjectCriterion), typeof (BaseObjectQuery<>)},
            {typeof (GenerateObjectFieldsCriterion<>), typeof (GenerateObjectFieldsQuery<>)},
            {typeof (ObjectCountCriterion<>), typeof (ObjectCountQuery<>)}
        };

        private readonly IQueryFactory _factory;

        public QueryForExtended(IQueryFactory factory)
        {
            _factory = factory;
        }

        public TResult With<TCriterion>(TCriterion criterion) where TCriterion : ICriterion
        {
            return
                (TResult)
                    GetType()
                        .GetMethod("WithInternal", BindingFlags.NonPublic | BindingFlags.Instance)
                        .MakeGenericMethod(criterion.GetType())
                        .Invoke(this, new object[] {criterion});
        }

        private Type GetLastNonGeneric(Type generic)
        {
            while (true)
            {
                if (generic.IsGenericType)
                {
                    generic = generic.GetGenericArguments().First();
                }
                else
                    return generic;
            }
        }

        private TResult WithInternal<TCriterion>(TCriterion criterion) where TCriterion : ICriterion
        {
            IQuery<TCriterion, TResult> query;

            try
            {
                query = _factory.Create<TCriterion, TResult>();
            }
            catch (Exception)
            {
                var criterionType = typeof (TCriterion);
                if (criterionType.IsGenericType)
                    criterionType = criterionType.GetGenericTypeDefinition();

                var domainType = typeof (TCriterion).IsGenericType
                    ? typeof (TCriterion).GetGenericArguments().First()
                    : typeof (TResult).IsGenericType 
                        ? typeof (TResult).GetGenericArguments().First() 
                        : typeof (TResult);

                query =
                    Activator.CreateInstance((QueriesAndCriteriaDict.ContainsKey(criterionType)
                        ? QueriesAndCriteriaDict[criterionType]
                        : typeof (BaseObjectQuery<>)).MakeGenericType(domainType)) as IQuery<TCriterion, TResult>;
            }

            if (query == null)
                throw new Exception("Не удалось подобрать Query");

            return query.Ask(criterion);
        }
    }
}