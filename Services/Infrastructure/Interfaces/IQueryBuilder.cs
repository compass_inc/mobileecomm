﻿namespace Services.Infrastructure.Interfaces
{
    public interface IQueryBuilder
    {
       IQueryFor<TResult> For<TResult>();
    }
}