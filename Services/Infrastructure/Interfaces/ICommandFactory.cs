﻿namespace Services.Infrastructure.Interfaces
{
    public interface ICommandFactory
    {
        ICommand<TCommandContext> Create<TCommandContext>() where TCommandContext : ICommandContext;
    }
}