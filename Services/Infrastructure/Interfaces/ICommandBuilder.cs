﻿namespace Services.Infrastructure.Interfaces
{
    public interface ICommandBuilder
    {
        void Execute<TCommandContext>(TCommandContext commandContext) where TCommandContext : ICommandContext;
    }
}