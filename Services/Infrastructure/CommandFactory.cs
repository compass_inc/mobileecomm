﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Windsor;
using Services.Base.CommandContexts;
using Services.Base.Commands;
using Services.Infrastructure.Interfaces;

namespace Services.Infrastructure
{
    public class CommandFactory : ICommandFactory
    {
        private static readonly Dictionary<Type, Type> CommandsAndContextDict = new Dictionary<Type, Type>
        {
            {typeof (CreateOrUpdateObjectContext<>), typeof (CreateOrUpdateObjectCommand<>)},
            {typeof (DeleteObjectContext<>), typeof (DeleteObjectCommand<>)}
        };


        private IWindsorContainer _container;
        public CommandFactory(IWindsorContainer container)
        {
            _container = container;
        }

        public ICommand<TCommandContext> Create<TCommandContext>() where TCommandContext : ICommandContext
        {
            ICommand<TCommandContext> res;
            try
            {
                res = _container.Resolve<ICommand<TCommandContext>>();
            }
            catch (Exception e)
            {
                var contextType = typeof (TCommandContext);
                if (contextType.IsGenericType &&
                    CommandsAndContextDict.ContainsKey(contextType.GetGenericTypeDefinition()))
                {
                    var commandType = CommandsAndContextDict[contextType.GetGenericTypeDefinition()];
                    var domainType = typeof (TCommandContext).GetGenericArguments().First();
                    res =
                        Activator.CreateInstance(
                            commandType.MakeGenericType(domainType)) as
                            ICommand<TCommandContext>;
                }
                else
                    throw e;
            }
            return res;
        }
    }
}
