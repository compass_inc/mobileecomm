﻿using Services.Infrastructure.Interfaces;

namespace Services.Customer.Criteria
{
    public class GetAuthTokenCriterion: ICriterion
    {
        public string Email { get; set; } 
    }
}