﻿using Services.Infrastructure.Interfaces;

namespace Services.Customer.Criteria
{
    public class ValidateCustomerPasswordCriterion : ICriterion
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}