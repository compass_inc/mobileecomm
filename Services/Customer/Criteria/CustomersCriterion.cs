﻿using System;
using Services.Base.Criteria;

namespace Services.Customer.Criteria
{
    public class CustomersCriterion: BaseObjectCriterion
    {
        public string Email { get; set; }
        public Guid? Token { get; set; }
    }
}