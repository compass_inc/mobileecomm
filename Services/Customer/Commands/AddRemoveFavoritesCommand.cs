﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Commands
{
    public class AddRemoveFavoritesCommand : ICommand<AddRemoveFavoritesContext>
    {
        public void Execute(AddRemoveFavoritesContext commandContext)
        {
            var session = SessionManager.GetSession();

            var customer = session.Get<Domain.Customer>(commandContext.CustomerId);
            if(customer.Favorites == null)
                customer.Favorites = new List<Domain.Product>();

            if (commandContext.AddingProducts != null && commandContext.AddingProducts.Any())
                session.Query<Domain.Product>()
                    .Where(p => commandContext.AddingProducts.Contains(p.Id))
                    .ToList()
                    .ForEach(p => customer.Favorites.Add(p));

            if (commandContext.RemovingProducts != null && commandContext.RemovingProducts.Any())
                session.Query<Domain.Product>()
                    .Where(p => commandContext.RemovingProducts.Contains(p.Id))
                    .ToList()
                    .ForEach(p => customer.Favorites.Remove(p));

            session.Flush();
        }
    }
}