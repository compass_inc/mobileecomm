﻿using System;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Commands
{
    public class SetAuthTokenCommand: ICommand<SetAuthTokenContext>
    {
        public void Execute(SetAuthTokenContext commandContext)
        {
            if(string.IsNullOrEmpty(commandContext.CustomerEmail))
                throw new Exception("не верный данные для задачи сессии авторизации");
            
            var session = SessionManager.GetSession();

            var customer = session.Query<Domain.Customer>().FirstOrDefault(c => c.Login == commandContext.CustomerEmail);
            if(customer == null)
                throw new Exception("не верные данные клиента для задачи сессии авторизации");

            session.SaveOrUpdate(new AuthSession {Customer = customer, Token = Guid.NewGuid()});
            session.Flush();
        }
    }
}