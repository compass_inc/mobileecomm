﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using DataAccess;
using Domain;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Commands
{
    public class AddRemoveAddressesCommand : ICommand< AddRemoveAddressesContext>
    {
        public void Execute(AddRemoveAddressesContext commandContext)
        {
            var session = SessionManager.GetSession();

            var customer = session.Get<Domain.Customer>(commandContext.CustomerId);
            if (customer.Addresses == null)
                customer.Addresses = new List<CustomerAddress>();

            if (commandContext.AddingAddresses != null && commandContext.AddingAddresses.Any())
                commandContext.AddingAddresses.ForEach(customer.Addresses.Add);

            if (commandContext.RemovingAddresses != null && commandContext.RemovingAddresses.Any())
                commandContext.RemovingAddresses.ForEach(a => customer.Addresses.Remove(a));

            session.Flush();
        }
    }
}