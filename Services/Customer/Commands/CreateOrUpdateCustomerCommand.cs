﻿using System;
using System.Linq;
using DataAccess;
using NHibernate.Linq;
using Services.Base.Commands;
using Services.Crypt;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Commands
{
    public class CreateOrUpdateCustomerCommand : CreateOrUpdateObjectCommand<Domain.Customer>,
        ICommand<CreateOrUpdateCustomerContext>
    {
        public void Execute(CreateOrUpdateCustomerContext commandContext)
        {
            var existCustomer = SessionManager.GetSession()
                .Query<Domain.Customer>()
                .FirstOrDefault(c => c.Login == commandContext.Object.Login);

            var newEmailExists = false;
            if (!string.IsNullOrEmpty(commandContext.NewLogin))
                newEmailExists =
                    SessionManager.GetSession().Query<Domain.Customer>().Any(c => c.Login == commandContext.NewLogin);

            if (commandContext.Object.Id == 0)
            {
                if (string.IsNullOrEmpty(commandContext.Password))
                    throw new ArgumentException("Не задан пароль!");

                if (string.IsNullOrEmpty(commandContext.Object.Login))
                    throw new ArgumentException("Не задана логин!");

                if (existCustomer == null)
                    commandContext.Object.Salt =
                        new CreateHashQuery().Ask(new CreateHashCriterion {Data = commandContext.Password});
                else
                    throw new ArgumentException("Указанный логин уже используется!");
            }
            else if ((existCustomer != null && existCustomer.Id != commandContext.Object.Id) || newEmailExists)
                throw new ArgumentException("Указанный логин уже используется!");

            if (existCustomer != null)
            {
                commandContext.Object.Salt = existCustomer.Salt;
                commandContext.Object.Orders = existCustomer.Orders;
                commandContext.Object.AuthSessions = existCustomer.AuthSessions;

                if (!string.IsNullOrEmpty(commandContext.NewLogin))
                    commandContext.Object.Login = commandContext.NewLogin;
            }

            base.Execute(commandContext);
        }
    }
}