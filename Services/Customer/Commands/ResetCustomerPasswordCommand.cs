﻿using Services.Auth;
using Services.Base.CommandContexts;
using Services.Base.Commands;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Commands
{
    public class ResetCustomerPasswordCommand : ResetPasswordCommand<Domain.Customer>,
        ICommand<ResetCustomerPasswordContext>
    {
        public void Execute(ResetCustomerPasswordContext commandContext)
        {
            string newPass;
            base.Execute(commandContext, out newPass);
            new SendMailCommand().Execute(new SendMailContext
            {
                Subject = "Восстановление пароля",
                Message = "Ваш новый пароль: " + newPass,
                Receiver = commandContext.Login
            });
        }
    }
}