﻿using Services.Auth;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Commands
{
    public class ChangeCustomerPasswordCommand : ChangePasswordCommand<Domain.Customer>,
        ICommand<ChangeCustomerPasswordContext>
    {
        public void Execute(ChangeCustomerPasswordContext commandContext)
        {
            base.Execute(commandContext);
        }
    }
}