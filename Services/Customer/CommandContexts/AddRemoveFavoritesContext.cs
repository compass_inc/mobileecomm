﻿using System.Collections.Generic;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.CommandContexts
{
    public class AddRemoveFavoritesContext : ICommandContext
    {
        public ulong CustomerId { get; set; }
        public ICollection<ulong> AddingProducts { get; set; }
        public ICollection<ulong> RemovingProducts { get; set; }
    }
}