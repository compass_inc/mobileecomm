﻿using Services.Base.CommandContexts;

namespace Services.Customer.CommandContexts
{
    public class CreateOrUpdateCustomerContext : CreateOrUpdateObjectContext<Domain.Customer>
    {
        public CreateOrUpdateCustomerContext(Domain.Customer obj) : base(obj)
        {
        }

        public string NewLogin { get; set; }

        public string Password { get; set; }
    }
}