﻿using System.Collections.Generic;
using Domain;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.CommandContexts
{
    public class AddRemoveAddressesContext : ICommandContext
    {
        public ulong CustomerId;
        public ICollection<CustomerAddress> AddingAddresses;
        public ICollection<CustomerAddress> RemovingAddresses;
    }
}