﻿using Services.Infrastructure.Interfaces;

namespace Services.Customer.CommandContexts
{
    public class SetAuthTokenContext : ICommandContext
    {
        public string CustomerEmail { get; set; }
    }
}