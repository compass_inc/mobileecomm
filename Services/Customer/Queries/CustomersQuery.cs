﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Base.Query;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Queries
{
    public class CustomersQuery : BaseObjectQuery<Domain.Customer>, IQuery<CustomersCriterion,ICollection<Domain.Customer>>
    {
        public ICollection<Domain.Customer> Ask(CustomersCriterion criterion)
        {
            if (criterion.Token != null)
            {
                var authSession =
                    SessionManager.GetSession().Query<AuthSession>().FirstOrDefault(s => s.Token == criterion.Token);
                return authSession == null
                    ? new List<Domain.Customer>()
                    : new List<Domain.Customer> {authSession.Customer};
            }

            var q = SessionManager.GetSession().Query<Domain.Customer>();

            if (!string.IsNullOrEmpty(criterion.Email))
                q = q.Where(c => c.Login == criterion.Email);

            return Ask(criterion,q).ToList();
        }
    }
}