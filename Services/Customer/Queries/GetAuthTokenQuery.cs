﻿using System;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Queries
{
    public class GetAuthTokenQuery : IQuery<GetAuthTokenCriterion,Guid?>
    {
        public Guid? Ask(GetAuthTokenCriterion criterion)
        {
            var authSession =
                SessionManager.GetSession()
                    .Query<AuthSession>()
                    .FirstOrDefault(s => s.Customer.Login == criterion.Email);
            return authSession == null ? new Guid?() : authSession.Token;
        }
    }
}