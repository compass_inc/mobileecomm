﻿using System;
using System.Linq;
using DataAccess;
using Domain.Enums;
using NHibernate.Linq;
using Services.Crypt;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;

namespace Services.Customer.Queries
{
    public class ValidateCustomerPasswordQuery: IQuery<ValidateCustomerPasswordCriterion,bool>
    {
        public bool Ask(ValidateCustomerPasswordCriterion criterion)
        {
            if (string.IsNullOrEmpty(criterion.Email))
                throw new ArgumentException("Не указан электронный адрес!");

            if (string.IsNullOrEmpty(criterion.Password))
                throw new ArgumentException("Не указан пароль!");

            var customer =
                SessionManager.GetSession().Query<Domain.Customer>().FirstOrDefault(c => c.Login == criterion.Email);

            if (customer == null)
                return false;

            if(customer.Status == CustomerState.Blocked)
                throw new ApplicationException("Пользователь заблокирован!");

            return customer.Salt == new CreateHashQuery().Ask(new CreateHashCriterion {Data = criterion.Password});
        }
    }
}