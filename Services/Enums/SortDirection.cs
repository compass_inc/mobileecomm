﻿namespace Services.Enums
{
    public enum SortDirection
    {
        Asc,
        Desc
    }
}