﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Reviews.Criteria;

namespace Services.Reviews.Queries
{
    public class ReviewsQuery : IQuery<ReviewsCriterion,ICollection<Review>>
    {
        public ICollection<Review> Ask(ReviewsCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Review>();

            if (criterion.ProductId > 0)
                q = q.Where(r => r.Product.Id == criterion.ProductId);
            if (criterion.PlaceId > 0)
                q = q.Where(r => r.Place.Id == criterion.PlaceId);

            return q.ToList();
        }
    }
}