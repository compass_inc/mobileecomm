﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain;
using Domain.Enums;
using NHibernate.Linq;
using Services.Infrastructure.Interfaces;
using Services.Reviews.Criteria;

namespace Services.Reviews.Queries
{
    public class ActiveReviewsQuery : IQuery<ActiveReviewsCriterion,ICollection<Review>>
    {
        public ICollection<Review> Ask(ActiveReviewsCriterion criterion)
        {
            var q = SessionManager.GetSession().Query<Review>();
            if (criterion.ProductId > 0)
                q = q.Where(r => r.Product.Id == criterion.ProductId && r.Status == ReviewState.Active);
            if (criterion.PlaceId > 0)
                q = q.Where(r => r.Place.Id == criterion.PlaceId && r.Status == ReviewState.Active);

            var reviews = q.ToList();

            var reviewsIds = reviews.Select(r => r.Id).ToList();

            q = SessionManager.GetSession().Query<Review>();
            reviews.AddRange(q.Where(pr => !reviewsIds.Contains(pr.Id) && reviewsIds.Contains(pr.Parent.Id)).ToList());

            return reviews;
        }
    }
}