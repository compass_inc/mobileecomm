﻿using Services.Infrastructure.Interfaces;

namespace Services.Reviews.Criteria
{
    public class ReviewsCriterion : ICriterion
    {
        public ulong ProductId { get; set; }
        public ulong PlaceId { get; set; } 
    }
}