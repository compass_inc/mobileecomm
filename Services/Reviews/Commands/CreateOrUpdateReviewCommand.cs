﻿using System;
using Domain;
using Domain.Enums;
using Services.Base.CommandContexts;
using Services.Base.Commands;

namespace Services.Reviews.Commands
{
    public class CreateOrUpdateReviewCommand : CreateOrUpdateObjectCommand<Review>
    {
        public override void Execute(CreateOrUpdateObjectContext<Review> commandContext)
        {
            if (commandContext.Object.Id == 0)
            {
                commandContext.Object.Status = ReviewState.Moderate;
                commandContext.Object.CreationDate = DateTime.Now;
            }

            base.Execute(commandContext);
        }
    }
}