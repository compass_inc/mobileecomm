﻿using System.Data;
using NHibernate.Dialect;

namespace DataAccess.Dialects
{
    public class MsSqlDialect : MsSql2008Dialect
    {
        public MsSqlDialect()
        {
            RegisterColumnType(DbType.UInt64, "BIGINT");
        }
    }
}