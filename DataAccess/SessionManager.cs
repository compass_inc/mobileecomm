﻿using System.Configuration;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Web;
using NHibernate;
using NHibernate.Cfg.ConfigurationSchema;
using NHibernate.Mapping.ByCode;
using Configuration = NHibernate.Cfg.Configuration;

namespace DataAccess
{
    public sealed class SessionManager
    {
        private const string CurrentSessionKey = "nhibernate.current_session";
        private static readonly ISessionFactory SessionFactory;

        static SessionManager()
        {
            var conf = new Configuration().Configure();

            var mapper = new ModelMapper();
            var hibernateConfiguration = ConfigurationManager.GetSection("hibernate-configuration") as HibernateConfiguration;
            if (hibernateConfiguration != null)
            {
                foreach (var map in hibernateConfiguration.SessionFactory.Mappings)
                {
                    mapper.AddMappings(Assembly.Load(map.Assembly).GetTypes());
                }
            }
            conf.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());
            SessionFactory = conf.BuildSessionFactory();
        }

        public static ISession GetSession()
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                var threadCurrentSession = CallContext.GetData("ECommSession") as ISession;
                if (threadCurrentSession == null)
                {
                    threadCurrentSession = SessionFactory.OpenSession();
                    CallContext.SetData("ECommSession", threadCurrentSession);
                }
                return threadCurrentSession;
            }

            var currentSession = context.Items[CurrentSessionKey] as ISession;

            if (currentSession == null)
            {
                currentSession = SessionFactory.OpenSession();
                context.Items[CurrentSessionKey] = currentSession;
            }

            return currentSession;
        }

        public static void CloseSession()
        {
            var context = HttpContext.Current;
            var currentSession = context.Items[CurrentSessionKey] as ISession;

            if (currentSession == null)
            {
                // No current session
                return;
            }

            currentSession.Close();
            context.Items.Remove(CurrentSessionKey);
        }

        public static void CloseSessionFactory()
        {
            if (SessionFactory != null)
            {
                SessionFactory.Close();
            }
        }
    }
}