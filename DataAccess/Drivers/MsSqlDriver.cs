﻿using NHibernate.Driver;
using NHibernate.SqlTypes;

namespace DataAccess.Drivers
{
    public class MsSqlDriver : Sql2008ClientDriver
    {

        protected override void InitializeParameter(System.Data.IDbDataParameter dbParam, string name, SqlType sqlType)
        {
            if (Equals(sqlType, SqlTypeFactory.UInt64)) sqlType = SqlTypeFactory.Int64;

            base.InitializeParameter(dbParam, name, sqlType);
        }

    }
}