﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class PriceMap : BaseObjectMap<Price>
    {
        public PriceMap()
        {
            Property(o=>o.Value);
            Property(o => o.Value2);
            Property(o=>o.ValueCalc, mapper => mapper.Formula("CASE WHEN Value2 > 0 THEN Value2 ELSE Value END"));
            
            Property(o=>o.Description);

            ManyToOne(p => p.Product, mapper => mapper.Lazy(LazyRelation.NoProxy));

            Set(o => o.ShopOrderLines, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
            }, relation => relation.OneToMany());
        }
    }
}