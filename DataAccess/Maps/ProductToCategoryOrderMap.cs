﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ProductToCategoryOrderMap : BaseObjectMap<ProductToCategoryOrder>
    {
        public ProductToCategoryOrderMap()
        {
            ManyToOne(m => m.SourceProduct, delegate(IManyToOneMapper mapper)
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Fetch(FetchKind.Join);
            });
            ManyToOne(m => m.KeyCategory, delegate(IManyToOneMapper mapper)
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Fetch(FetchKind.Join);
            });
        }
    }
}