﻿using Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Maps
{
    public class AuthSessionMap : ClassMapping<AuthSession>
    {
        public AuthSessionMap()
        {
            Id(o => o.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
            });

                ManyToOne(o=> o.Customer, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });
            Property(o => o.Token);
        }
    }
}