﻿using Domain;

namespace DataAccess.Maps
{
    public class OrderStateMap : BaseObjectMap<ShopOrderState>
    {
        public OrderStateMap()
        {
            Property(o=>o.IsDefault);
            Property(o=>o.Name);
        }
    }
}