﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
	public class CustomObjectMap : BaseObjectMap<CustomObject>
	{
		public CustomObjectMap()
		{
			ManyToOne(o => o.Type, mapper => mapper.Lazy(LazyRelation.NoProxy));

			Set(o => o.Fields, mapper =>
			{
				mapper.Lazy(CollectionLazy.Extra);
				mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
			}, relation => relation.OneToMany());
		}
	}
}
