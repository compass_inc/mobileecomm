﻿using Domain;

namespace DataAccess.Maps
{
    public class ImageTypeMap : BaseObjectMap<ImageType>
    {
        public ImageTypeMap()
        {
            Property(o=>o.Name);
        }
    }
}