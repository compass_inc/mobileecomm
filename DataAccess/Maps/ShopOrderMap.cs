﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ShopOrderMap : BaseObjectMap<ShopOrder>
    {
        public ShopOrderMap()
        {
            Property(o => o.CreationDate);
            Property(o => o.CustomerMail);
            Property(o => o.CustomerName);
            Property(o => o.CustomerPhone);
            Property(o => o.IsPickup);
            Property(o => o.OrderSum);
            Property(o => o.CustomerAddress);
            Property(o=>o.WriteOffBonusSum);
            Property(o => o.DeliveryTime);

            ManyToOne(o => o.Customer, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            ManyToOne(o => o.Status, mapper => mapper.Lazy(LazyRelation.NoLazy));

            ManyToOne(o => o.Coupon, mapper =>
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Cascade(Cascade.None);
            });

            ManyToOne(o => o.BonusProgram, mapper =>
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Cascade(Cascade.None);
            });

            Set(o => o.Lines, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Set(o => o.Fields, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());
        }
    }
}