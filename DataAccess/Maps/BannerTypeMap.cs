﻿using Domain;

namespace DataAccess.Maps
{
    public class BannerTypeMap : BaseObjectMap<BannerType>
    {
        public BannerTypeMap()
        {
            Property(o=>o.Name);  
        }
    }
}