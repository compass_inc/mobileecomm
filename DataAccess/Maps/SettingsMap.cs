﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class SettingsMap : BaseObjectMap<Settings>
    {
        public SettingsMap()
        {
            Property(o=>o.BackGroundColor);
            Property(o => o.HeaderFooterColor);
            Property(o=>o.ShopOrderEmail);
            Property(o => o.FromEmail);
            Property(o => o.ShopName);
            Property(o=>o.FreeDeliveryPercentNotify);
            Property(o=>o.BgFontColor);
            Property(o => o.HfFontColor);
            Property(o => o.UseGravatar);
            Property(o => o.OnlyCity);
            Property(o => o.NoOrderMode);
            Property(o => o.ShowFilialInfoInProduct);
            Property(o => o.AuthRequired);
            Property(o => o.OnePageCatalog);
            Property(o => o.SideMainMenu);
            Property(o => o.NoComments);
            Property(o => o.StartPageBannerCode);

            ManyToOne(o => o.LogoImage, mapper => mapper.Lazy(LazyRelation.NoLazy));
            ManyToOne(o => o.ReviewDefaultImage, mapper => mapper.Lazy(LazyRelation.NoLazy));

            Set(o => o.CategoriesColors, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

        }
    }

    public class CategoryColorMap : BaseObjectMap<CategoryColor>
    {
        public CategoryColorMap()
        {
            Property(o=>o.BackGroundColor);
            Property(o => o.HeaderFooterColor);
            Property(o => o.BgFontColor);
            Property(o => o.HfFontColor);

            ManyToOne(o => o.Category, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
                mapper.Unique(true);
             
            });
        }
    }
}