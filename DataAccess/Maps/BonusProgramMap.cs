﻿using Domain;
using Domain.Enums;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class BonusProgramMap : BaseObjectMap<BonusProgram>
    {
        public BonusProgramMap()
        {
            Property(o => o.Name);
            Property(o => o.Description);
            Property(o => o.MaxDiscountPercent);
            Property(o => o.Status, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<BonusProgramState>>();
                mapper.NotNullable(true);
            });

            Set(o => o.Rules, mapper =>
            {
                mapper.Lazy(CollectionLazy.NoLazy);
                mapper.Fetch(CollectionFetchMode.Join);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Property(o => o.Status, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<BonusProgramState>>();
                mapper.NotNullable(true);
            });
        }
    }

    public class BonusProgramRuleMap : BaseObjectMap<BonusProgramRule>
    {
        public BonusProgramRuleMap()
        {
            Property(o => o.ChargeInterest);
            Property(o => o.MinShopOrderSum);
            Property(o => o.MaxShopOrderSum);
            Property(o => o.StartBonusSum);
            Property(o => o.EndBonusSum);
        }
    }
}