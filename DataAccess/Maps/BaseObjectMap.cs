﻿using Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Maps
{
    public class BaseObjectMap<T> : ClassMapping<T> where T: BaseObject
    {
        public BaseObjectMap()
        {
            
            Table(typeof(T).Name);

            Id(o => o.Id, mapper =>
            {
                mapper.Generator(Generators.Identity);
                mapper.Column((typeof (T).IsGenericType
                    ? typeof (T).GetGenericTypeDefinition()
                        .Name.Remove(typeof (T).GetGenericTypeDefinition().Name.IndexOf('`'))
                    : typeof (T).Name) + "_Id");
            });

            Property(o=>o.SortOrder);
            Property(o=>o.Code);
        }
    }
}