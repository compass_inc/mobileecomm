﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class LocaleStringMap : BaseObjectMap<LocaleString>
    {
        public LocaleStringMap()
        {
            Property(x => x.Name);
            ManyToOne(o => o.Type, mapper => mapper.Lazy(LazyRelation.NoProxy));

            Set(x => x.LocaleValues, mapper =>
            {
                mapper.Lazy(CollectionLazy.NoLazy);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                mapper.Fetch(CollectionFetchMode.Join);
            }, relation => relation.OneToMany());
        }
    }
}