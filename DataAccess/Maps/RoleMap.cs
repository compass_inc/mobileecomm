﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class RoleMap:BaseObjectMap<Role>
    {
        public RoleMap()
        {
            Property(o=>o.Name);
            Set(o => o.Users, mapper =>
            {
                mapper.Table("User_Roles");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("role_users_key");
                    keyMapper.Column("role_users_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("user_roles_key");
                mapper.ForeignKey("user_roles_key");
            }));
        }
    }
}