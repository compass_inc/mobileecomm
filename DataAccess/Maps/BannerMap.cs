﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class BannerMap : BaseObjectMap<Banner>
    {
        public BannerMap()
        {
            Property(o=>o.Name);
            Set(o => o.Images, delegate(ISetPropertiesMapper<Banner, Image> mapper)
            {
                mapper.Table("Banner_Images");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper => keyMapper.ForeignKey("banner_key"));
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("image_key");
                mapper.ForeignKey("image_key");
            }));

            Set(o => o.Products, delegate(ISetPropertiesMapper<Banner, Product> mapper)
            {
                mapper.Table("Product_Labels");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("banner_labels_key");
                    keyMapper.Column("banner_labels_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("product_labels_key");
                mapper.ForeignKey("product_labels_key");
            }));

            ManyToOne(o => o.Type, mapper => mapper.Lazy(LazyRelation.NoProxy));
        }

    }
}