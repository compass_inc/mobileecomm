﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class LocaleStringValueMap : BaseObjectMap<LocaleStringValue>
    {
        public LocaleStringValueMap()
        {
            Property(x => x.Value);
            Property(x => x.Locale);
            ManyToOne(x => x.LocaleString, mapper => mapper.Lazy(LazyRelation.NoProxy));
        }
    }
}