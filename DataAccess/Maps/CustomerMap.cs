﻿using Domain;
using Domain.Enums;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class CustomerMap : BaseObjectMap<Customer>
    {
        public CustomerMap()
        {
            Property(o => o.Name);
            Property(o => o.Phone);
            Property(o => o.Login, mapper => mapper.Unique(true));
            Property(o => o.Email, mapper => mapper.Unique(true));
            Property(o => o.Salt);
            Property(o => o.Status, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<CustomerState>>();
                mapper.NotNullable(true);
            });

            ManyToOne(o => o.Avatar, mapper =>
            {
                mapper.Cascade(Cascade.Persist);
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            Set(o => o.AuthSessions, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Set(o => o.Addresses, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Set(o => o.Orders, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
            }, relation => relation.OneToMany());

            Set(o => o.Favorites, mapper =>
            {
                mapper.Table("Customer_FavoriteProduct");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.Column("customer_favproduct_key");
                    keyMapper.ForeignKey("customer_favproduct_key");
                });

            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.ForeignKey("favproduct_customer_key");
                mapper.Column("favproduct_customer_key");
            }));
        }
    }

    public class CustomerAddressMap : BaseObjectMap<CustomerAddress>
    {
        public CustomerAddressMap()
        {
            Property(o => o.Description);
            Property(o => o.City);
            Property(o => o.House);
            Property(o => o.Street);
            Property(o => o.FrontDoor);

            ManyToOne(o => o.Customer, mapper => mapper.Lazy(LazyRelation.NoProxy));
        }
    }
}