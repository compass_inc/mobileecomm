﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class CustomerBonusMap : BaseObjectMap<CustomerBonus>
    {
        public CustomerBonusMap()
        {
            Property(o => o.Sum);
            ManyToOne(o => o.Customer, mapper =>
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Fetch(FetchKind.Join);
            });
            ManyToOne(o => o.Program, mapper =>
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Fetch(FetchKind.Join);
            });
        }
    }
}