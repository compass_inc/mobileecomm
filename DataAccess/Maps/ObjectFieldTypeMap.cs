﻿using Domain;
using Domain.Enums;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ObjectFieldTypeMap<T> where T : BaseObject
    {
        public static void MakeMap(BaseObjectMap<ObjectFieldType<T>> mapObj)
        {
            mapObj.Table(typeof (T).Name + "_FieldsType");
            mapObj.Property(o => o.Name);
            mapObj.Property(o => o.IsRequired);
            mapObj.Property(o => o.TypeName, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<FieldType>>();
                mapper.NotNullable(true);
            });

            mapObj.Set(o => o.Fields, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Inverse(true);
                mapper.Key(keyMapper => keyMapper.OnDelete(OnDeleteAction.Cascade));
            }, relation => relation.OneToMany());
        }
    }

    public class OrderFieldTypeMap : BaseObjectMap<ObjectFieldType<ShopOrder>>
    {
        public OrderFieldTypeMap()
        {
            ObjectFieldTypeMap<ShopOrder>.MakeMap(this);
        }
    }

    public class ProductFieldTypeMap : BaseObjectMap<ObjectFieldType<Product>>
    {
        public ProductFieldTypeMap()
        {
            ObjectFieldTypeMap<Product>.MakeMap(this);
        }
    }

	public class CustomObjectFieldTypeMap : BaseObjectMap<ObjectFieldType<CustomObject>>
	{
		public CustomObjectFieldTypeMap()
		{
			ObjectFieldTypeMap<CustomObject>.MakeMap(this);
		}
	}
}