﻿using Domain;
using Domain.Enums;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class UserMap : BaseObjectMap<User>
    {
        public UserMap()
        {
            Table("Adm_User");
            Property(o => o.Login, mapper => mapper.Unique(true));
            Property(o => o.Email);
            Property(o => o.Name);
            Property(o => o.Salt);
            Property(o => o.Status, delegate (IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<UserState>>();
                mapper.NotNullable(true);
            });

            Set(o => o.Roles, mapper =>
            {
                mapper.Table("User_Roles");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("user_roles_key");
                    keyMapper.Column("user_roles_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("role_users_key");
                mapper.ForeignKey("role_users_key");
            }));
        }
    }
}