﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class PlaceMap : BaseObjectMap<Place>
    {
        public PlaceMap()
        {
            Property(o => o.Name);

            Property(o => o.City);
            Property(o => o.Street);
            Property(o => o.House);
            Property(o => o.Phone);
            Property(o => o.DeliveryStartTime);
            Property(o => o.DeliveryEndTime);
            Property(o => o.DeliveryFreeSum);
            Property(o => o.MinOrderSum);
            Property(o => o.WorkStartTime);
            Property(o => o.WorkEndTime);
            Property(o => o.MinDeliverySum);
            Property(o => o.MinDeliveryTime);
            Property(o => o.Coordinates);
            Property(o => o.Email);

            ManyToOne(o => o.HeaderLogo, mapper => mapper.Lazy(LazyRelation.NoProxy));
            ManyToOne(o => o.MainLogo, mapper => mapper.Lazy(LazyRelation.NoProxy));

            Set(o => o.Products, delegate(ISetPropertiesMapper<Place, Product> mapper)
            {
                mapper.Table("Product_Places");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("place_products_key");
                    keyMapper.Column("place_products_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("product_places_key");
                mapper.ForeignKey("product_places_key");
            }));
        }
    }
}