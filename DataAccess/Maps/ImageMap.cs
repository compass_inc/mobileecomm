﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ImageMap : BaseObjectMap<Image>
    {
        public ImageMap()
        {
            Property(o => o.Description);
            Property(o => o.Path);
            Property(o => o.SavePath);

            ManyToOne(o => o.Type, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            Set(o => o.Banners, delegate(ISetPropertiesMapper<Image, Banner> mapper)
            {
                mapper.Table("Banner_Images");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper => keyMapper.ForeignKey("image_key"));
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("banner_key");
                mapper.ForeignKey("banner_key");
            }));

            Set(o => o.Products, delegate(ISetPropertiesMapper<Image, Product> mapper)
            {
                mapper.Table("Product_Images");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("image_products_key");
                    keyMapper.Column("image_products_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("product_images_key");
                mapper.ForeignKey("product_images_key");
            }));

            Set(o => o.Categories, mapper => mapper.Lazy(CollectionLazy.Extra), relation => relation.OneToMany());
            Set(o => o.Customers, mapper => mapper.Lazy(CollectionLazy.Extra), relation => relation.OneToMany());
        }
    }
}