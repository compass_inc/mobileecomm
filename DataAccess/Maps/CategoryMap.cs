﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class CategoryMap : BaseObjectMap<Category>
    {
        public CategoryMap()
        {
            Property(o => o.Name);
            ManyToOne(o => o.Image, mapper => mapper.Lazy(LazyRelation.NoProxy));
            ManyToOne(o => o.Parent, mapper => mapper.Lazy(LazyRelation.NoProxy));
            Set(o => o.Colors, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Inverse(true);
                mapper.Key(keyMapper => keyMapper.OnDelete(OnDeleteAction.Cascade));
            }, relation => relation.OneToMany());

            Set(o => o.Products, delegate(ISetPropertiesMapper<Category, Product> mapper)
            {
                mapper.Table("Product_Categories");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper => keyMapper.ForeignKey("category_key"));
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("product_key");
                mapper.ForeignKey("product_key");
            }));

            Set(o => o.ProdToCatOrders, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All);
            }, relation => relation.OneToMany());
        }
    }
}