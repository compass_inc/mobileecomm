﻿using Domain;
using Domain.Enums;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class CouponMap : BaseObjectMap<Coupon>
    {
        public CouponMap()
        {
            Property(o=>o.Name);
            Property(o=>o.EndDate);
            Property(o => o.UseCount);
            Property(o => o.MaxUseCount);
            Property(o => o.Status, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<CouponState>>();
                mapper.NotNullable(true);
            });
            Property(o => o.Description);
            Property(o=>o.DiscountValue);
            Set(o=>o.Products, mapper =>
            {
                mapper.Table("Product_Coupons");
                mapper.Lazy(CollectionLazy.NoLazy);
                mapper.Fetch(CollectionFetchMode.Join);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper => keyMapper.ForeignKey("coupon_key"));
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("product_key");
                mapper.ForeignKey("product_coupon_key");
            }));

            Set(o=>o.ShopOrdersUsedIn, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
            }, relation => relation.OneToMany());
        }
    }
}