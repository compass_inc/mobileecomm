﻿using Domain;

namespace DataAccess.Maps
{
    public class ProductTypeMap : BaseObjectMap<ProductType>
    {
        public ProductTypeMap()
        {
            Property(o => o.Name);
        }
    }
}