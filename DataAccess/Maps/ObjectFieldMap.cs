﻿using Domain;
using NHibernate;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ObjectFieldMap<T> where T:BaseObject
    {
        public static void MakeMap(BaseObjectMap<ObjectField<T>> mapObj)
        {
            mapObj.Table(typeof(T).Name+"_Fields");
            mapObj.Property(o=>o.Value, mapper=> mapper.Type(NHibernateUtil.StringClob));
            mapObj.ManyToOne(o=>o.FieldType, mapper => mapper.Lazy(LazyRelation.NoProxy));
        }
    }

    public class OrderFieldMap : BaseObjectMap<ObjectField<ShopOrder>>
    {
        public OrderFieldMap()
        {
            ObjectFieldMap<ShopOrder>.MakeMap(this);
        }   
    }

    public class ProductFieldMap : BaseObjectMap<ObjectField<Product>>
    {
        public ProductFieldMap()
        {
            ObjectFieldMap<Product>.MakeMap(this);
        }
    }

	public class CustomObjectFieldMap : BaseObjectMap<ObjectField<CustomObject>>
	{
		public CustomObjectFieldMap()
		{
			ObjectFieldMap<CustomObject>.MakeMap(this);
		}
	}
}