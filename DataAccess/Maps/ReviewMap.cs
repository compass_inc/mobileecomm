﻿using Domain;
using Domain.Enums;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ReviewMap :BaseObjectMap<Review>
    {
        public ReviewMap()
        {
            ManyToOne(o => o.Customer, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });
            Property(o => o.CustomerName);
            Property(o => o.CustomerEMail);

            Property(o => o.Status, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<ReviewState>>();
                mapper.NotNullable(true);
            });

            ManyToOne(o => o.Product, mapper =>
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Fetch(FetchKind.Join);
            });
            ManyToOne(o => o.Place, mapper =>
            {
                mapper.Lazy(LazyRelation.NoProxy);
                mapper.Fetch(FetchKind.Join);
            });
            Property(o => o.Rate);
            Property(o => o.Description);
            Property(o => o.Title);
            Property(o => o.IsOwnerAnswer);
            Property(o => o.CreationDate);

            Set(o => o.Childs, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Key(keyMapper => keyMapper.ForeignKey("Parent"));
            }, relation => relation.OneToMany());
            ManyToOne(o => o.Parent, mapper => mapper.Lazy(LazyRelation.NoProxy));
        }
    }
}