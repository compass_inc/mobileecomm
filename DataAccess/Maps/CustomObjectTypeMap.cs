﻿using Domain;

namespace DataAccess.Maps
{
	public class CustomObjectTypeMap : BaseObjectMap<CustomObjectType>
	{
		public CustomObjectTypeMap()
		{
			Property(o => o.Name);
		}
	}
}
