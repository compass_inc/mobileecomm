﻿using Domain;
using Domain.Enums;
using NHibernate;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ProductMap : BaseObjectMap<Product>
    {
        public ProductMap()
        {
            Property(o => o.Name, mapper => mapper.Type(NHibernateUtil.StringClob));
            Property(o => o.ShortDescription, mapper => mapper.Type(NHibernateUtil.StringClob));
            Property(o => o.LongDescription, mapper => mapper.Type(NHibernateUtil.StringClob));
            Property(o => o.Rate);
            Property(o=>o.EndActivityDate);

            ManyToOne(o => o.Type, mapper => mapper.Lazy(LazyRelation.NoProxy));

            Property(o => o.Status, delegate(IPropertyMapper mapper)
            {
                mapper.Type<NHibernate.Type.EnumStringType<ProductState>>();
                mapper.NotNullable(true);
            });

            Set(o => o.FavoritesCustomers, delegate(ISetPropertiesMapper<Product, Customer> mapper)
            {
                mapper.Table("Customer_FavoriteProduct");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("favproduct_customer_key");
                    keyMapper.Column("favproduct_customer_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("customer_favproduct_key");
                mapper.ForeignKey("customer_favproduct_key");
            }));

            Set(o => o.Featured, delegate(ISetPropertiesMapper<Product, Product> mapper)
            {
                mapper.Table("Product_Featured");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("featuredowner_key");
                    keyMapper.Column("featuredowner_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("featured_key");
                mapper.ForeignKey("featured_key");
            }));

            Set(o => o.FeaturedOwners, delegate(ISetPropertiesMapper<Product, Product> mapper)
            {
                mapper.Table("Product_Featured");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("featured_key");
                    keyMapper.Column("featured_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("featuredowner_key");
                mapper.ForeignKey("featuredowner_key");
            }));

            Set(o => o.Places, delegate(ISetPropertiesMapper<Product, Place> mapper)
            {
                mapper.Table("Product_Places");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("product_places_key");
                    keyMapper.Column("product_places_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("place_products_key");
                mapper.ForeignKey("place_products_key");
            }));

            Set(o => o.Images, delegate(ISetPropertiesMapper<Product, Image> mapper)
            {
                mapper.Table("Product_Images");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.Persist);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("product_images_key");
                    keyMapper.Column("product_images_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("image_products_key");
                mapper.ForeignKey("image_products_key");
            }));

            Set(o => o.Labels, delegate(ISetPropertiesMapper<Product, Banner> mapper)
            {
                mapper.Table("Product_Labels");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper =>
                {
                    keyMapper.ForeignKey("product_labels_key");
                    keyMapper.Column("product_labels_key");
                });
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("banner_labels_key");
                mapper.ForeignKey("banner_labels_key");
            }));

            Set(o => o.Prices, delegate(ISetPropertiesMapper<Product, Price> mapper)
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Set(o => o.Categories, delegate(ISetPropertiesMapper<Product, Category> mapper)
            {
                mapper.Table("Product_Categories");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper => keyMapper.ForeignKey("product_key"));
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("category_key");
                mapper.ForeignKey("category_key");
            }));

            Set(o => o.Coupons, mapper =>
            {
                mapper.Table("Product_Coupons");
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
                mapper.Key(keyMapper => keyMapper.ForeignKey("product_coupon_key"));
            }, relation => relation.ManyToMany(mapper =>
            {
                mapper.Column("coupon_key");
                mapper.ForeignKey("coupon_key");
            }));

            Set(o => o.CategoriesOrders, delegate(ISetPropertiesMapper<Product, ProductToCategoryOrder> mapper)
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Set(o => o.Fields, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }, relation => relation.OneToMany());

            Set(o => o.ShopOrderLines, mapper =>
            {
                mapper.Lazy(CollectionLazy.Extra);
                mapper.Cascade(Cascade.None);
            }, relation => relation.OneToMany());
        }
    }
}