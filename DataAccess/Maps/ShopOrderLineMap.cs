﻿using Domain;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Maps
{
    public class ShopOrderLineMap : BaseObjectMap<ShopOrderLine>
    {
        public ShopOrderLineMap()
        {
            Property(m => m.DeliveryDescription);
            Property(m => m.DeliveryValue);
            Property(m => m.DiscountDescription);
            Property(m => m.DiscountValue);
            Property(m => m.ProductAmount);
            Property(m => m.ProductName);
            Property(m => m.ProductPriceDescription);
            Property(m => m.ProductPriceValue);

            ManyToOne(m => m.ShopOrder, mapper => mapper.Lazy(LazyRelation.NoProxy));
            ManyToOne(m => m.Product, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });

            ManyToOne(m => m.ProductPrice, mapper =>
            {
                mapper.Lazy(LazyRelation.NoLazy);
                mapper.Fetch(FetchKind.Join);
            });
        } 
    }
}