﻿using Domain;

namespace DataAccess.Maps
{
    public class LocaleStringTypeMap :BaseObjectMap<LocaleStringType>
    {
        public LocaleStringTypeMap()
        {
            Property(o => o.Name);
        }
    }
}