﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccess;
using Domain;
using Domain.Dto;
using DomainTranslators.Customers;
using DomainTranslators.ShopOrders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate.Linq;
using Services.Base.CommandContexts;
using Services.Base.Commands;
using Services.Category.Commands;
using Services.Coupon.Criteria;
using Services.Coupon.Queries;
using Services.Customer.CommandContexts;
using Services.Customer.Commands;
using Services.Settings.commandContexts;
using Services.Settings.Commands;
using Services.ShopOrder.CommandContext;
using Services.ShopOrder.Commands;

namespace Tests
{
    [TestClass]
    public class CommandTests
    {
        [TestMethod]
        public void TestChangeOrderCommand()
        {
            new SetCategoryOrderCommand().Execute(new SetObjectOrderContext<Category>(3L,15L));
        }

        [TestMethod]
        public void TestValidateShopOrderCreateOrUpdateCommand()
        {

            //new CreateOrUpdateShopOrderCommand().Execute(new CreateOrUpdateShopOrderContext(
            //    (new ShopOrderDtoToShopOrder().Map(new ShopOrderDto
            //    {
            //        CustomerMail = "test",
            //        CustomerAddress = "test",
            //        Lines =
            //          new List<ShopOrderLineDto> { new ShopOrderLineDto { ProductId = 1, ProductAmount = 1 }}
            //    })
            //));

            new CreateOrUpdateShopOrderCommand().Execute(new CreateOrUpdateShopOrderContext(new ShopOrder
            {
                CustomerName = "test2",
                CustomerPhone = "test2",
                CustomerMail = "Art@mail.ru"
            }));

            //new CreateOrUpdateShopOrderCommand().Execute(
            //    new CreateOrUpdateObjectContext<ShopOrder>(new ShopOrderDtoToShopOrder().Map(new ShopOrderDto()
            //    {
            //        CustomerName = "test2",
            //        CustomerPhone = "test2",
            //        CustomerMail = "Art@mail.ru",
            //        CreationDate = "14741073522965",
            //        Lines = new List<ShopOrderLineDto>(){new ShopOrderLineDto{ ProductId = 8, ProductPriceId = 15, ProductAmount = 1}},
            //        Fields = new List<ObjectFieldDto>() { new ObjectFieldDto { IsRequired = true, Value = "Хз", FieldTypeId = 1, FieldTypeName = "String", Id = 0, SortOrder = 0 }, new ObjectFieldDto { IsRequired = true, Value = "123", FieldTypeId = 2, FieldTypeName = "Int32", Id = 0, SortOrder = 0 } }


            //    }));

        }


        [TestMethod]
        public void TestEmailSend()
        {
            new SendMailCommand().Execute(new SendMailContext{Message = "Test",Receiver = "alexeyklenov@yandex.ru",Subject = "test"});
        }

        [TestMethod]
        public void TestRemoveAddressesCommand()
        {
          new AddRemoveAddressesCommand().Execute(new AddRemoveAddressesContext
          {
              CustomerId = 3,
              RemovingAddresses = new[] {new DtoToCustomerAddress().Map(new CustomerAddressDto {Id = 1})}
          });
        }

        [TestMethod]
        public void TestAddFavoritesCommand()
        {
            new AddRemoveFavoritesCommand().Execute(new AddRemoveFavoritesContext
            {
                AddingProducts = new[] {2UL},
                CustomerId = 6
            });
        }

        [TestMethod]
        public void TestDeleteCommand()
        {
            SessionManager.GetSession()
                .Save(new CategoryColor() {Category = SessionManager.GetSession().Get<Category>(3UL)});

            new DeleteObjectCommand<CategoryColor>().Execute(new DeleteObjectContext<CategoryColor>() { ObjectIds = new[] { 3UL } });

        }

        [TestMethod]
        public void TestGetCouponDiscount()
        {
            var res = new GetCouponDiscountQuery().Ask(new GetCouponDiscountCriterion
            {
                Code = "6789",
                ProductIds = new List<ulong> {1, 2, 3}
            });
        }

        [TestMethod]
        public void TestDeleteCouponProductRelation()
        {
            var session = SessionManager.GetSession();
            var product = new Product {Name = "tttt2"};
                session.Save(product);

            session.Save(new Coupon {Name = "test2", Products = new List<Product> {product}});
            session.Flush();

            session = SessionManager.GetSession();
            product = session.Query<Product>().FirstOrDefault(p => p.Name == "tttt2");
            session.Delete(product);
           
        }

        [TestMethod]
        public void TestDeleteCouponShopOrderRelation()
        {
            var session = SessionManager.GetSession();
            var product = new Product { Name = "tttt2" };
            session.Save(product);

            session.Save(new Coupon { Name = "test222", Products = new List<Product> { product } });
            session.Flush();

            session = SessionManager.GetSession();
            var coupon = session.Query<Coupon>().FirstOrDefault(p => p.Name == "test222");

            session.Save(new ShopOrder() {Code = "test222", Coupon = coupon, CreationDate = DateTime.Now});
            session.Flush();

            session = SessionManager.GetSession();
            //var shopOrder = session.Query<ShopOrder>().FirstOrDefault(p => p.Code == "test222");
            coupon = session.Query<Coupon>().First(c => c.Name == "test222");
            session.Delete(coupon);
            session.Flush();
        }

        [TestMethod]
        public void TestYmlImport()
        {
            new ImportFromYmlCommand().Execute(new ImportFromYmlContext()
            {
                FileContent = File.ReadAllText("test_yml.xml")
            });
        }

        [TestMethod]
        public void TestResetCustomerPassword()
        {
            new ResetCustomerPasswordCommand().Execute(new ResetCustomerPasswordContext {Login = "alexeyklenov@yandex.ru" });
        }
    }
}
