﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using DomainTranslators.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Category.Criteria;
using Services.Category.Queries;
using Services.Crypt;
using Services.Enums;
using Services.Product.Criteria;
using Services.Product.Queries;
using Services.ShopOrder.Criteria;
using Services.ShopOrder.Queries;

namespace Tests
{
    [TestClass]
    public class QueryTests
    {
        [TestMethod]
        public void CategoryQueryWithPlace()
        {
           var res = SessionManager.GetSession().CreateQuery("select count(prod) from Category as cat left join cat.Products as prod left join prod.Places as place where place.Id = 2").List();
           new CategoryQuery().Ask(new CategoriesCriterion() {PlaceId = 1});
        }

        [TestMethod]
        public void CategoryQueryWithParams()
        {
            var res =
                new CategoryQuery().Ask(new CategoriesCriterion() {StartCategoryId = 0, WithChildren = true})
                    .Select(c => new CategoryToCategoryDto().Map(c))
                    .ToList();
        }

       [TestMethod]
        public void GethashQuery()
        {
            var hash = new CreateHashQuery().Ask(new CreateHashCriterion() {Data = "1"});
            
        }

       [TestMethod]
       public void GetProductsQuery()
       {
           var products =
               new ProductsQuery().Ask(new ProductsCriterion
               {
                   CategoryId = new List<ulong?> {3},
                   SortByPrice = true,
                   SortDirection = SortDirection.Asc,
                   PageSize = 200,
                   PageNumber = 0
               });

       }

       [TestMethod]
       public void GetShopOrders()
       {
           var shopOrders =
               new ShopOrdersQuery().Ask(new ShopOrdersCriterion()
               {
                   //CreationDate = new DateTime(2016,9,18,13,12,5)
               });

       }
    }
}
