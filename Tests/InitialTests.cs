﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate.Cfg.ConfigurationSchema;
using NHibernate.Linq;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using Services.Crypt;
using Configuration = NHibernate.Cfg.Configuration;

namespace Tests
{
    [TestClass]
    public class InitialTests
    {
        [TestMethod]
        public void ValidateDataBaseAndUpdate()
        {
            var conf = new Configuration().Configure();
            
            var mapper = new ModelMapper();
            var hibernateConfiguration = ConfigurationManager.GetSection("hibernate-configuration") as HibernateConfiguration;
            if (hibernateConfiguration != null)
            {
                foreach (var map in hibernateConfiguration.SessionFactory.Mappings)
                {
                    mapper.AddMappings(Assembly.Load(map.Assembly).DefinedTypes);
                }
            }
            conf.AddMapping( mapper.CompileMappingForAllExplicitlyAddedEntities());
            
            try
            {
                new SchemaValidator(conf).Validate();
            }
            catch
            {
                var upd = new SchemaUpdate(conf);
                upd.Execute(true, true);
                if (upd.Exceptions.Any())
                    throw upd.Exceptions.First();
            }

            //проверяем наличие группы администратора и пользователя
            var session = SessionManager.GetSession();
            var admRole = session.Query<Role>().FirstOrDefault(r => r.Code == "Admins");
            if (admRole == null)
            {
                admRole = new Role {Code = "Admins", Name = "Администраторы"};
                session.Save(admRole);
            }

            var user = session.Query<User>().FirstOrDefault(u => u.Roles.Contains(admRole));
            if (user == null)
            {
                user = new User
                {
                    Login = "admin@test.ru",
                    Roles = new List<Role> {admRole},
                    Salt = new CreateHashQuery().Ask(new CreateHashCriterion {Data = "1"})
                };
                session.Save(user);
            }
        }
    }
}
