﻿function EditObjectView(location, id) {
    window.location = location + "?objectId=" + id;
}

function DeleteObject(deleteAction, updateAction, id, updatableBlock) {

    var promise = new $.Deferred();

    BootstrapDialog.confirm({
        type: BootstrapDialog.TYPE_WARNING,
        title: 'Внимание',
        data: {'pass':''},
        message: function (dialog) {
            var message = $('<p>Продолжить?</p>');
            message.append('</br><label>пароль для подтверждения</label>');
            var input = $('<input type="password" id="confirm-pass" class="form-control"/>');
            input.change(function() {
                dialog.setData('pass', $(this).text());
            });
            message.append(input);
            return message;
        },
        btnOKLabel: 'Продолжить',
        btnCancelLabel: 'Отмена',
        callback: function(result) {
            if (result) {
                if (updatableBlock)
                    showModalLoader(updatableBlock);
                $.post(deleteAction, { "objectId": id, "confirmPass":$('#confirm-pass').val() })
                    .done(function() {
                        if (updateAction) {
                            $.post(updateAction).done(function(result) {
                                if (updatableBlock) {
                                    $(updatableBlock).replaceWith(result);
                                    hideModalLoader(updatableBlock);
                                }
                                promise.resolve();
                            });
                        } else {
                            if (updatableBlock)
                                hideModalLoader(updatableBlock);
                            promise.resolve();
                        }
                    })
                    .fail(function (xhr) {
                    if (updatableBlock)
                        hideModalLoader(updatableBlock);
                    if (xhr.status == 403) {
                        BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: 'Ошибка',
                            message: 'Доступ запрещен',
                        });
                        promise.resolve();
                    } else {
                        promise.reject();
                    }
                });
            }
        }
    });

    return promise;
}