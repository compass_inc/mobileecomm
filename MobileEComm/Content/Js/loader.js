﻿function showModalLoader(blockingEl) {
    $(blockingEl).block({ css: { border: 'none',width:'15%',backgroundColor:'none'}, message: '<div class="modal-content"><div class="modal-body"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span><span>  Загрузка данных...</span></div></div>' });
}

function hideModalLoader(blockingEl) {
    $(blockingEl).unblock();
}