﻿
var lastSelectItem;
var selectedObjectId;
var multiple;

function SelectModal(objectType, modalTitle, isMultiple) {
    var promise = new $.Deferred();
    selectedObjectId = undefined;
    multiple = isMultiple;

    $.get('/admin/' + objectType + '/selectview').done(function (view) {
        $('#select-modal-title').text(modalTitle);
        $('#select-modal-content').html(view);
        $('.hide-in-select').hide();
        if (isMultiple === true) {
            $('.multiple-selector').show();
            $('.multiple-apply').on('click', function () { SelectMultiple(promise); });
            $('.multiple-apply').show();

        }
        $('#select-modal').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function () {
            $('.hide-in-select').hide();
            if (isMultiple === true) {
                $('.multiple-selector').show();
            }
        });
        $('#select-modal').modal();
        $('#select-modal').on('hidden.bs.modal', function(e) {
            if (selectedObjectId == undefined)
                promise.reject();
        });
        if ($('.selectable').first().prop("tagName") === 'TR') {
            $('#select-modal').off('click', '.selectable');
            $('#select-modal').on('click', '.selectable td:not(:last-child)', function () { return SelectElement($(this).closest('tr'), promise); });
        } else {
            $('#select-modal').off('click', '.selectable td:not(:last-child)');
            $('#select-modal').on('click', '.selectable', function() { return SelectElement(this, promise); });
        }
    });
    return promise;
}

function SelectMultiple(promise) {
    selectedObjectId = [];
    $('#select-modal').unbind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified');
    $('#select-modal').off('click', '.selectable td:not(:last-child)');

    $('.multiple-selector:checked').each(function(item) {
        selectedObjectId.push($(this).attr("data-object-id"));
    });
    if (typeof selectData !== 'undefined') {
        lastSelectItem = [];
        selectedObjectId.forEach(function(item) {
            lastSelectItem.push(selectData[item]);
        });
    } else {
        lastSelectItem = undefined;
    }
    $('#select-modal').modal('hide');
    promise.resolve(selectedObjectId);
    return false;
}

function SelectElement(sender, promise) {
    $('#select-modal').off('click', '.selectable');
    if (multiple === true)
        selectedObjectId = [$(sender).attr("data-object-id")];
    else
        selectedObjectId = $(sender).attr("data-object-id");

    if (typeof selectData !== 'undefined') {
        if (multiple === true)
            lastSelectItem = [selectData[selectedObjectId[0]]];
        else
            lastSelectItem = selectData[selectedObjectId];
    } else {
        lastSelectItem = undefined;
    }
    $('#select-modal').modal('hide');
    promise.resolve(selectedObjectId);
    return false;
}