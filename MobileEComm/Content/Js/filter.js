﻿function ApplyFilter(filterData, updateAction, updatableBlock) {
    showModalLoader(updatableBlock);
    $.post(updateAction, filterData).done(function (result) {
        if (updatableBlock) {
            $(updatableBlock).replaceWith(result);
            hideModalLoader(updatableBlock);
        }
    });
    return false;
}