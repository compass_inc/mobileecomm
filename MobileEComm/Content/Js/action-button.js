﻿$(document).ready(function () {
    $('.FAB__action-button')
        .hover(function() {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            },
            function() {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });
});

function ShowActionButton(action, text, icon) {
    $('.action-button__text').text(text);
    $('.action-button__icon').text(icon);
    $('.action-button__icon').click(function(){action()});
    $('.FAB').show();
}