﻿function ApplyPage(pageNumber, pageSize, updateAction, updatableBlock) {
    showModalLoader(updatableBlock);
    $.post(updateAction, { PageNumber: pageNumber, PageSize: pageSize }).done(function (result) {
        if (updatableBlock) {
            $(updatableBlock).replaceWith(result);
            hideModalLoader(updatableBlock);
        }
    });

    return false;
}

function ApplyPageSize(pageSize, updateAction, updatableBlock) {
    showModalLoader(updatableBlock);
    $.post(updateAction, { PageNumber: 0, PageSize: pageSize }).done(function (result) {
        if (updatableBlock) {
            $(updatableBlock).replaceWith(result);
            hideModalLoader(updatableBlock);
        }
    });
    return false;
}