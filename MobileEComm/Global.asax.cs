﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Domain;
using MobileEComm.ActionFilters;
using MobileEComm.Models.Binders;
using MvcInfrastructure;
using Newtonsoft.Json;
using Services.Infrastructure.Interfaces;
using Services.Users.Criteria;

namespace MobileEComm
{
    public class MvcApplication : HttpApplication
    {
        private static IWindsorContainer _container;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configuration.Filters.Add(new HttpHeadersFilter());
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            #region register model binders
            
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new DecimalModelBinder());
            
            #endregion
            
            BootstrapContainer();

        }

        protected void FormsAuthentication_OnAuthenticate(object sender, FormsAuthenticationEventArgs args)
        {
            if (FormsAuthentication.CookiesSupported)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        var ticket = FormsAuthentication.Decrypt(
                        Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                        var user =
                            _container.Resolve<IQueryBuilder>()
                                .For<User>()
                                .With(new UsersCriterion {Email = ticket.Name});
                        if (user != null)
                            args.User = new GenericPrincipal(new GenericIdentity(user.Login, "Forms"), user.Roles.Select(r=>r.Code).ToArray());
                    }
                    catch (Exception e)
                    {
                        // Decrypt method failed.
                    }
                }
            }
            else
            {
                throw new HttpException("Не поддерживаются cookie");
            }
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            var nCulture = new CultureInfo("ru-RU")
            {
                NumberFormat = {NumberDecimalSeparator = "."}
            };
            Thread.CurrentThread.CurrentUICulture = nCulture;
            Thread.CurrentThread.CurrentCulture = nCulture;
        }

        private static void BootstrapContainer()
        {
            _container = new WindsorContainer();

            _container.AddFacility<TypedFactoryFacility>();

            _container.Install(FromAssembly.InDirectory(new AssemblyFilter(HttpRuntime.BinDirectory)));

            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            GlobalConfiguration.Configuration.Services.Replace(
                typeof (IHttpControllerActivator),
                new WindsorCompositionRoot(_container));            
        }
    }
}