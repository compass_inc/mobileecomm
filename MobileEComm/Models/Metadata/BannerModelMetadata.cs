﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class BannerModelMetadata : ModelMetadataConfiguration<BannerDto>
    {
        public BannerModelMetadata()
        {
            Configure(b => b.Name).DisplayName("Название").Required("Обязательное поле");
        }
    }
}