﻿using Domain;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class ShopOrderStateModelMetadata : ModelMetadataConfiguration<ShopOrderState>
    {
        public ShopOrderStateModelMetadata()
        {
            Configure(m => m.Name).DisplayName("Название").Required("Обязательное поле");
            Configure(m => m.IsDefault).DisplayName("Статус по умолчанию");
            Configure(m => m.Code).DisplayName("Код");
        }
    }
}