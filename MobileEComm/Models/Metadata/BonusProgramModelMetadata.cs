﻿using Domain;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class BonusProgramModelMetadata : ModelMetadataConfiguration<BonusProgram>
    {
        public BonusProgramModelMetadata()
        {
            Configure(o => o.Description).DisplayName("Описание");
            Configure(o => o.Name).DisplayName("Имя");
            Configure(o => o.MaxDiscountPercent)
                .DisplayName("Максимальная процент от заказа для оплаты бонусами")
                .Required("Обязательное поле");

            Configure(o => o.Status).DisplayName("Статус");

        }
    }
}