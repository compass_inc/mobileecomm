﻿using Domain;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class BannerTypeModelMetadata: ModelMetadataConfiguration<BannerType>
    {
        public BannerTypeModelMetadata()
        {
            Configure(b => b.Name).DisplayName("Название").Required("Обязательное поле");
            Configure(b => b.Code).DisplayName("Символьный код");
        }
    }
}