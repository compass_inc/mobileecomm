﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class PlaceModelMetadata: ModelMetadataConfiguration<PlaceDto>
    {
        public PlaceModelMetadata()
        {
            Configure(o => o.Name).DisplayName("Навание");
            Configure(o => o.Email).DisplayName("Электронная почта");
            Configure(o => o.Coordinates).DisplayName("Координаты");
            Configure(o => o.City).DisplayName("Город").Required("Обязательное поле");
            Configure(o => o.Street).DisplayName("Улица").Required("Обязательное поле");
            Configure(o => o.House).DisplayName("Дом").Required("Обязательное поле");
            Configure(o => o.Phone).DisplayName("Телефон").Required("Обязательное поле");
            Configure(o => o.MinOrderSum).Format("{0:0.00}").ApplyFormatInEditMode().DisplayName("Минимальная сумма заказа").Required("Обязательное поле");
            Configure(o => o.DeliveryFreeSum).Format("{0:0.00}").ApplyFormatInEditMode().DisplayName("Сумма заказа для бесплатной доставки");
            Configure(o => o.DeliveryStartTime).DisplayName("От").Required("Обязательное поле");
            Configure(o => o.DeliveryEndTime).DisplayName("До").Required("Обязательное поле");
            Configure(o => o.WorkStartTime).DisplayName("От").Required("Обязательное поле");
            Configure(o => o.WorkEndTime).DisplayName("До").Required("Обязательное поле");
            Configure(o => o.MinDeliverySum).DisplayName("Минимальная стоимость доставки").Format("{0:0.00}").ApplyFormatInEditMode().Required("Обязательно поле");
            Configure(o => o.MinDeliveryTime).DisplayName("Минимальное время доставки").Required("Обязательное поле");
        }
    }
}