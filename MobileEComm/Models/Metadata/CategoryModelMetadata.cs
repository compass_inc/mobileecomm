﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class CategoryModelMetadata: ModelMetadataConfiguration<CategoryDto>
    {
        public CategoryModelMetadata()
        {
            Configure(c => c.Name).DisplayName("Название").Required("Обязательное поле");
            Configure(c => c.Code).DisplayName("Код");
            Configure(c => c.ParentId).AsDropDownList("Categories").DisplayName("Родительская категория");
        }
    }
}