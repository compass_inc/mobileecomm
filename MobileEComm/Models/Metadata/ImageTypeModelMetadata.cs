﻿using Domain;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class ImageTypeModelMetadata : ModelMetadataConfiguration<ImageType>
    {
        public ImageTypeModelMetadata()
        {
            Configure(o => o.Name).DisplayName("Название").Required("Обязательное поле");
            Configure(o => o.Code).DisplayName("Код");
        }
    }
}