﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class RoleModelMetadata : ModelMetadataConfiguration<RoleDto>
    {
        public RoleModelMetadata()
        {
            Configure(r => r.Name).DisplayName("Имя").Required("Обязательное поле");
            Configure(r => r.Code).DisplayName("Код");
        }
    }
}