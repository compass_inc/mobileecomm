﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class ImageModelMetadata : ModelMetadataConfiguration<ImageDto>
    {
        public ImageModelMetadata()
        {
            Configure(b => b.Description).DisplayName("Описание").AsMultilineText();
            Configure(b => b.Path).DisplayName("Путь").Required("Обязательное поле");
        }
    }
}