﻿using Domain;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class LocalStringTypeModelMetadata : ModelMetadataConfiguration<LocaleStringType>
    {
        public LocalStringTypeModelMetadata()
        {
            Configure(b => b.Name).DisplayName("Название").Required("Обязательное поле");
            Configure(b => b.Code).DisplayName("Код").Required("Обязательное поле");
        }
    }
}