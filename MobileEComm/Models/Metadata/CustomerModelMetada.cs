﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class CustomerModelMetada : ModelMetadataConfiguration<CustomerDto>
    {
        public CustomerModelMetada()
        {
            Configure(o => o.Email).DisplayName("Электронная почта").AsEmail().Required("Обязательное поле");
            Configure(o => o.Name).DisplayName("ФИО");
            Configure(o => o.Phone).DisplayName("Телефон");
            Configure(o => o.Status).DisplayName("Статус");
        }
    }
}