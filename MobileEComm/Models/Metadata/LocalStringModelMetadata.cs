﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class LocalStringModelMetadata : ModelMetadataConfiguration<LocaleStringDto>
    {
        public LocalStringModelMetadata()
        {
            Configure(b => b.Name).DisplayName("Название").Required("Обязательное поле");
            Configure(b => b.Code).DisplayName("Код").Required("Обязательное поле");
        }
    }
}