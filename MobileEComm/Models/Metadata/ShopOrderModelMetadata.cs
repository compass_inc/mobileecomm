﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class   ShopOrderModelMetadata : ModelMetadataConfiguration<ShopOrderDto>
    {
        public ShopOrderModelMetadata()
        {
            Configure(m => m.CreationDate).DisplayName("Дата создания");
            Configure(m => m.CustomerName).DisplayName("Как обращаться");
            Configure(m => m.CustomerMail).DisplayName("Электронная почта");
            Configure(m => m.CustomerPhone).DisplayName("Телефон");
            Configure(m => m.IsPickup).DisplayName("Самовывоз");
            Configure(m => m.CustomerAddress).DisplayName("Адрес доставки");
            Configure(m => m.DeliveryTime).DisplayName("Время доставки");
            Configure(m => m.BonusProgramName).DisplayName("Бонусная программа");
            Configure(m => m.WriteOffBonusSum).DisplayName("Сумма");
        }
    }
}