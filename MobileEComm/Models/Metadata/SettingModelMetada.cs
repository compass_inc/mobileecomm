﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class SettingModelMetada : ModelMetadataConfiguration<SettingsDto>
    {
        public SettingModelMetada()
        {
            Configure(s => s.BackGroundColor).DisplayName("Цвет заднего фона");
            Configure(s => s.HeaderFooterColor).DisplayName("Цвет футера и хэдэра");
            Configure(s => s.ShopOrderEmail).DisplayName("Email для уведомлений о заказе");
            Configure(s => s.FromEmail).DisplayName("Поле 'отправитель' для рассылки");
            Configure(s => s.FreeDeliveryPercentNotify)
                .DisplayName("Процент от суммы бесплатной доставки для уведомления клиента");

            Configure(s => s.ShopName).DisplayName("Название магазина");
            Configure(s => s.UseGravatar).DisplayName("Использовать gravatar");
            Configure(s => s.NoOrderMode).DisplayName("Режим работы 'Без заказа'");
            Configure(s => s.AuthRequired).DisplayName("Обязательная авторизация");
            Configure(s => s.OnePageCatalog).DisplayName("Одностраничный каталог");
            Configure(s => s.SideMainMenu).DisplayName("Боковое главное меню");
            Configure(s => s.NoComments).DisplayName("Отключить отзывы");
            Configure(s => s.OnlyCity).DisplayName("Выбор только города");
            Configure(s => s.ShowFilialInfoInProduct).DisplayName("Отображать информацию о связанном филиале в продукте");
            Configure(s => s.StartPageBannerCode).DisplayName("Тип баннера для стартовой страницы");
        }
    }
}