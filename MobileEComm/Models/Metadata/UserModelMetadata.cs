﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class UserModelMetadata : ModelMetadataConfiguration<UserDto>
    {
        public UserModelMetadata()
        {
            Configure(u => u.Name).DisplayName("ФИО");
            Configure(u => u.Login).DisplayName("Логин").Required("Обязательное поле");
            Configure(u => u.Email).DisplayName("Электронная почта").Required("Обязательное поле");
            Configure(u => u.Status).DisplayName("Статус");
        }
    }
}