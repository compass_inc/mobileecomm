﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class ReviewModelMetadata : ModelMetadataConfiguration<ReviewDto>
    {
        public ReviewModelMetadata()
        {
            Configure(r => r.CreationDate).DisplayName("Дата создания").ReadOnly();
            Configure(r => r.IsOwnerAnswer).DisplayName("Ответ компании");
            Configure(r => r.Title).DisplayName("Заголовок").Required("Обязательное поле");
            Configure(r => r.Description).DisplayName("Текст").Required("Обязательное поле");
            Configure(r => r.Rate).DisplayName("Рейтинг").Optional();
            Configure(r => r.Status).DisplayName("Статус").Required("Обязательное поле");
            Configure(r => r.DisplayEMail).DisplayName("Электронная почта");
        }
    }
}