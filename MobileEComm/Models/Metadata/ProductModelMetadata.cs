﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class ProductModelMetadata : ModelMetadataConfiguration<ProductDto>
    {
        public ProductModelMetadata()
        {
            Configure(m => m.Name).DisplayName("Наименование").Required("Обязательное поле");
            Configure(m => m.Status).DisplayName("Статус").Required("Обязательное поле");
            Configure(m => m.ShortDescription).DisplayName("Краткое описание").AsMultilineText();
            Configure(m => m.LongDescription).DisplayName("Полное описание").AsMultilineText();
            Configure(m => m.Code).DisplayName("Символьный код");
        }    
    }
}