﻿using Domain;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class FieldTypeModelMetadata : ModelMetadataConfiguration<IObjectFieldType>
    {
        public FieldTypeModelMetadata()
        {
            Configure(o => o.CultureSpecTypeName).DisplayName("Тип").Required("Обязательное поле");
            Configure(o => o.Name).DisplayName("Имя").Required("Обязательное поле");
            Configure(o => o.SortOrder).DisplayName("Сортировка");
            Configure(o => o.IsRequired).DisplayName("Обязательность заполнения");
        }
    }
}