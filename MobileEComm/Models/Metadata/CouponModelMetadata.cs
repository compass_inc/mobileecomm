﻿using Domain.Dto;
using MvcExtensions;

namespace MobileEComm.Models.Metadata
{
    public class CouponModelMetadata : ModelMetadataConfiguration<CouponDto>
    {
        public CouponModelMetadata()
        {
            Configure(o => o.Name).DisplayName("Название");
            Configure(o => o.Description).DisplayName("Описание");
            Configure(o => o.DiscountValue).DisplayName("Процент скидки");
            Configure(o => o.StatusName).DisplayName("Статус");
            Configure(o => o.MaxUseCount).DisplayName("Максимальное число использований");
            Configure(o => o.EndDate).DisplayName("Дата завершения");
            Configure(o => o.Code).DisplayName("Код");
        }
    }
}