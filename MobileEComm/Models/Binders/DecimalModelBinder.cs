﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace MobileEComm.Models.Binders
{
    public class DecimalModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (valueProviderResult.AttemptedValue.Equals("N.aN") ||
                valueProviderResult.AttemptedValue.Equals("NaN") ||
                valueProviderResult.AttemptedValue.Equals("Infini.ty") ||
                valueProviderResult.AttemptedValue.Equals("Infinity") ||
                string.IsNullOrEmpty(valueProviderResult.AttemptedValue))
                return 0m;

            return Convert.ToDecimal(valueProviderResult.AttemptedValue
                .Replace(".",Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)
                .Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)
                );
        }
    }
}