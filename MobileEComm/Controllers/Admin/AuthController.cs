﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Services.Infrastructure.Interfaces;
using Services.Users.Criteria;

namespace MobileEComm.Controllers.Admin
{
    public class AuthController : Controller
    {
        private readonly IQueryBuilder _queryBuilder;
        public AuthController(IQueryBuilder queryBuilder)
        {
            _queryBuilder = queryBuilder;
        }

        public ActionResult Index(string login, string returnUrl, string errorText)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Login = login;
            ViewBag.ErrorText = errorText;
            return View("~/Views/Admin/Auth/Index.cshtml");
        }

        public ActionResult In(string login, string password, string returnUrl)
        {
            var res = _queryBuilder.For<bool>().With(new ValidateUserPasswordCriterion {Email = login, Password = password});
            if (!res)
                return RedirectToAction("Index",
                    new RouteValueDictionary
                    {
                        {"returnUrl", returnUrl},
                        {"login", login},
                        {"errorText", "Не верный логин/пароль"}
                    });
            

            FormsAuthentication.SetAuthCookie(login, true);

            return Redirect(string.IsNullOrEmpty(returnUrl) ? Url.Action("Index", "Main") : returnUrl);
        }

        [Authorize]
        public ActionResult Out()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}
