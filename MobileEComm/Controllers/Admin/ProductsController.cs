﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;
using Services.Product.Criteria;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ProductsController : BaseObjectCrudController<Product,ProductDto>
    {
        public ProductsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "Products")
        {
            UpdateAction = () => Url.Action("IndexData", "Products");
            UpdatableBlock = "#products-table-block";
        }

        protected string NameFilter;

        public override ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie(ControllerName + "_NameFilter"));
            return base.Index();
        }

        public override ActionResult IndexData()
        {

            GetPagination();

            NameFilter = Request.Params.AllKeys.Contains("NameFilter")
                ? Request.Params["NameFilter"]
                : Request.Cookies.AllKeys.Contains(ControllerName + "_NameFilter")
                    ? Request.Cookies[ControllerName + "_NameFilter"].Value
                    : null;

            Response.Cookies.Add(new HttpCookie(ControllerName + "_NameFilter", NameFilter));

            ViewBag.NameFilter = NameFilter;

            var criterion = new ProductsCriterion
            {
                Name = NameFilter,
                CategoryId = new List<ulong?> {0},
                PageNumber = PageNumber,
                PageSize = PageSize
            };

            return IndexData(criterion, criterion);
        }

        public ActionResult GetNextCategorySortOrder(ulong categoryId, ulong productId)
        {
            return Json(QueryBuilder.For<ulong>()
                .With(new GetNextCategoryOrderCriterion { CategoryId = categoryId /*, ProductId = productId*/ }));
        }
    }
}