﻿using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class LocaleStringTypesController : BaseObjectCrudController<LocaleStringType, LocaleStringType>
    {
        public LocaleStringTypesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "LocaleStringTypes")
        {
        }
    }
}