﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;
using DomainTranslators;
using Newtonsoft.Json;
using Services.Base.CommandContexts;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;
using Services.Users.Criteria;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class BaseObjectCrudController<TDt, TDtoT> : Controller
        where TDt : BaseObject
        where TDtoT : BaseObject
    {
        protected readonly IQueryBuilder QueryBuilder;
        protected readonly ICommandBuilder CommandBuilder;
        protected readonly ITranslatorBuilder Translator;

        protected string ControllerName { get; set; }
        /// <summary>
        /// Html идентификатор обновляемого через ajax блока с данным об объектах для передачи во view
        /// </summary>
        protected string UpdatableBlock { get; set; }

        /// <summary>
        /// Функция получения пути до обновляющего данные через ajax метода 
        /// </summary>
        protected Func<string> UpdateAction { get; set; }


        public BaseObjectCrudController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator, string controllerName)
        {
            QueryBuilder = queryBuilder;
            CommandBuilder = commandBuilder;
            Translator = translator;
            ControllerName = controllerName;
        }

        protected int PageNumber;
        protected int PageSize;

        protected void GetPagination()
        {
            PageSize =
                Request.Params.AllKeys.Contains("PageSize")
                    ? int.Parse(Request.Params["PageSize"])
                    : Request.Cookies.AllKeys.Contains(ControllerName + "_PageSize")
                        ? int.Parse(Request.Cookies[ControllerName + "_PageSize"].Value)
                        : 10;
            PageNumber = Request.Params.AllKeys.Contains("PageNumber")
                ? int.Parse(Request.Params["PageNumber"])
                : PageSize > 0 ? 0 : -1;

            SetPagination();
        }

        protected void SetPagination()
        {
            Response.Cookies.Add(new HttpCookie(ControllerName +"_PageSize",PageSize.ToString()));
        }

        [System.Web.Mvc.HttpGet]
        public virtual ActionResult Index()
        {
            GetPagination();

            return
                Index(new BaseObjectCriterion
                {
                    PageNumber = PageNumber,
                    PageSize = PageSize
                });
        }

        protected ActionResult Index(ICriterion criterion, ICriterion countObjCriterion = null)
        {
            GetPagination();

            ViewBag.PageNumber = PageNumber;
            ViewBag.PageSize = PageSize;

            ViewBag.MaxPage = ViewBag.PageSize > 0
                ? Math.Ceiling(CountObjects(countObjCriterion ?? new ObjectCountCriterion<TDt>())/
                               (double) ViewBag.PageSize)
                : 0;

            ViewBag.UpdateAction = UpdateAction == null ? null : UpdateAction();
            ViewBag.UpdatableBlock = UpdatableBlock;

            ViewData.Model =
                QueryBuilder.For<ICollection<TDt>>().With(criterion).Select(o => Translator.Map<TDt, TDtoT>(o)).ToList();
            return View(string.Format("~/Views/Admin/{0}/Index.cshtml", ControllerName));
        }

        [System.Web.Mvc.HttpPost]
        public virtual ActionResult IndexData()
        {
            GetPagination();

            return IndexData(new BaseObjectCriterion
            {
                PageNumber = PageNumber,
                PageSize = PageSize
            });
        }

        protected ActionResult IndexData(ICriterion criterion, ICriterion countObjCriterion = null)
        {
            GetPagination();

            ViewBag.PageNumber = PageNumber;
            ViewBag.PageSize = PageSize;

            ViewBag.MaxPage = ViewBag.PageSize > 0
                ? Math.Ceiling(CountObjects(countObjCriterion ?? new ObjectCountCriterion<TDt>())/
                               (double) ViewBag.PageSize)
                : 0;

            ViewBag.UpdateAction = UpdateAction == null ? null : UpdateAction();
            ViewBag.UpdatableBlock = UpdatableBlock;

            ViewData.Model =
                QueryBuilder.For<ICollection<TDt>>().With(criterion).Select(o => Translator.Map<TDt, TDtoT>(o)).ToList();
            return View(string.Format("~/Views/Admin/{0}/IndexData.cshtml", ControllerName));
        }

        protected virtual int CountObjects(ICriterion countCriterion)
        {
            return QueryBuilder.For<int>().With(countCriterion);
        }

        public virtual ActionResult Edit(ulong objectId)
        {
            return Edit(new BaseObjectCriterion {Ids = new List<ulong> {objectId}});
        }

        protected ActionResult Edit(ICriterion criterion)
        {
            var res = QueryBuilder.For<ICollection<TDt>>().With(criterion);
            if (res != null)
            {
                var objects = res.Select(o => Translator.Map<TDt,TDtoT>(o)).ToList();

                if (objects.Any())
                {
                    ViewData.Model = objects.First();
                    return View(string.Format("~/Views/Admin/{0}/Add.cshtml", ControllerName));
                }
            }
            return RedirectToAction("Index");
        }

        [System.Web.Mvc.HttpGet]
        public virtual ActionResult Add()
        {
            ViewData.Model = Translator.Map<TDt, TDtoT>(Activator.CreateInstance<TDt>());
            return View(string.Format("~/Views/Admin/{0}/Add.cshtml", ControllerName));
        }

        [ValidateInput(false)]
        [System.Web.Mvc.HttpPost]
        public virtual ActionResult Add(TDtoT o)
        {
            return Add(o, new CreateOrUpdateObjectContext<TDt>(null));
        }

        [ValidateInput(false)]
        [System.Web.Mvc.HttpPost]
        protected virtual ActionResult Add<TContext>(TDtoT o, TContext context) where TContext : CreateOrUpdateObjectContext<TDt>
        {
            ViewData.Model = o;
            if (!ModelState.IsValid)
                return View(string.Format("~/Views/Admin/{0}/Add.cshtml", ControllerName));
            else
            {
                var domainObj = Translator.Map<TDtoT, TDt>(o);
                if (context == null)
                    CommandBuilder.Execute(new CreateOrUpdateObjectContext<BaseObject>(domainObj));
                else
                {
                    context.Object = domainObj;
                    CommandBuilder.Execute(context);
                }
                ViewBag.Message = "Элемент успешно добавлен";
                return RedirectToAction("Index");
            }
        }

        [System.Web.Mvc.HttpPost]
        [Authorize(Roles = "Admins")]
        public virtual ActionResult Delete(ulong objectId, string confirmPass = "")
        {
            if (
                !QueryBuilder.For<bool>()
                    .With(new ValidateUserPasswordCriterion
                    {
                        Email = System.Web.HttpContext.Current.User.Identity.Name,
                        Password = confirmPass
                    }))
            {
                Response.StatusCode = (int)HttpStatusCode.Forbidden;
                Response.TrySkipIisCustomErrors = true;
                return Content("Не верный пароль подтверждения", "text/plain");
            }

            CommandBuilder.Execute(new DeleteObjectContext<TDt> {ObjectIds = new List<ulong> {objectId}});
            return Json("success");
        }

        [System.Web.Mvc.HttpPost]
        public virtual ActionResult SetOrder(ulong pk, ulong value)
        {
            CommandBuilder.Execute(new SetObjectOrderContext<TDt>(pk, value));
            return Json("success");
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult SelectView()
        {
            return IndexData();
        }

        [System.Web.Mvc.HttpPost]
        public virtual ActionResult Get(ulong[] objectIds)
        {
            return Content(
                JsonConvert.SerializeObject(QueryBuilder.For<ICollection<TDt>>()
                    .With(new BaseObjectCriterion {Ids = objectIds})
                    .Select(o => Translator.Map<TDt, TDtoT>(o))
                    .ToList(), Formatting.None,
                    new JsonSerializerSettings() {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}),
                "application/json"
                );
        }

        #region Custom Fields

        public virtual ActionResult FieldTypes()
        {
            ViewBag.Controller = ControllerName;
            ViewData.Model = QueryBuilder.For<ICollection<ObjectFieldType<TDt>>>().With(new BaseObjectCriterion()).OrderBy(t => t.SortOrder).ToList();
            return View("~/Views/Admin/Base/FieldTypes.cshtml");
        }

        public virtual ActionResult FieldTypesData()
        {
            ViewBag.Controller = ControllerName;
            ViewData.Model = QueryBuilder.For<ICollection<ObjectFieldType<TDt>>>().With(new BaseObjectCriterion()).OrderBy(t=>t.SortOrder).ToList();
            return View("~/Views/Admin/Base/FieldTypesData.cshtml");
        }

        [System.Web.Mvc.HttpGet]
        public virtual ActionResult AddFieldType()
        {
            ViewBag.Controller = ControllerName;
            ViewData.Model = Activator.CreateInstance<ObjectFieldType<TDt>>();
            return View("~/Views/Admin/Base/AddFieldType.cshtml");
        }

        [System.Web.Mvc.HttpPost]
        public virtual ActionResult AddFieldType(ObjectFieldType<TDt> field)
        {
            ViewBag.Controller = ControllerName;
            ViewData.Model = field;
            if (!ModelState.IsValid)
                return View("~/Views/Admin/Base/FieldTypesData.cshtml");
            else
            {
                CommandBuilder.Execute(new CreateOrUpdateObjectContext<ObjectFieldType<TDt>>(field));

                ViewBag.Message = "Элемент успешно добавлен";
                return RedirectToAction("FieldTypes");
            }
        }

        public virtual ActionResult EditFieldType(ulong objectId)
        {
            var res = QueryBuilder.For<ICollection<ObjectFieldType<TDt>>>().With(new BaseObjectCriterion { Ids = new[] { objectId } });
            if (res != null && res.Any())
            {
                    ViewData.Model = res.First();
                    return View("~/Views/Admin/Base/AddFieldType.cshtml");
            }
            return RedirectToAction("FieldTypes");
        }

        [System.Web.Mvc.HttpPost]
        public virtual ActionResult DeleteFieldType(ulong objectId)
        {
            CommandBuilder.Execute(new DeleteObjectContext<ObjectFieldType<TDt>> { ObjectIds = new List<ulong> { objectId } });
            return Json("success");
        }

        #endregion

        #region Filter Methods

        protected virtual string GetFilterValue(string paramName)
        {
            return Request.Params.AllKeys.Contains(paramName)
                ? string.IsNullOrEmpty(Request.Params[paramName])
                    ? null
                    : Request.Params[paramName]
                : Request.Cookies.AllKeys.Contains(ControllerName + "_" + paramName)
                    ? Request.Cookies[ControllerName + "_" + paramName].Value
                    : null;
        }

        protected virtual T? GetFilterValue<T>(string paramName) where T : struct
        {
            var filterValue = GetFilterValue(paramName);
            if (string.IsNullOrEmpty(filterValue))
                return new T?();

            var parseMethod = typeof (T).GetMethod("Parse",new[] {typeof(string)});
            if (parseMethod == null)
                return new T?();

            return (T) parseMethod.Invoke(null, new object[] {filterValue});
        }

        #endregion
    }
}
