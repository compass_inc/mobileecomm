﻿using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class LocaleStringsController : BaseObjectCrudController<LocaleString, LocaleStringDto>
    {
        public LocaleStringsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "LocaleStrings")
        {
        }
    }
}
