﻿using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class BannerTypesController : BaseObjectCrudController<BannerType,BannerType>
    {
        public BannerTypesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "BannerTypes")
        {
        }
    }
}