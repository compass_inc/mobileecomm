﻿using System;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ReviewsController : BaseObjectCrudController<Review, ReviewDto>
    {
        public ReviewsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "Reviews")
        {
        }

        public ActionResult AddOwnerAnswerReview(ulong parentReviewId, string parentReviewTitle)
        {
            var reviewDto = Translator.Map<Review, ReviewDto>(Activator.CreateInstance<Review>());
            reviewDto.IsOwnerAnswer = true;
            reviewDto.ParentId = parentReviewId;
            reviewDto.ParentTitle = parentReviewTitle;
            ViewData.Model = reviewDto;

            return View(string.Format("~/Views/Admin/{0}/Add.cshtml", ControllerName));
        }
    }
}