﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using NHibernate.Util;
using Services.Base.CommandContexts;
using Services.Infrastructure.Interfaces;
using Services.Settings.commandContexts;
using Services.Settings.Criteria;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly IQueryBuilder _query;
        private readonly ICommandBuilder _command;
        private readonly ITranslatorBuilder _translator;

        public SettingsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _command = commandBuilder;
            _translator = translator;
        }

        public ActionResult Index()
        {
            ViewData.Model = _translator.Map<Settings, SettingsDto>(_query.For<Settings>().With(new SettingsCriterion()));

            return View("~/Views/Admin/Settings/Index.cshtml");
        }

        public ActionResult Save(SettingsDto sets)
        {
            _command.Execute(new CreateOrUpdateObjectContext<Settings>(_translator.Map<SettingsDto, Settings>(sets)));
            return RedirectToAction("Index");
        }

        public ActionResult ImportYml()
        {
            return View("~/Views/Admin/Settings/ImportYmlIndex.cshtml");
        }

        public ActionResult StartImportYml()
        {
            if (Request.Files.Any())
            {
                try
                {
                    var file = Request.Files[0];
                    if (file != null)
                    {
                        file.SaveAs(HttpRuntime.AppDomainAppPath + "\\import_yml");

                        _command.Execute(new ImportFromYmlContext
                        {
                            FileContent = System.IO.File.ReadAllText(HttpRuntime.AppDomainAppPath + "\\import_yml"),
                            ImportImages = false
                        });

                        System.IO.File.Delete(HttpRuntime.AppDomainAppPath + "\\import_yml");
                    }
                }
                catch
                {
                    System.IO.File.Delete(HttpRuntime.AppDomainAppPath + "\\import_yml");
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        ReasonPhrase = "ошибка обработки файла"
                    });
                }
            }
            return Json("success");;
        }
    }
}