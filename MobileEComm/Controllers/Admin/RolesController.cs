﻿using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class RolesController : BaseObjectCrudController<Role, RoleDto>
    {
        public RolesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator)
            : base(queryBuilder, commandBuilder, translator, "Roles")
        {
        }
    }
}
