﻿using System.Web.Mvc;
using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ShopOrderStatesController : BaseObjectCrudController<ShopOrderState,ShopOrderState>
    {
        public ShopOrderStatesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "ShopOrderStates")
        {
        }
    }
}