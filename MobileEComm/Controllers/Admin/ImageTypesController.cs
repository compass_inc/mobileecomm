﻿using System.Web.Mvc;
using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ImageTypesController : BaseObjectCrudController<ImageType, ImageType>
    {
        public ImageTypesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "ImageTypes")
        {
        }
    }
}