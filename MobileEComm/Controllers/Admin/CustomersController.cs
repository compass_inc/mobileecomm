﻿using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Customer.CommandContexts;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class CustomersController : BaseObjectCrudController<Customer,CustomerDto>
    {
        public CustomersController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "Customers")
        {
            UpdateAction = () => Url.Action("IndexData", "Customers");
            UpdatableBlock = "#customers-table-block";
        }

        public ActionResult AddWPwd(CustomerDto dto, string password)
        {
            return Add(dto,
                new CreateOrUpdateCustomerContext(null) {Password = password});
        }

        public HttpResponseMessage ChangePwd(ulong customerId, string password)
        {
            CommandBuilder.Execute(new ChangeCustomerPasswordContext {AuthObjectId = customerId, NewPassword = password});
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}