﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Domain;
using DomainTranslators;
using Services.Base.CommandContexts;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class LoyalityController : Controller
    {
        private readonly IQueryBuilder _query;
        private readonly ICommandBuilder _command;
        private readonly ITranslatorBuilder _translator;

        public LoyalityController(IQueryBuilder query, ICommandBuilder command, ITranslatorBuilder translator)
        {
            _query = query;
            _command = command;
            _translator = translator;
        }


        #region BonusProgram

        public ActionResult BonusProgram()
        {
            ViewData.Model = _query.For<ICollection<BonusProgram>>().With(new BaseObjectCriterion()).FirstOrDefault() ??
                             new BonusProgram();
            return View("~/Views/Admin/Loyality/BonusProgram.cshtml");
        }

        public ActionResult EditBonusProgram(BonusProgram program)
        {
            ViewData.Model = program;
            if (ModelState.IsValid)
                _command.Execute(new CreateOrUpdateObjectContext<BonusProgram>(program));

            return RedirectToAction("BonusProgram");
        }

        #endregion
    }
}