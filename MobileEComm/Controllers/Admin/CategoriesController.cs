﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Category.Criteria;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class CategoriesController : BaseObjectCrudController<Category, CategoryDto>
    {

        public CategoriesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator)
            : base(queryBuilder, commandBuilder, translator, "Categories")
        {
        }

        private IEnumerable<CategoryDto> Categories
        {
            get
            {
                return
                    QueryBuilder.For<ICollection<Category>>()
                        .With(new CategoriesCriterion {WithChildren = true})
                        .Select(Translator.Map<Category, CategoryDto>);
            }
        }

        public override ActionResult Index()
        {
            return Index(new CategoriesCriterion{WithChildren = true});
        }

        public override ActionResult IndexData()
        {
            return IndexData(new CategoriesCriterion { WithChildren = true });
        }

        public override ActionResult Add()
        {
            ViewData["Categories"] = Categories;
            return base.Add();
        }

        public override ActionResult Add(CategoryDto o)
        {
            ViewData["Categories"] = Categories;
              
            return base.Add(o);
        }

        public override ActionResult Edit(ulong objectId)
        {
            ViewData["Categories"] = Categories;

            return Edit(new CategoriesCriterion {Ids =  new List<ulong> {objectId}});
        }

        public override ActionResult SetOrder(ulong pk, ulong value)
        {
            base.SetOrder(pk, value);
            return IndexData();
        }
    }
}
