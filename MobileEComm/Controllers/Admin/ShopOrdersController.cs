﻿using System;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;
using Services.ShopOrder.CommandContext;
using Services.ShopOrder.Criteria;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ShopOrdersController : BaseObjectCrudController<ShopOrder,ShopOrderDto>
    {
        public ShopOrdersController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "ShopOrders")
        {
            UpdateAction = () => Url.Action("IndexData", "ShopOrders");
            UpdatableBlock = "#shop-orders-table-block";
        }

        protected DateTime? StartDateFilter;
        protected DateTime? EndDateFilter;

        public override ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie(ControllerName + "_DateFilter"));
            return base.Index();
        }

        public override ActionResult IndexData()
        {
            GetPagination();

            StartDateFilter = GetFilterValue<DateTime>("StartDateFilter");
            Response.Cookies.Add(new HttpCookie(ControllerName + "_StartDateFilter",
                StartDateFilter == null ? "" : StartDateFilter.ToString()));
            ViewBag.StartDateFilter = StartDateFilter;

            EndDateFilter = GetFilterValue<DateTime>("EndDateFilter");
            Response.Cookies.Add(new HttpCookie(ControllerName + "_EndDateFilter",
                EndDateFilter == null ? "" : EndDateFilter.ToString()));
            ViewBag.EndDateFilter = EndDateFilter;

            var criterion = new ShopOrdersCriterion
            {
                CreationDateStart = StartDateFilter,
                CreationDateEnd = EndDateFilter,
                PageNumber = PageNumber,
                PageSize = PageSize
            };

            return IndexData(criterion,criterion);
        }

        public override ActionResult Add(ShopOrderDto o)
        {
            return base.Add(o,new CreateOrUpdateShopOrderContext(null));
        }
    }
}