﻿using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class PlacesController : BaseObjectCrudController<Place,PlaceDto>
    {
        public PlacesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator)
            : base(queryBuilder, commandBuilder, translator, "Places")
        {
            UpdateAction = () => Url.Action("IndexData", "Places");
            UpdatableBlock = "#places-table-block";
        }
    }
}