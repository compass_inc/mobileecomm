﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DataAccess;
using Domain;
using DomainTranslators;
using NHibernate.Cfg.ConfigurationSchema;
using NHibernate.Linq;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using Services.Base.CommandContexts;
using Services.Crypt;
using Services.Infrastructure.Interfaces;
using Services.Settings.Criteria;
using Configuration = NHibernate.Cfg.Configuration;

namespace MobileEComm.Controllers.Admin
{

    public class MainController : Controller
    {

        protected readonly IQueryBuilder QueryBuilder;
        protected readonly ICommandBuilder CommandBuilder;
        protected readonly ITranslatorBuilder Translator;

        public MainController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator)
        {
            QueryBuilder = queryBuilder;
            CommandBuilder = commandBuilder;
            Translator = translator;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View("~/Views/Admin/Index.cshtml");
        }

        [Authorize]
        public ActionResult MainMenu()
        {
            var currentController = ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();
            var currentAction = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();


            var menu = new MenuItemCollection();

            menu.Add(new MenuItem("Филиалы", "", "", Url.Action("Index", "Places"))
            {
                Selected = "Places" == currentController
            });

            var catalog = new MenuItem("Каталог");
            var products = new MenuItem("Продукты");
            products.ChildItems.Add(new MenuItem("Список", "", "", Url.Action("Index", "Products"))
            {
                Selected = "Products" == currentController && !currentAction.Contains("FieldType")
            });
            products.ChildItems.Add(new MenuItem("Типы полей", "", "", Url.Action("FieldTypes", "Products"))
            {
                Selected = "Products" == currentController && currentAction.Contains("FieldType")
            });

            products.Selected = products.ChildItems.Cast<MenuItem>().Any(c => c.Selected);

            catalog.ChildItems.Add(products);

            catalog.ChildItems.Add(new MenuItem("Категории", "", "", Url.Action("Index", "Categories"))
            {
                Selected = "Categories" == currentController
            });
            catalog.ChildItems.Add(new MenuItem("Отзывы", "", "", Url.Action("Index", "Reviews"))
            {
                Selected = "Reviews" == currentController
            });

            catalog.Selected = catalog.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            menu.Add(catalog);
            
            var shoporder = new MenuItem("Заказы");
            shoporder.ChildItems.Add(new MenuItem("Заказы", "", "", Url.Action("Index", "ShopOrders"))
            {
                Selected = "ShopOrders" == currentController && !currentAction.Contains("FieldType")
            });
            shoporder.ChildItems.Add(new MenuItem("Типы полей", "", "", Url.Action("FieldTypes", "ShopOrders"))
            {
                Selected = "ShopOrders" == currentController && currentAction.Contains("FieldType")
            });

            shoporder.ChildItems.Add(new MenuItem("Статусы", "", "", Url.Action("Index", "ShopOrderStates"))
            {
                Selected = "ShopOrderStates" == currentController
            });

            shoporder.Selected = shoporder.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            menu.Add(shoporder);

            menu.Add(new MenuItem("Клиенты", "", "", Url.Action("Index", "Customers"))
            {
                Selected = "Customers" == currentController
            });

            var images = new MenuItem("Изображения");
            images.ChildItems.Add(new MenuItem("Галерея", "", "", Url.Action("Index", "Images"))
            {
                Selected = "Images" == currentController
            });
            images.ChildItems.Add(new MenuItem("Типы", "", "", Url.Action("Index", "ImageTypes"))
            {
                Selected = "ImageTypes" == currentController
            });

            images.Selected = images.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            menu.Add(images);

            var banners = new MenuItem("Баннеры");
            banners.ChildItems.Add(new MenuItem("Баннеры", "", "", Url.Action("Index", "Banners"))
            {
                Selected = "Banners" == currentController
            });
            banners.ChildItems.Add(new MenuItem("Типы", "", "", Url.Action("Index", "BannerTypes"))
            {
                Selected = "BannerTypes" == currentController
            });

            banners.Selected = banners.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            menu.Add(banners);

            var loyality = new MenuItem("Программы лояльности");
            loyality.ChildItems.Add(new MenuItem("Бонусы", "", "", Url.Action("BonusProgram", "Loyality"))
            {
                Selected = "Loyality" == currentController
            });
            loyality.ChildItems.Add(new MenuItem("Купоны", "", "", Url.Action("Index", "Coupons"))
            {
                Selected = "Coupons" == currentController
            });

            loyality.Selected = loyality.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            menu.Add(loyality);

            var settings = new MenuItem("Настройки");
            settings.ChildItems.Add(new MenuItem("Общие", "", "", Url.Action("Index", "Settings"))
            {
                Selected = "Settings" == currentController && currentAction.Contains("Index")
            });
            settings.ChildItems.Add(new MenuItem("Импорт YML", "", "", Url.Action("ImportYml", "Settings"))
            {
                Selected = "Settings" == currentController && currentAction.Contains("ImportYml")
            });

            var locale = new MenuItem("Локализация");
            locale.ChildItems.Add(new MenuItem("Строки", "", "", Url.Action("Index", "LocaleStrings"))
            {
                Selected = "LocaleStrings" == currentController
            });
            locale.ChildItems.Add(new MenuItem("Типы", "", "", Url.Action("Index", "LocaleStringTypes"))
            {
                Selected = "LocaleStringTypes" == currentController
            });
            locale.Selected = locale.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            settings.ChildItems.Add(locale);



            var users = new MenuItem("Пользователи");
            users.ChildItems.Add(new MenuItem("Список", "", "", Url.Action("Index", "Users"))
            {
                Selected = "Users" == currentController
            });
            users.ChildItems.Add(new MenuItem("Роли", "", "", Url.Action("Index", "Roles"))
            {
                Selected = "Roles" == currentController
            });

            users.Selected = users.ChildItems.Cast<MenuItem>().Any(c => c.Selected);

            settings.ChildItems.Add(users);

            settings.Selected = settings.ChildItems.Cast<MenuItem>().Any(c => c.Selected);
            menu.Add(settings);

            return PartialView("~/Views/Admin/MainMenu.cshtml",menu);
        }

        public ActionResult Install()
        {
            var conf = new Configuration().Configure();

            var mapper = new ModelMapper();
            var hibernateConfiguration = ConfigurationManager.GetSection("hibernate-configuration") as HibernateConfiguration;
            if (hibernateConfiguration != null)
            {
                foreach (var map in hibernateConfiguration.SessionFactory.Mappings)
                {
                    mapper.AddMappings(Assembly.Load(map.Assembly).GetTypes());
                }
            }
            conf.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());

            try
            {
                new SchemaValidator(conf).Validate();
            }
            catch
            {
                var upd = new SchemaUpdate(conf);
                upd.Execute(true, true);
                if (upd.Exceptions.Any())
                    throw upd.Exceptions.First();
            }

            //проверяем наличие группы администратора и пользователя
            var session = SessionManager.GetSession();
            var admRole = session.Query<Role>().FirstOrDefault(r => r.Code == "Admins");
            if (admRole == null)
            {
                admRole = new Role { Code = "Admins", Name = "Администраторы" };
                session.Save(admRole);
            }

            var user = session.Query<User>().FirstOrDefault(u => u.Roles.Contains(admRole));
            if (user == null)
            {
                user = new User
                {
                    Login = "admin",
                    Roles = new List<Role> {admRole},
                    Salt = new CreateHashQuery().Ask(new CreateHashCriterion { Data = "1" })
                };
                session.SaveOrUpdate(user);
                session.Flush();
            }
            //Сохраняем настройки(если их нет, они заинициилизируются)
            CommandBuilder.Execute(
                new CreateOrUpdateObjectContext<Settings>(QueryBuilder.For<Settings>().With(new SettingsCriterion())));

            return Json("Успех", JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ValidateDataBaseAndUpdate()
        {
            var conf = new Configuration().Configure();

            var mapper = new ModelMapper();
            var hibernateConfiguration = ConfigurationManager.GetSection("hibernate-configuration") as HibernateConfiguration;
            if (hibernateConfiguration != null)
            {
                foreach (var map in hibernateConfiguration.SessionFactory.Mappings)
                {
                    mapper.AddMappings(Assembly.Load(map.Assembly).GetTypes());
                }
            }
            conf.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());

            try
            {
                new SchemaValidator(conf).Validate();
            }
            catch
            {
                var upd = new SchemaUpdate(conf);
                upd.Execute(true, true);
                if (upd.Exceptions.Any())
                    throw upd.Exceptions.First();
            }
            return Json("Успех",JsonRequestBehavior.AllowGet);
        }

    }
}
