﻿using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class CouponsController : BaseObjectCrudController<Coupon,CouponDto>
    {
        public CouponsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "Coupons")
        {
        }
    }
}