﻿using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
	[Authorize]
	public class CustomObjectTypesController : BaseObjectCrudController<CustomObjectType, CustomObjectType>
	{
		public CustomObjectTypesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
			ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "CustomObjectTypes")
		{
		}
	}
}