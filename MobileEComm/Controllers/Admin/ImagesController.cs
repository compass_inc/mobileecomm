﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using NHibernate.Util;
using Services.Base.CommandContexts;
using Services.Base.Criteria;
using Services.Images.Criteria;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ImagesController : BaseObjectCrudController<Image, ImageDto>
    {
        public ImagesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "Images")
        {
            UpdateAction = () => Url.Action("IndexData", "Images");
            UpdatableBlock = ".gallery-container";
        }

        private IEnumerable<ImageType> ImageTypes
        {
            get
            {
                return
                    QueryBuilder.For<ICollection<ImageType>>()
                        .With(new BaseObjectCriterion());
            }
        }

        public override ActionResult Add()
        {
            return null;
        }

        public override ActionResult Add(ImageDto o)
        {
            if (Request.Files.Any())
            {
                for (var i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    if (file == null)
                        continue;

                    var filePath = ConfigurationManager.AppSettings["BaseImageDir"] + file.FileName;
                    var savePath = ConfigurationManager.AppSettings["SaveImageDir"] + file.FileName;
                    var counter = 1;

                    while (System.IO.File.Exists(HttpRuntime.AppDomainAppPath + savePath))
                    {
                        counter++;
                        savePath = ConfigurationManager.AppSettings["SaveImageDir"] + file.FileName.Split('.').First() +
                                   counter + "." +
                                   file.FileName.Split('.').Last();
                        filePath = ConfigurationManager.AppSettings["BaseImageDir"] + file.FileName.Split('.').First() +
                                   counter + "." +
                                   file.FileName.Split('.').Last();
                    }

                    file.SaveAs(HttpRuntime.AppDomainAppPath + savePath);

                    o.Description = file.FileName;
                    o.Path = filePath.Replace(@"\\", @"\");

                    var img = Translator.Map<ImageDto, Image>(o);
                    img.SavePath = savePath;

                    CommandBuilder.Execute(new CreateOrUpdateObjectContext<Image>(img));
                }
            }
            return Json("success");
        }

        public override ActionResult Index()
        {
            ViewData.Add("ImageTypes", ImageTypes);
            Response.Cookies.Add(new HttpCookie(ControllerName + "_TypeId", "0"));
            return base.Index();
        }

        public override ActionResult IndexData()
        {
            GetPagination();

            ViewData.Add("ImageTypes", ImageTypes);

            var typeId = Request.Params.AllKeys.Contains("TypeId")
                ? ulong.Parse(Request.Params["TypeId"])
                : Request.Cookies.AllKeys.Contains(ControllerName + "_TypeId")
                    ? ulong.Parse(Request.Cookies[ControllerName + "_TypeId"].Value)
                    : 0;

            Response.Cookies.Add(new HttpCookie(ControllerName + "_TypeId", typeId.ToString()));

            ViewBag.CurrentTypeId = typeId;

            var criterion = new ImagesCriterion {TypeId = typeId, PageNumber = PageNumber, PageSize = PageSize};
            return IndexData(criterion, criterion);
        }

        public override ActionResult Edit(ulong objectId)
        {
            return
                Add(
                    Translator.Map<Image, ImageDto>(
                        QueryBuilder.For<ICollection<Image>>()
                            .With(new ImagesCriterion {Ids = new[] {objectId}})
                            .First()));
        }

        public override ActionResult Delete(ulong objectId, string confirmPass= "")
        {
            var image = QueryBuilder.For<ICollection<Image>>()
                .With(new BaseObjectCriterion {Ids = new List<ulong> {objectId}})
                .FirstOrDefault();
            if (image != null)
            {
                CommandBuilder.Execute(new DeleteObjectContext<Image> {ObjectIds = new List<ulong> {objectId}});
                if (!string.IsNullOrEmpty(image.Path) && !image.Path.Contains("http"))
                    System.IO.File.Delete(HttpRuntime.AppDomainAppPath + image.SavePath);
            }

            return Json("success");
        }
    }
}