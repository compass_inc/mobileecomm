﻿using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
    [Authorize]
    public class ProductTypesController : BaseObjectCrudController<ProductType, ProductType>
    {
        public ProductTypesController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator) : base(queryBuilder, commandBuilder, translator, "ProductTypes")
        {
        }
    }
}