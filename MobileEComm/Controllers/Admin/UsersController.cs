﻿using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;
using Services.Users.CommandContexts;

namespace MobileEComm.Controllers.Admin
{
    [Authorize(Roles = "Admins")]
    public class UsersController : BaseObjectCrudController<User,UserDto>
    {
        public UsersController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator)
            : base(queryBuilder, commandBuilder, translator, "Users")
        {
            UpdateAction = () => Url.Action("IndexData", "Users");
            UpdatableBlock = "#users-table-block";
        }

        public ActionResult AddWPwd(UserDto dto, string password)
        {
            return Add(dto,
                new CreateOrUpdateUserContext(null) { Password = password });
        }

        public HttpResponseMessage ChangePwd(ulong userId, string password)
        {
            CommandBuilder.Execute(new ChangeUserPasswordContext { AuthObjectId = userId, NewPassword = password });
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

    }
}
