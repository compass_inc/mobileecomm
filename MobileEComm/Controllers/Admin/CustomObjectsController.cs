﻿using Domain;
using DomainTranslators;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Admin
{
	[Authorize]
	public class CustomObjectsController : BaseObjectCrudController<CustomObject, CustomObject>
	{
		public CustomObjectsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, ITranslatorBuilder translator)
			: base(queryBuilder, commandBuilder, translator, "CustomObjects")
		{
			UpdateAction = () => Url.Action("IndexData", "CustomObjects");
			UpdatableBlock = "#customobjects-table-block";
		}
	}
}