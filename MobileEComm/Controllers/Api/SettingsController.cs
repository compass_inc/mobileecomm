﻿using System.Web.Http;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Infrastructure.Interfaces;
using Services.Settings.Criteria;

namespace MobileEComm.Controllers.Api
{
    public class SettingsController : ApiController
    {
            private readonly IQueryBuilder _query;
        private readonly ITranslatorBuilder _translator;

        public SettingsController(IQueryBuilder queryBuilder, ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _translator = translator;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public SettingsDto Index()
        {
            return _translator.Map<Settings, SettingsDto>(_query.For<Settings>().With(new SettingsCriterion()));
        }
    }
}