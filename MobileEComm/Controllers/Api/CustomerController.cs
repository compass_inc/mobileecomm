﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Base.CommandContexts;
using Services.Customer.CommandContexts;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;
using Services.ShopOrder.Criteria;

namespace MobileEComm.Controllers.Api
{
    public class CustomerController : ApiController
    {

        private readonly IQueryBuilder _query;
        private readonly ICommandBuilder _command;
        private readonly ITranslatorBuilder _translator;
        public CustomerController(IQueryBuilder query, ICommandBuilder command, ITranslatorBuilder translator)
        {
            _query = query;
            _command = command;
            _translator = translator;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public HttpResponseMessage Auth(ValidateCustomerPasswordCriterion validateCriterion)
        {
            if (!_query.For<bool>().With(validateCriterion))
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Ошибка авторизации");

            _command.Execute(new SetAuthTokenContext {CustomerEmail = validateCriterion.Email});

            var token = _query.For<Guid?>().With(new GetAuthTokenCriterion {Email = validateCriterion.Email});

            return Request.CreateResponse(HttpStatusCode.OK, token);
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public HttpResponseMessage Register(ValidateCustomerPasswordCriterion validateCriterion)
        {
            try
            {
                _command.Execute(new CreateOrUpdateCustomerContext(new Customer {Login = validateCriterion.Email, Email = validateCriterion.Email})
                {
                    Password = validateCriterion.Password
                });
            }
            catch (ArgumentException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Conflict, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public void ChangePassword(ChangeCustomerPasswordContext context)
        {
            var customer =
                 _query.For<ICollection<Customer>>()
                     .With(new CustomersCriterion { Email = HttpContext.Current.User.Identity.Name })
                     .First();

            context.AuthObjectId = customer.Id;

            _command.Execute(context);
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public HttpResponseMessage ResetPassword(ResetCustomerPasswordContext context)
        {
            try
            {
                _command.Execute(context);
            }
            catch (ArgumentException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Conflict, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public ulong AddAddress(CustomerAddressDto address)
        {
            var customer =
                 _query.For<ICollection<Customer>>()
                     .With(new CustomersCriterion { Email = HttpContext.Current.User.Identity.Name })
                     .First();

            var addrObj = _translator.Map<CustomerAddressDto, CustomerAddress>(address);

            _command.Execute(new AddRemoveAddressesContext
            {
                CustomerId = customer.Id,
                AddingAddresses = new[] { addrObj }
            });

            return addrObj.Id;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public void RemoveAddress(CustomerAddressDto address)
        {
            var customer =
                 _query.For<ICollection<Customer>>()
                     .With(new CustomersCriterion { Email = HttpContext.Current.User.Identity.Name })
                     .First();

            _command.Execute(new AddRemoveAddressesContext
            {
                CustomerId = customer.Id,
                RemovingAddresses = new[] { _translator.Map<CustomerAddressDto, CustomerAddress>(address) }
            });
        }

        public class FavoritesContext
        {
            public ulong ProductId { get; set; }
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public void AddFavorite(FavoritesContext context)
        {
            var customer =
                _query.For<ICollection<Customer>>()
                    .With(new CustomersCriterion { Email = HttpContext.Current.User.Identity.Name })
                    .First();

            _command.Execute(new AddRemoveFavoritesContext
            {
                CustomerId = customer.Id,
                AddingProducts = new[] { context.ProductId }
            });
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public void RemoveFavorite(FavoritesContext context)
        {
            var customer =
                _query.For<ICollection<Customer>>()
                    .With(new CustomersCriterion { Email = HttpContext.Current.User.Identity.Name })
                    .First();

            _command.Execute(new AddRemoveFavoritesContext
            {
                CustomerId = customer.Id,
                RemovingProducts = new[] { context.ProductId }
            });
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public CustomerDto Profile()
        {
            return
                _translator.Map<Customer, CustomerDto>(
                    _query.For<ICollection<Customer>>()
                        .With(new CustomersCriterion {Email = HttpContext.Current.User.Identity.Name})
                        .First());
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public void EditProfile(CustomerDto profile)
        {
            var newEmail = profile.Email;
            profile.Email = HttpContext.Current.User.Identity.Name;
            _command.Execute(new CreateOrUpdateCustomerContext(_translator.Map<CustomerDto, Customer>(profile))
            {
                NewLogin = (newEmail == profile.Email ? null : newEmail)
            });
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public IDictionary<ulong,string> UploadAvatar()
        {
            if (HttpContext.Current.Request.Files.Count == 0)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = "Нет изображения для загрузки!"
                });

            var file = HttpContext.Current.Request.Files[0];

            var filePath = ConfigurationManager.AppSettings["BaseImageDir"] + file.FileName;
            var savePath = ConfigurationManager.AppSettings["SaveImageDir"] + file.FileName;
            var counter = 1;

            while (System.IO.File.Exists(HttpRuntime.AppDomainAppPath + savePath))
            {
                counter++;
                filePath = ConfigurationManager.AppSettings["BaseImageDir"] + file.FileName.Split('.').First() + counter +
                           "." +
                           file.FileName.Split('.').Last();
                savePath = ConfigurationManager.AppSettings["SaveImageDir"] + file.FileName.Split('.').First() + counter +
                          "." +
                          file.FileName.Split('.').Last();
            }


            file.SaveAs(HttpRuntime.AppDomainAppPath + savePath);

            var customer =
                _query.For<ICollection<Customer>>()
                    .With(new CustomersCriterion {Email = HttpContext.Current.User.Identity.Name})
                    .First();
            var image = new Image { Description = file.FileName, Path = filePath };
             _command.Execute(new CreateOrUpdateObjectContext<Image>(image));

            customer.Avatar = image;

            _command.Execute(new CreateOrUpdateCustomerContext(customer));

            return new Dictionary<ulong, string> {{customer.Avatar.Id, customer.Avatar.Path}};
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public ICollection<ShopOrderDto> ShopOrders()
        {
            var customer =
                _query.For<ICollection<Customer>>()
                    .With(new CustomersCriterion {Email = HttpContext.Current.User.Identity.Name})
                    .First();
            return
                _query.For<ICollection<ShopOrder>>()
                    .With(new ShopOrdersCriterion { CustomerId = customer.Id})
                    .Select(so => _translator.Map<ShopOrder, ShopOrderDto>(so))
                    .ToList();
        }
    }
}