﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Banners.Criteria;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Api
{
    public class BannersController : ApiController
    {
        private readonly IQueryBuilder _query;
        private readonly ICommandBuilder _command;
        private readonly ITranslatorBuilder _translator;

        public BannersController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _command = commandBuilder;
            _translator = translator;
        }
        public class BannersContext
        {
            public string BannerTypeCode { get; set; }
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public ICollection<BannerDto> Get(BannersCriterion criterion)
        {
            return _query.For<ICollection<Banner>>()
                .With(criterion)
                .Select(c => _translator.Map<Banner, BannerDto>(c))
                .ToList();
        }
    }
}
