﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Base.CommandContexts;
using Services.Category.Criteria;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;
using Services.Product.Criteria;
using Services.Reviews.Criteria;

namespace MobileEComm.Controllers.Api
{
    public class CatalogController : ApiController
    {
        private readonly IQueryBuilder _query;
        private readonly ICommandBuilder _command;
        private readonly ITranslatorBuilder _translator;

        public CatalogController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _command = commandBuilder;
            _translator = translator;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public ICollection<CategoryDto> Categories(CategoriesCriterion criterion)
        {
            if (Request.Properties.Any(p => p.Key == "PlaceId"))
                criterion.PlaceId = ulong.Parse((string)Request.Properties["PlaceId"]);
            else if(Request.Properties.Any(p => p.Key == "CityName"))
                criterion.CityName = (string)Request.Properties["CityName"];

            return _query.For<ICollection<Category>>()
                .With(criterion)
                .Select(c => _translator.Map<Category, CategoryDto>(c))
                .ToList();
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public ICollection<ProductDto> Products(ProductsCriterion criterion)
        {
            if (Request.Properties.Any(p => p.Key == "PlaceId"))
                criterion.PlaceId = ulong.Parse((string)Request.Properties["PlaceId"]);
            else if (Request.Properties.Any(p => p.Key == "CityName"))
                criterion.CityName = (string)Request.Properties["CityName"];

            return _query.For<ICollection<Product>>()
                .With(criterion)
                .Select(p => _translator.Map<Product, ProductDto>(p))
                .ToList();
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public int ProductsCount(ProductsCriterion criterion)
        {
            if (Request.Properties.Any(p => p.Key == "PlaceId"))
                criterion.PlaceId = ulong.Parse((string)Request.Properties["PlaceId"]);
            else if (Request.Properties.Any(p => p.Key == "CityName"))
                criterion.CityName = (string)Request.Properties["CityName"];

            return _query.For<int>().With(criterion);
        }

        #region Reviews

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public ICollection<ReviewDto> Reviews(ActiveReviewsCriterion criterion)
        {
            return
                _query.For<ICollection<Review>>()
                    .With(criterion)
                    .Select(r => _translator.Map<Review, ReviewDto>(r))
                    .ToList();
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public void AddReview(ReviewDto review)
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null &&
                HttpContext.Current.User.Identity.IsAuthenticated)
                review.CustomerId =
                    _query.For<ICollection<Customer>>()
                        .With(new CustomersCriterion() {Email = HttpContext.Current.User.Identity.Name})
                        .First().Id;

            if (review.Id > 0 || review.IsOwnerAnswer)
                throw new Exception("Данное действие запрещено");

            _command.Execute(new CreateOrUpdateObjectContext<Review>(_translator.Map<ReviewDto, Review>(review)));
        }

        #endregion
    }
}