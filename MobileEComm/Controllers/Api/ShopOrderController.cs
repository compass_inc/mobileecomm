﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Base.Criteria;
using Services.BonusProgram.Criteria;
using Services.Coupon.Criteria;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;
using Services.ShopOrder.CommandContext;

namespace MobileEComm.Controllers.Api
{
    public class ShopOrderController : ApiController
    {
        private readonly IQueryBuilder _query;
        private readonly ICommandBuilder _command;
        private readonly ITranslatorBuilder _translator;

        public ShopOrderController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder,
            ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _command = commandBuilder;
            _translator = translator;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public ICollection<ObjectFieldDto> GetFields()
        {
            return _query.For<ICollection<ObjectField<ShopOrder>>>()
                .With(new GenerateObjectFieldsCriterion<ShopOrder>())
                .Select(s => _translator.Map<ObjectField<ShopOrder>, ObjectFieldDto>(s))
                .ToList();
        }

        public class ShopOrderAddContext
        {
            public ShopOrderDto Order { get; set; }
            public string CouponCode { get; set; }
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public void Add(ShopOrderAddContext context)
        {
            _command.Execute(new CreateOrUpdateShopOrderContext(_translator.Map<ShopOrderDto, ShopOrder>(context.Order))
            {
                CouponCode = context.CouponCode
            });
        }


        public class MaxWriteOffBonusContext
        {
            public decimal ShopOrderSum { get; set; }
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        [System.Web.Http.Authorize]
        public decimal GetMaxWriteOffBonusSum(MaxWriteOffBonusContext context)
        {
            var customerId =
                _query.For<ICollection<Customer>>()
                    .With(new CustomersCriterion() {Email = HttpContext.Current.User.Identity.Name})
                    .First().Id;
            return _query.For<IDictionary<ulong, decimal>>()
                .With(new MaxWriteOffBonusSumCriterion {ShopOrderSum = context.ShopOrderSum, CustomerId = customerId})
                .FirstOrDefault().Value;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public IDictionary<ulong, decimal> GetCouponDiscountSum(GetCouponDiscountCriterion criterion)
        {
            return _query.For<IDictionary<ulong, decimal>>().With(criterion);
        }
    }
}