﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Domain;
using Domain.Dto;
using DomainTranslators;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace MobileEComm.Controllers.Api
{
    public class PlacesController : ApiController
    {
        private readonly IQueryBuilder _query;
        private readonly ITranslatorBuilder _translator;

        public PlacesController(IQueryBuilder queryBuilder, ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _translator = translator;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public ICollection<PlaceDto> Index()
        {
            return
                _query.For<ICollection<Place>>()
                    .With(new BaseObjectCriterion())
                    .Select(p => _translator.Map<Place, PlaceDto>(p))
                    .ToList();
        }

        public class PlacesContext
        {
            public ulong Id { get; set; }
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public PlaceDto Get(PlacesContext context)
        {
            return
                _query.For<ICollection<Place>>()
                    .With(new BaseObjectCriterion() {Ids = new List<ulong>{ context.Id} })
                    .Select(p => _translator.Map<Place, PlaceDto>(p))
                    .FirstOrDefault();
        }


    }
}
