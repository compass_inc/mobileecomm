﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MobileEComm.HttpHandlers
{
    public class OptionsHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return request.Method == HttpMethod.Options
                ? Task.Factory.StartNew(() => new HttpResponseMessage(HttpStatusCode.OK), cancellationToken)
                : base.SendAsync(request, cancellationToken);
        }
    }
}