﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MobileEComm.Infrastructure;
using Services.Customer.Criteria;
using Services.Customer.Queries;
using Utils;

namespace MobileEComm.HttpHandlers
{
    public class InitDataHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(
      HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Headers.Any(pair => pair.Key == "Place"))
                request.Properties["PlaceId"] = request.Headers.First(h => h.Key == "Place").Value.FirstOrDefault();
            else if(request.Headers.Any(pair => pair.Key == "CityName"))
                request.Properties["CityName"] = Transliteration.Back(request.Headers.First(h => h.Key == "CityName").Value.FirstOrDefault());

            if (request.Headers.Any(pair => pair.Key == "AuthToken"))
            {
                var token = Guid.Parse(request.Headers.First(h => h.Key == "AuthToken").Value.First());
                var customer = new CustomersQuery().Ask(new CustomersCriterion {Token = token}).FirstOrDefault();
                if (customer != null)
                {
                    Thread.CurrentPrincipal =
                        HttpContext.Current.User =
                            new CustomerPrincipal
                            {
                                Identity = new CustomerIdentity {Name = customer.Login, IsAuthenticated = true}
                            };
                }
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}