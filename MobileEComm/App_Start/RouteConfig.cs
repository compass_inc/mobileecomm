﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MobileEComm
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "Admin",
                url: "admin/{controller}/{action}/{id}",
                defaults: new { controller = "Main", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "MobileEComm.Controllers.Admin" }
                );

            routes.MapRoute(
               name: "default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Main", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "MobileEComm.Controllers.Admin" }
               );

            //routes.MapRoute(
            //    name: "Client",
            //    url: "client/{controller}/{action}/{id}",
            //    defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional},
            //    namespaces: new[] {"MobileEComm.Controllers.Client"}

            //    );
        }
    }
}