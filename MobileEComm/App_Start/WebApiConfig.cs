﻿using System.Net.Http.Headers;
using System.Web.Http;
using MobileEComm.HttpHandlers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MobileEComm
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new {action = "Index"}
                );

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            
            config.MessageHandlers.Add(new InitDataHandler());
            config.MessageHandlers.Add(new OptionsHandler());
        }
    }
}
