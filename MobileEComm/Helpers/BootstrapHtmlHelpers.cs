﻿using System;
using System.Web.Mvc;

namespace MobileEComm.Helpers
{
    static class Types
    {
        public static string String = typeof (string).Name;
        public static string Date = typeof (DateTime).Name;

        public static bool IsNumeric(string type)
        {
            return type == typeof (int).Name || type == typeof (decimal).Name || type == typeof (double).Name;
        }
    }


    public static class BootstrapHtmlHelpers
    {
        public static MvcHtmlString BootstrapEditorForModel(this HtmlHelper htmlHelper)
        {
            var resHtml = "";
            var props = htmlHelper.ViewData.ModelMetadata.Properties;
            foreach (var prop in props)
            {
                resHtml += "<div class=\"form-group\">";

                if (prop.ModelType.Name == Types.String || Types.IsNumeric(prop.ModelType.Name))
                {
                    resHtml += "<label>" +
                               (string.IsNullOrEmpty(prop.DisplayName) ? prop.PropertyName : prop.DisplayName) +
                               "</label>";
                    resHtml += "<input type=\"text\" class=\"form-control\"" + ">";
                }
                else if(prop.ModelType.Name == Types.Date)
                {
                    
                }

                resHtml += "</div>";
            }
            return new MvcHtmlString(resHtml);
        }

        public static MvcHtmlString BootstrapChangeSortBlock(this HtmlHelper htmlHelper, object sortIndex, string action)
        {
           return new MvcHtmlString(string.Format("<span> {0} </span><span onclick=\"return false;\" class=\"pointer glyphicon glyphicon-arrow-up\"> </span><span class=\"pointer glyphicon glyphicon-arrow-down\">",sortIndex));

        }
    }
}