﻿using System.Security.Principal;

namespace MobileEComm.Infrastructure
{
    public class CustomerPrincipal:IPrincipal
    {
        public bool IsInRole(string role)
        {
            return false;
        }

        public IIdentity Identity { get; set; }
    }

    public class CustomerIdentity: IIdentity
    {
        public string Name { get; set; }
        public string AuthenticationType { get; private set; }
        public bool IsAuthenticated { get; set; }
    }
}