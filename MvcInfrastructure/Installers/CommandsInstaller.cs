﻿using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Services.Infrastructure;
using Services.Infrastructure.Interfaces;
using ICommandFactory = Services.Infrastructure.Interfaces.ICommandFactory;

namespace MvcInfrastructure.Installers
{
    public class CommandsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var commands = Types.FromAssemblyInDirectory(new AssemblyFilter(HttpRuntime.BinDirectory))
                                .BasedOn(typeof(ICommand<>))
                                .WithService
                                .Base()
                                .WithServiceAllInterfaces()
                                .LifestyleTransient();

            container.Register(commands,
                Component.For<ICommandBuilder>().ImplementedBy<CommandBuilder>().LifestyleTransient(),
                Component.For<ICommandFactory>()
                    .ImplementedBy<CommandFactory>()
                    .DependsOn(Dependency.OnValue("container", container))
                    .LifestyleTransient());
        }
    }
}