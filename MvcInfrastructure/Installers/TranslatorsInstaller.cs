﻿using System.Web;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DomainTranslators;

namespace MvcInfrastructure.Installers
{
    public class TranslatorsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var translators = Types.FromAssemblyInDirectory(new AssemblyFilter(HttpRuntime.BinDirectory))
                   .BasedOn(typeof(IDomainTranslator<,>))
                   .WithService
                   .AllInterfaces()
                   .LifestyleTransient();


            container.Register(translators,
                Component.For<ITranslatorBuilder>().ImplementedBy<TranslatorBuilder>().LifestyleTransient(),
                Component.For<ITranslatorFactory>().AsFactory().LifestyleTransient());
        }
    }
}