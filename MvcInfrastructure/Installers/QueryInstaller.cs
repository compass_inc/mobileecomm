﻿using System.Web;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Services.Infrastructure;
using Services.Infrastructure.Interfaces;

namespace MvcInfrastructure.Installers
{
    public class QueryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            var queries = Types.FromAssemblyInDirectory(new AssemblyFilter(HttpRuntime.BinDirectory))
                               .BasedOn(typeof(IQuery<,>))
                               .WithService
                               .AllInterfaces()
                               .LifestyleTransient();

            container.Register(queries,
                               Component.For<IQueryBuilder>().AsFactory().LifestyleTransient(),
                               Component.For<IQueryFactory>().AsFactory().LifestyleTransient(),
                               Component.For(typeof(IQueryFor<>)).ImplementedBy(typeof(QueryForExtended<>)).LifestyleTransient());
        }
    }
}