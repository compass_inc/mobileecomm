import axios from 'axios';

import { HOST } from '../utils/constants';

import { fetchFilialsReceived, fetchCitiesReceived } from './filial';
import { fetchSettingsReceived } from './settings';
import { fetchAdditionalFieldsReceived } from './order';
import { setAuthToken, loadProfile } from './profile';

export function load() {
    return dispatch => {
        axios.get(`${HOST}/api/settings`)
            .then(res => {
                var settings = res.data;
                dispatch(fetchSettingsReceived(settings));

                axios.get(`${HOST}/api/places`)
                    .then(res => {
                        setTimeout(() => {
                            var filials = getFilials(res.data, settings.onlyCity);
                            dispatch(fetchFilialsReceived(filials));
                            dispatch(setFirstRunFlag( localStorage.getItem('FirstRunFlag') === null ));
                            dispatch({ type: 'COMPLETE_LOADING' });
                        }, 2000);
                    });

                if(settings.noOrderMode === false)
                    axios.get(`${HOST}/api/shoporder/GetFields`)
                        .then(res => {
                            res.data.sort((a, b) => a.sortOrder - b.sortOrder);
                            dispatch(fetchAdditionalFieldsReceived(settings));
                        });

                const token = localStorage.getItem('AuthToken');
                if(token) {
                    dispatch(setAuthToken(token));
                    dispatch(loadProfile());
                }
            });
    }
}

function getFilials(filials, onlyCity) {
    if(!onlyCity)
        return filials;

    return filials.reduce((r, filial) => {
        if(r.find(f => f.city === filial.city) === undefined)
            return r.concat(filial);
        return r;
    }, []);
}

export function setFirstRunFlag(flag) {
    return {
        type: 'SET_FIRST_RUN_FLAG',
        payload: flag
    }
}