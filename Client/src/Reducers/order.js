function getInitialState() {
	return {
		additionalFields: [],
		maxBonusSum: 0,
		discounts: undefined,
		coupon: undefined
	}
}

export default function(state = getInitialState(), action) {
	switch (action.type) {
		case 'FETCH_ADDITIONAL_FIELDS_RECEIVED':
			return { ...state, additionalFields: action.payload };
		case 'GET_MAX_BONUS_SUM_RECEIVED':
			return { ...state, maxBonusSum: action.payload };
		case 'GET_COUPON_DISCOUNT_SUM_RECEIVED':
			return { ...state, discounts: action.payload };
		case 'SET_COUPON':
			return { ...state, coupon: action.payload };
		case 'CLEAR_DISCOUNTS': {
			return { ...getInitialState(), additionalFields: state.additionalFields }
		}
		default: return state;
	}
}