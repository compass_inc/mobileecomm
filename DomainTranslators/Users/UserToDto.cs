﻿using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;

namespace DomainTranslators.Users
{
    public class UserToDto : IDomainTranslator<User,UserDto>
    {
         private readonly ITranslatorBuilder _translator;

         public UserToDto(ITranslatorBuilder translator)
        {
            _translator = translator;
        }

        public UserDto Map(User obj)
        {
            var dto = new UserDto();
            dto.InjectFrom(obj);

            dto.Status = DescribedEnum<UserState>.GetDescription(obj.Status);

            if (obj.Roles != null && obj.Roles.Any())
                dto.Roles = obj.Roles.Select(_translator.Map<Role, RoleDto>).ToList();

            return dto;
        }
    }
}