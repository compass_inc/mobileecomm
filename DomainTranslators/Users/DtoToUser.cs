﻿using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;

namespace DomainTranslators.Users
{
    public class DtoToUser : IDomainTranslator<UserDto, User>
    {
        private readonly ITranslatorBuilder _translator;

        public DtoToUser(ITranslatorBuilder translator)
        {
            _translator = translator;
        }

        public User Map(UserDto dto)
        {
            var user = new User();
            user.InjectFrom(dto);

            if (!string.IsNullOrEmpty(dto.Status))
                user.Status = DescribedEnum<UserState>.GetValue(dto.Status);

            if (dto.Roles != null && dto.Roles.Any())
                user.Roles = dto.Roles.Select(_translator.Map<RoleDto, Role>).ToList();

            return user;
        }
    }
}