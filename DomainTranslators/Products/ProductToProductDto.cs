﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Products
{
    public class ProductToProductDto : IDomainTranslator<Product, ProductDto>
    {
        private readonly ITranslatorBuilder _translator;
        private readonly IQueryBuilder _query;

        public ProductToProductDto(ITranslatorBuilder translator, IQueryBuilder query)
        {
            _translator = translator;
            _query = query;
        }

        public ProductDto Map(Product obj)
        {
            var generatedFields = _query.For<ICollection<ObjectField<Product>>>()
                        .With(new GenerateObjectFieldsCriterion<Product>());              

            var dto = new ProductDto();
            dto.InjectFrom(obj);

            if (obj.EndActivityDate != null)
                dto.EndActivityDate = obj.EndActivityDate.Value.ToShortDateString();

            if (obj.Categories != null)
                dto.Categories = obj.Categories.Select(c => _translator.Map<Category,CategoryDto>(c)).ToList();

            if (obj.Labels != null)
                dto.Labels = obj.Labels.Select(l => _translator.Map<Banner, BannerDto>(l)).ToList();

            if (obj.Images != null)
                dto.Images = obj.Images.Select(i => _translator.Map<Image, ImageDto>(i)).ToList();

            if (obj.Featured != null)
                dto.Featured = obj.Featured.ToDictionary(f => f.Id, f => f.Name);

            if (obj.Places != null)
                dto.Places = obj.Places.ToDictionary(f => f.Id, f => f.Name);

            if (obj.Prices != null)
                dto.Prices = obj.Prices.Select(p => _translator.Map<Price, PriceDto>(p)).ToList();

            if (obj.CategoriesOrders != null)
                dto.CategoryOrders =
                    obj.CategoriesOrders.Select(
                        co => _translator.Map<ProductToCategoryOrder, ProductToCategoryOrderDto>(co)).ToList();

            dto.Fields =
                (obj.Fields ?? generatedFields)
                    .Select(o => _translator.Map<ObjectField<Product>, ObjectFieldDto>(o))
                    .ToList();

            if (dto.Fields.Count < generatedFields.Count)
                dto.Fields =
                    dto.Fields.Concat(
                        generatedFields.Where(gf => !dto.Fields.Select(f => f.FieldTypeId).Contains(gf.FieldType.Id))
                            .Select(o => _translator.Map<ObjectField<Product>, ObjectFieldDto>(o))
                            .ToList()).ToList();

            dto.Fields = dto.Fields.OrderBy(f => f.SortOrder).ToList();

            if (obj.Type != null)
            {
                dto.TypeId = obj.Type.Id;
                dto.TypeCode = obj.Type.Code;
                dto.TypeName = obj.Type.Name;
            }

            dto.Status = DescribedEnum<ProductState>.GetDescription(obj.Status);

            return dto;
        }
    }
}