﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Products
{
    public class ProductDtoToProduct : IDomainTranslator<ProductDto, Product>
    {
        private readonly IQueryBuilder _query;
        private readonly ITranslatorBuilder _translator;

        public ProductDtoToProduct(IQueryBuilder queryBuilder, ITranslatorBuilder translator)
        {
            _query = queryBuilder;
            _translator = translator;
        }

        public Product Map(ProductDto dto)
        {
            var obj = new Product();

            obj.InjectFrom(dto);

            obj.EndActivityDate = string.IsNullOrEmpty(dto.EndActivityDate)
                ? null
                : new DateTime?(DateTime.Parse(dto.EndActivityDate));

            if (dto.Images != null)
                obj.Images =
                    _query.For<ICollection<Image>>()
                        .With(new BaseObjectCriterion {Ids = dto.Images.Select(d => d.Id).ToList()})
                        .ToList();

            if (dto.Categories != null)
                obj.Categories =
                    _query.For<ICollection<Category>>()
                        .With(new BaseObjectCriterion {Ids = dto.Categories.Select(c => c.Id).ToList()})
                        .ToList();

            if (dto.Labels != null)
                obj.Labels =
                    _query.For<ICollection<Banner>>()
                        .With(new BaseObjectCriterion {Ids = dto.Labels.Select(c => c.Id).ToList()})
                        .ToList();

            if (dto.Places != null)
                obj.Places =
                    _query.For<ICollection<Place>>()
                        .With(new BaseObjectCriterion {Ids = dto.Places.Keys.ToList()})
                        .ToList();

            if (dto.Featured != null)
                obj.Featured =
                    _query.For<ICollection<Product>>()
                        .With(new BaseObjectCriterion {Ids = dto.Featured.Keys.ToList()})
                        .ToList();

            if (dto.Prices != null)
                dto.Prices.Select(p => _translator.Map<PriceDto, Price>(p)).ToList().ForEach(p =>
                {
                    p.Product = obj;
                    obj.Prices.Add(p);
                });

            if (dto.Fields != null && dto.Fields.Any())
                obj.Fields = dto.Fields.Select(f => _translator.Map<ObjectFieldDto, ObjectField<Product>>(f)).ToList();
            else
                obj.Fields = new List<ObjectField<Product>>();

            if (dto.CategoryOrders != null)
            {
                obj.CategoriesOrders =
                    dto.CategoryOrders.Select(
                        co => _translator.Map<ProductToCategoryOrderDto, ProductToCategoryOrder>(co)).ToList();
               
            }

            obj.Type = _query.For<ICollection<ProductType>>()
             .With(new BaseObjectCriterion { Ids = new[] { dto.TypeId } })
             .FirstOrDefault();

            obj.Status = DescribedEnum<ProductState>.GetValue(dto.Status);

            return obj;
        }
    }
}