﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Products
{
    public class CategoryOrderToDto : IDomainTranslator<ProductToCategoryOrder, ProductToCategoryOrderDto>
    {
        public ProductToCategoryOrderDto Map(ProductToCategoryOrder obj)
        {
            var dto = new ProductToCategoryOrderDto();
            dto.InjectFrom(obj);

            dto.CategoryId = obj.KeyCategory.Id;
            dto.CategoryName = obj.KeyCategory.Name;
            dto.ProductId = obj.SourceProduct.Id;

            return dto;
        }
    }
}