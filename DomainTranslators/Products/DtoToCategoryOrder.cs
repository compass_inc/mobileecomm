﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Products
{
    public class DtoToCategoryOrder : IDomainTranslator<ProductToCategoryOrderDto, ProductToCategoryOrder>
    {
        private readonly IQueryBuilder _query; 
        public DtoToCategoryOrder(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }

        public ProductToCategoryOrder Map(ProductToCategoryOrderDto dto)
        {
            var obj = new ProductToCategoryOrder();
            obj.InjectFrom(dto);

            if (dto.CategoryId > 0)
                obj.KeyCategory =
                    _query.For<ICollection<Category>>()
                        .With(new BaseObjectCriterion {Ids = new[] {dto.CategoryId}})
                        .First();

            if (dto.ProductId > 0)
                obj.SourceProduct =
                    _query.For<ICollection<Product>>()
                        .With(new BaseObjectCriterion {Ids = new[] {dto.ProductId}})
                        .First();

            return obj;
        }
    }
}