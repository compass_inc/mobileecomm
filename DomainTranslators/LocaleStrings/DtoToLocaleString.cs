﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.LocaleStrings
{
    public class DtoToLocaleString : IDomainTranslator<LocaleStringDto, LocaleString>
    {
        private readonly ITranslatorBuilder _translator;
        private readonly IQueryBuilder _query;

        public DtoToLocaleString(ITranslatorBuilder translator, IQueryBuilder queryBuilder)
        {
            _translator = translator;
            _query = queryBuilder;
        }

        public LocaleString Map(LocaleStringDto dto)
        {
            var obj = new LocaleString();
            obj.InjectFrom(dto);

            if (dto.Values != null && dto.Values.Any())
                obj.LocaleValues = dto.Values.Select(lv =>
                {
                    var localValue = _translator.Map<LocaleStringValueDto, LocaleStringValue>(lv);
                    localValue.LocaleString = obj;
                    return localValue;
                }).ToList();
            else
                obj.LocaleValues = new List<LocaleStringValue>();


            obj.Type = _query.For<ICollection<LocaleStringType>>()
               .With(new BaseObjectCriterion { Ids = new[] { dto.TypeId } })
               .FirstOrDefault();

            return obj;
        }
    }
}