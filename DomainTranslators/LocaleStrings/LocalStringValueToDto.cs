﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.LocaleStrings
{
    public class LocalStringValueToDto : IDomainTranslator<LocaleStringValue, LocaleStringValueDto>
    {
        public LocaleStringValueDto Map(LocaleStringValue obj)
        {
            var dto  = new LocaleStringValueDto();
            dto.InjectFrom(obj);
            if (obj.LocaleString != null)
                dto.LocaleStringId = obj.LocaleString.Id;
            return dto;
        }
    }
}