﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.LocaleStrings
{
    public class DtoToLocalStringValue : IDomainTranslator<LocaleStringValueDto, LocaleStringValue>
    {
        public LocaleStringValue Map(LocaleStringValueDto dto)
        {
            var obj = new LocaleStringValue();
            obj.InjectFrom(dto);
            return obj;
        }
    }
}