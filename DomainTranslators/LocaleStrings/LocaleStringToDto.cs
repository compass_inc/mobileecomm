﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.LocaleStrings
{
    public class LocaleStringToDto : IDomainTranslator<LocaleString, LocaleStringDto>
    {
        private readonly ITranslatorBuilder _translator;

        public LocaleStringToDto(ITranslatorBuilder translator)
        {
            _translator = translator;
        }

        public LocaleStringDto Map(LocaleString obj)
        {
            var dto = new LocaleStringDto();
            dto.InjectFrom(obj);

            if (obj.LocaleValues != null && obj.LocaleValues.Any())
                dto.Values =
                    obj.LocaleValues.Select(lv => _translator.Map<LocaleStringValue, LocaleStringValueDto>(lv)).ToList();
            else
                dto.Values = new List<LocaleStringValueDto>();

            if (obj.Type != null)
            {
                dto.TypeId = obj.Type.Id;
                dto.TypeCode = obj.Type.Code;
                dto.TypeName = obj.Type.Name;
            }

            return dto;
        }
    }
}