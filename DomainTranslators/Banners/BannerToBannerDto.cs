﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Banners
{
    public class BannerToBannerDto : IDomainTranslator<Banner, BannerDto>
    {
        public BannerDto Map(Banner obj)
        {
            var dto = new BannerDto();
            dto.InjectFrom(obj);

            dto.Images = obj.Images != null
                ? obj.Images.ToDictionary(o => o.Id, o => o.Path)
                : new Dictionary<ulong, string>();

            if (obj.Type != null)
            {
                dto.TypeId = obj.Type.Id;
                dto.TypeCode = obj.Type.Code;
                dto.TypeName = obj.Type.Name;
            }

            return dto;
        }
    }
}