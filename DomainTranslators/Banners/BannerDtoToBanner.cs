﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Banners
{
    public class BannerDtoToBanner : IDomainTranslator<BannerDto, Banner>
    {
                private readonly IQueryBuilder _query;

        public BannerDtoToBanner(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }

        public Banner Map(BannerDto dto)
        {
            var ban = new Banner();
            ban.InjectFrom(dto);

            ban.Images = _query.For<ICollection<Image>>()
                .With(new BaseObjectCriterion { Ids = dto.Images.Keys.ToArray() })
                .ToList();

            ban.Type = _query.For<ICollection<BannerType>>()
                .With(new BaseObjectCriterion { Ids = new[] { dto.TypeId } })
                .FirstOrDefault();

            return ban;
        }
    }
}