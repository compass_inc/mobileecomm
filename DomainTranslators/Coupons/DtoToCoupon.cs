﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Coupons
{
    public class DtoToCoupon : IDomainTranslator<CouponDto, Coupon>
    {
        private readonly IQueryBuilder _query;

        public DtoToCoupon(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }

        public Coupon Map(CouponDto dto)
        {
            var obj = new Coupon();
            obj.InjectFrom(dto);

            if (dto.Products != null && dto.Products.Any())
                obj.Products = _query.For<ICollection<Product>>()
                    .With(new BaseObjectCriterion {Ids = dto.Products.Keys});
            else
                obj.Products = new List<Product>();

            obj.Status = DescribedEnum<CouponState>.GetValue(dto.StatusName);

            return obj;
        }
    }
}