﻿using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;

namespace DomainTranslators.Coupons
{
    public class CouponToDto : IDomainTranslator<Coupon,CouponDto>
    {
        public CouponDto Map(Coupon obj)
        {
            var dto = new CouponDto();
            dto.InjectFrom(obj);

            if (obj.Products != null && obj.Products.Any())
                dto.Products = obj.Products.ToDictionary(p => p.Id, p => p.Name);

            dto.StatusName = DescribedEnum<CouponState>.GetDescription(obj.Status);

            return dto;
        }
    }
}