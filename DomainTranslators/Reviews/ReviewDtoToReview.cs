﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Reviews
{
    public class ReviewDtoToReview : IDomainTranslator<ReviewDto, Review>
    {
        private IQueryBuilder _query;

        public ReviewDtoToReview(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }

        public Review Map(ReviewDto dto)
        {
            var obj = new Review();
            obj.InjectFrom(dto);

            obj.Status = !string.IsNullOrEmpty(dto.Status)
                ? DescribedEnum<ReviewState>.GetValue(dto.Status)
                : ReviewState.Moderate;

            if (dto.ProductId > 0)
                obj.Product =
                    _query.For<ICollection<Product>>()
                        .With(new BaseObjectCriterion {Ids = new[] {dto.ProductId}})
                        .First();

            if (dto.CustomerId > 0)
            {
                obj.Customer =
                    _query.For<ICollection<Customer>>()
                        .With(new BaseObjectCriterion {Ids = new[] {dto.CustomerId}})
                        .First();
                obj.CustomerName = obj.Customer.Name;
                obj.CustomerEMail = obj.Customer.Login;
            }

            if (dto.ParentId > 0)
                obj.Parent =
                    _query.For<ICollection<Review>>().With(new BaseObjectCriterion {Ids = new[] {dto.ParentId}}).First();

            if (dto.PlaceId > 0)
                obj.Place =
                    _query.For<ICollection<Place>>().With(new BaseObjectCriterion {Ids = new[] {dto.PlaceId}}).First();

            obj.CreationDate = dto.CreationDate == null ? DateTime.Now : DateTime.Parse(dto.CreationDate);

            return obj;
        }
    }
}