﻿using System.Globalization;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;

namespace DomainTranslators.Reviews
{
    public class ReviewToReviewDto : IDomainTranslator<Review, ReviewDto>
    {
        public ReviewDto Map(Review obj)
        {
            var dto = new ReviewDto();
            dto.InjectFrom(obj);

            if (obj.Customer != null)
            {
                dto.CustomerId = obj.Customer.Id;
                if (obj.Customer.Avatar != null)
                    dto.CustomerAvatarPath = obj.Customer.Avatar.Path;
            }

            if (obj.Product != null)
            {
                dto.ProductId = obj.Product.Id;
                dto.ProductName = obj.Product.Name;
            }

            if (obj.Parent != null)
            {
                dto.ParentId = obj.Parent.Id;
                dto.ParentTitle = obj.Parent.Title;
            }

            if (obj.Place != null)
            {
                dto.PlaceId = obj.Place.Id;
                dto.PlaceName = obj.Place.Name;
            }

            dto.Status = DescribedEnum<ReviewState>.GetDescription(obj.Status);

            dto.CreationDate = obj.CreationDate.ToString(CultureInfo.InvariantCulture);


            return dto;
        }
    }
}