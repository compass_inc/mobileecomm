﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Category.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Categories
{
    public class CategoryDtoToCategory : IDomainTranslator<CategoryDto, Category>
    {
        private readonly IQueryBuilder _query;
        public CategoryDtoToCategory(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }
        public Category Map(CategoryDto dto)
        {
            var cat = dto.Id > 0
                ? _query.For<ICollection<Category>>()
                    .With(new CategoriesCriterion {Ids = new[] {dto.Id}})
                    .FirstOrDefault()
                : new Category();

            cat.InjectFrom(dto);

            cat.Parent =
                _query.For<ICollection<Category>>()
                    .With(new CategoriesCriterion {Ids = new[] {dto.ParentId ?? 0}})
                    .FirstOrDefault();

            cat.Image =
                _query.For<ICollection<Image>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.ImageId}})
                    .FirstOrDefault();

            return cat;
        }
    }
}