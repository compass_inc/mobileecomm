﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Categories
{
    public class CategoryToCategoryDto : IDomainTranslator<Category,CategoryDto>
    {
        public CategoryDto Map(Category obj)
        {
            var dto = new CategoryDto();
            dto.InjectFrom(obj);

            if (obj.Image != null)
            {
                dto.ImagePath = obj.Image.Path;
                dto.ImageId = obj.Image.Id;
            }
            else
                dto.ImagePath = string.Empty;;

            dto.ParentId = obj.Parent == null ? null : new ulong?(obj.Parent.Id);

            return dto;
        }
    }
}