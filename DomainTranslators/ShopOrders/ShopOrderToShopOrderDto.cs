﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.ShopOrders
{
    public class ShopOrderToShopOrderDto : IDomainTranslator<ShopOrder, ShopOrderDto>
    {
        private readonly ITranslatorBuilder _translator;
        private readonly IQueryBuilder _query;

        public ShopOrderToShopOrderDto(ITranslatorBuilder translator, IQueryBuilder query)
        {
            _translator = translator;
            _query = query;
        }

        public ShopOrderDto Map(ShopOrder obj)
        {
            if (obj.Id == 0 && obj.Lines == null)
                obj.Fields =
                    _query.For<ICollection<ObjectField<ShopOrder>>>()
                        .With(new GenerateObjectFieldsCriterion<ShopOrder>());

            var dto = new ShopOrderDto();
            dto.InjectFrom(obj);

            if (obj.Customer != null)
                dto.CustomerId = obj.Customer.Id;

            dto.CreationDate = obj.CreationDate.ToString();

            if (obj.DeliveryTime != null)
                dto.DeliveryTime = obj.DeliveryTime.Value.ToShortTimeString();

            if (obj.Status != null)
            {
                dto.StatusId = obj.Status.Id;
                dto.StatusName = obj.Status.Name;
            }

            if (obj.Lines != null && obj.Lines.Any())
                dto.Lines = obj.Lines.Select(l => _translator.Map<ShopOrderLine, ShopOrderLineDto>(l)).ToList();

            if (obj.Fields != null)
                dto.Fields =
                    obj.Fields
                        .Select(o => _translator.Map<ObjectField<ShopOrder>, ObjectFieldDto>(o))
                        .ToList();

            if (obj.BonusProgram != null)
            {
                dto.BonusProgramId = obj.BonusProgram.Id;
                dto.BonusProgramName = obj.BonusProgram.Name;
            }

            if (obj.Coupon != null)
            {
                dto.CouponId = obj.Coupon.Id;
                dto.CouponCode = obj.Coupon.Code;
            }
            return dto;
        }
    }
}