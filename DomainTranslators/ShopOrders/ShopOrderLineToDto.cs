﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.ShopOrders
{
    public class ShopOrderLineToDto: IDomainTranslator<ShopOrderLine, ShopOrderLineDto>
    {
        public ShopOrderLineDto Map(ShopOrderLine obj)
        {
            var dto = new ShopOrderLineDto();
            
            dto.InjectFrom(obj);

            if (obj.Product != null)
                dto.ProductId = obj.Product.Id;

            if (obj.ProductPrice != null)
                dto.ProductPriceId = obj.ProductPrice.Id;

            dto.ShopOrderId = obj.ShopOrder.Id;

            return dto;
        }
    }
}