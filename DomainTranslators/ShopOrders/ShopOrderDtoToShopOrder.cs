﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.ShopOrders
{
    public class ShopOrderDtoToShopOrder : IDomainTranslator<ShopOrderDto, ShopOrder>
    {
        private readonly ITranslatorBuilder _translator;
        private readonly IQueryBuilder _query;

        public ShopOrderDtoToShopOrder(ITranslatorBuilder builder, IQueryBuilder queryBuilder)
        {
            _translator = builder;
            _query = queryBuilder;
        }

        public ShopOrder Map(ShopOrderDto dto)
        {
            var obj = new ShopOrder();

            obj.InjectFrom(dto);

            if (!string.IsNullOrEmpty(dto.CreationDate) )
                obj.CreationDate = DateTime.Parse(dto.CreationDate);

            if(!string.IsNullOrEmpty(dto.DeliveryTime))
                obj.DeliveryTime = DateTime.Parse(dto.DeliveryTime);

            obj.Customer =
                _query.For<ICollection<Customer>>()
                    .With(new CustomersCriterion {Ids = new[] {dto.CustomerId}})
                    .FirstOrDefault();
            
            if (obj.Customer != null)
            {
                if (string.IsNullOrEmpty(obj.CustomerMail))
                    obj.CustomerMail = obj.Customer.Login;

                if (string.IsNullOrEmpty(obj.CustomerName))
                    obj.CustomerName = obj.Customer.Name;

                if (string.IsNullOrEmpty(obj.CustomerPhone))
                    obj.CustomerPhone = obj.Customer.Phone;
            }

            obj.Status =
                _query.For<ICollection<ShopOrderState>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.StatusId}})
                    .FirstOrDefault();

            if (dto.Lines!=null && dto.Lines.Any())
                obj.Lines = dto.Lines.Select(l => _translator.Map<ShopOrderLineDto, ShopOrderLine>(l)).ToList();

            if (obj.OrderSum == 0 && dto.Lines != null && dto.Lines.Any())
                obj.OrderSum = obj.Lines.Sum(l => l.LineSum);

            if (dto.Fields != null && dto.Fields.Any())
                obj.Fields = dto.Fields.Select(f => _translator.Map<ObjectFieldDto, ObjectField<ShopOrder>>(f)).ToList();

                obj.BonusProgram =
                    _query.For<ICollection<BonusProgram>>()
                        .With(new BaseObjectCriterion() {Ids = new[] {dto.BonusProgramId}})
                        .FirstOrDefault();

            return obj;
        }
    }
}