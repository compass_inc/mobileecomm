﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.ShopOrders
{
    public class DtoToShopOrderLine : IDomainTranslator<ShopOrderLineDto, ShopOrderLine>
    {
        private IQueryBuilder _query;
        
        public DtoToShopOrderLine(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }

        public ShopOrderLine Map(ShopOrderLineDto dto)
        {
            var obj = new ShopOrderLine();
            
            obj.InjectFrom(dto);

            obj.ShopOrder =
                _query.For<ICollection<ShopOrder>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.ShopOrderId}})
                    .FirstOrDefault();

            obj.Product =
                _query.For<ICollection<Product>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.ProductId}})
                    .FirstOrDefault();
            if (obj.Product != null)
                obj.ProductName = obj.Product.Name;

            obj.ProductPrice =
                _query.For<ICollection<Price>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.ProductPriceId}})
                    .FirstOrDefault();

            if (obj.ProductPrice != null)
            {
                obj.ProductPriceDescription = obj.ProductPrice.Description;
                obj.ProductPriceValue = obj.ProductPrice.Value2 > 0 ? obj.ProductPrice.Value2 : obj.ProductPrice.Value;
            }

            return obj;
        }
    }
}