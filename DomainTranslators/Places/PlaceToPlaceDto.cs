﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Utils;

namespace DomainTranslators.Places
{
    public class PlaceToPlaceDto : IDomainTranslator<Place,PlaceDto>
    {
        public PlaceDto Map(Place obj)
        {
            var dto = new PlaceDto();
            dto.InjectFrom(obj);
            if (!string.IsNullOrEmpty(dto.City))
                dto.CityTranslit = Transliteration.Front(dto.City);
            dto.DeliveryStartTime = obj.DeliveryStartTime.ToShortTimeString();
            dto.DeliveryEndTime = obj.DeliveryEndTime.ToShortTimeString();
            dto.WorkStartTime = obj.WorkStartTime.ToShortTimeString();
            dto.WorkEndTime = obj.WorkEndTime.ToShortTimeString();

            if (obj.HeaderLogo != null)
            {
                dto.HeaderLogoId = obj.HeaderLogo.Id;
                dto.HeaderLogoPath = obj.HeaderLogo.Path;
            }

            if (obj.MainLogo != null)
            {
                dto.MainLogoId = obj.MainLogo.Id;
                dto.MainLogoPath = obj.MainLogo.Path;
            }

            return dto;
        }
    }
}