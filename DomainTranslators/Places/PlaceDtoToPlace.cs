﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Places
{
    public class PlaceDtoToPlace : IDomainTranslator<PlaceDto, Place>
    {
         private readonly IQueryBuilder _query;
         public PlaceDtoToPlace(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }

        public Place Map(PlaceDto dto)
        {
            var place = dto.Id > 0
                ? _query.For<ICollection<Place>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.Id}})
                    .FirstOrDefault()
                : new Place();

            place.InjectFrom(dto);
            place.DeliveryStartTime = DateTime.Parse(dto.DeliveryStartTime);
            place.DeliveryEndTime = DateTime.Parse(dto.DeliveryEndTime);
            place.WorkStartTime = DateTime.Parse(dto.WorkStartTime);
            place.WorkEndTime = DateTime.Parse(dto.WorkEndTime);

            place.HeaderLogo =
                _query.For<ICollection<Image>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.HeaderLogoId}})
                    .FirstOrDefault();

            place.MainLogo =
                _query.For<ICollection<Image>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.MainLogoId}})
                    .FirstOrDefault();

            return place;
        }
    }
}