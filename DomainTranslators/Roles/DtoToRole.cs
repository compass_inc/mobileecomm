﻿using System.Collections.Generic;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Roles
{
    public class DtoToRole : IDomainTranslator< RoleDto, Role>
    {
         private readonly IQueryBuilder _query;

        public DtoToRole(IQueryBuilder query)
        {
            _query = query;
        }

        public Role Map(RoleDto dto)
        {
            var obj = new Role();
            obj.InjectFrom(dto);

            if (dto.UsersIds != null)
                obj.Users = _query.For<ICollection<User>>().With(new BaseObjectCriterion {Ids = dto.UsersIds});

            return obj;
        }
    }
}