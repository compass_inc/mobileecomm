﻿using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Roles
{
    public class RoleToDto : IDomainTranslator<Role, RoleDto>
    {
        public RoleDto Map(Role obj)
        {
            var dto = new RoleDto();
            dto.InjectFrom(obj);

            if (obj.Users != null)
                dto.UsersIds = obj.Users.Select(u =>u.Id).ToList();

            return dto;
        }
    }
}