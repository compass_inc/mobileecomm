﻿using Domain;

namespace DomainTranslators
{
    public interface ITranslatorFactory
    {
        IDomainTranslator<TSource, TResult> Create<TSource, TResult>()
            where TSource : BaseObject
            where TResult : BaseObject;
    }
}