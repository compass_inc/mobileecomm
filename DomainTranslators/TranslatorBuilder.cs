﻿using Domain;

namespace DomainTranslators
{
    public class TranslatorBuilder : ITranslatorBuilder
    {
        private readonly ITranslatorFactory _factory;

        public TranslatorBuilder(ITranslatorFactory translatorFactory)
        {
            _factory = translatorFactory;
        }


        public TResult Map<TSource, TResult>(TSource obj) where TSource : BaseObject where TResult : BaseObject
        {
            if (typeof (TSource) == typeof (TResult))
                return obj as TResult;
            return _factory.Create<TSource, TResult>().Map(obj);
        }
    }
}