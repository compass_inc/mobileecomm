﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.ObjectFields
{
    public class ObjectFieldToDto<T> : IDomainTranslator<ObjectField<T>,ObjectFieldDto> where T: BaseObject
    {
        public ObjectFieldDto Map(ObjectField<T> obj)
        {
            var dto = new ObjectFieldDto();
            dto.InjectFrom(obj);

            dto.FieldTypeId = obj.FieldType.Id;
            dto.FieldTypeName = obj.FieldType.TypeName;
            dto.SortOrder = obj.FieldType.SortOrder;

            dto.FieldTypeDescription = obj.FieldType.Name;

            dto.IsRequired = obj.FieldType.IsRequired;

            return dto;
        }

    }

    public class ShopOrderObjectFieldToDto : ObjectFieldToDto<ShopOrder>
    {
    }
    public class ProductObjectFieldToDto : ObjectFieldToDto<Product>
    {
    }
}