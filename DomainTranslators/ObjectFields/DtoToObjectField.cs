﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.ObjectFields
{
    public class DtoToObjectField<T> : IDomainTranslator<ObjectFieldDto, ObjectField<T>> where T : BaseObject
    {
        private IQueryBuilder _query;

        public DtoToObjectField(IQueryBuilder query)
        {
            _query = query;
        }

        public ObjectField<T> Map(ObjectFieldDto dto)
        {
            var obj = new ObjectField<T>();
            obj.InjectFrom(dto);

            obj.FieldType =
                _query.For<ICollection<ObjectFieldType<T>>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.FieldTypeId}})
                    .FirstOrDefault();

            return obj;
        }
    }

     public class DtoToShopOrderObjectField: DtoToObjectField<ShopOrder>
     {
         public DtoToShopOrderObjectField(IQueryBuilder query)
             : base(query)
         {
         }
     }

     public class DtoToProductObjectField : DtoToObjectField<Product>
     {
         public DtoToProductObjectField(IQueryBuilder query)
             : base(query)
         {
         }
     }
}