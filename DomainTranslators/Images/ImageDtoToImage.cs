﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Images
{
    public class ImageDtoToImage : IDomainTranslator<ImageDto, Image>
    {
        private readonly IQueryBuilder _query;
        public ImageDtoToImage(IQueryBuilder query)
        {
            _query = query;
        }

        public Image Map(ImageDto dto)
        {
            var obj = new Image();
            obj.InjectFrom(dto);

            obj.Type =
                _query.For<ICollection<ImageType>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.TypeId}})
                    .FirstOrDefault();

            return obj;
        }
    }
}