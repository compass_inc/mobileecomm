﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Images
{
    public class ImageToImageDto : IDomainTranslator<Image, ImageDto>
    {
        public ImageDto Map(Image obj)
        {
            var dto = new ImageDto();
            dto.InjectFrom(obj);

            if (obj.Type != null)
            {
                dto.TypeId = obj.Type.Id;
                dto.TypeName = obj.Type.Name;
            }

            return dto;
        }
    }
}