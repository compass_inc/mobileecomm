﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Prices
{
    public class PriceToPriceDto : IDomainTranslator<Price,PriceDto>
    {
        public PriceDto Map(Price obj)
        {
            var dto = new PriceDto();
            dto.InjectFrom(obj);

            if (obj.Product != null)
                dto.ProductId = obj.Product.Id;

            return dto;
        }
    }
}