﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Prices
{
    public class PriceDtoToPrice : IDomainTranslator<PriceDto,Price>
    {
        private readonly IQueryBuilder _query;
        public PriceDtoToPrice(IQueryBuilder queryBuilder)
        {
            _query = queryBuilder;
        }
        public Price Map(PriceDto dto)
        {
            var obj = new Price();
            obj.InjectFrom(dto);

            obj.Product =
                _query.For<ICollection<Product>>()
                    .With(new BaseObjectCriterion {Ids = new List<ulong> {dto.ProductId}})
                    .FirstOrDefault();

            return obj;
        }
    }
}