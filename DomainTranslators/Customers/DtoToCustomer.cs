﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Customer.Criteria;
using Services.Infrastructure.Interfaces;
using Services.Product.Criteria;

namespace DomainTranslators.Customers
{
    public class DtoToCustomer : IDomainTranslator<CustomerDto, Customer>
    {
        private readonly IQueryBuilder _query;
        private readonly ITranslatorBuilder _translator;

        public DtoToCustomer(IQueryBuilder query, ITranslatorBuilder translator )
        {
            _query = query;
            _translator = translator;
        }

        public Customer Map(CustomerDto dto)
        {
            var obj = new Customer();
            obj.InjectFrom(dto);

            if (obj.Id == 0)
                obj.Status = CustomerState.Active;

            obj.Login = dto.Email;

            obj.Avatar =
                _query.For<ICollection<Image>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.AvatarImageId}})
                    .FirstOrDefault();

            if (dto.Addresses != null && dto.Addresses.Any())
                obj.Addresses =
                    dto.Addresses.Select(a =>
                    {
                        var address = _translator.Map<CustomerAddressDto, CustomerAddress>(a);
                        address.Customer = obj;
                        return address;
                    }).ToList();
            else
                obj.Addresses = new List<CustomerAddress>();

            if (!string.IsNullOrEmpty(dto.Status))
                obj.Status = DescribedEnum<CustomerState>.GetValue(dto.Status);

            if (dto.FavoritesIds != null && dto.FavoritesIds.Any())
                obj.Favorites =
                    _query.For<ICollection<Product>>()
                        .With(new ProductsCriterion {Ids = dto.FavoritesIds, CategoryId = new List<ulong?>{0} });

            return obj;
        }
    }
}