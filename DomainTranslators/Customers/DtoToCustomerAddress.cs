﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Customers
{
    public class DtoToCustomerAddress : IDomainTranslator<CustomerAddressDto, CustomerAddress>
    {
        public CustomerAddress Map(CustomerAddressDto dto)
        {
            return new CustomerAddress().InjectFrom(dto) as CustomerAddress;
        }
    }
}