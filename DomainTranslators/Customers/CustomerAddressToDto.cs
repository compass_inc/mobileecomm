﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Customers
{
    public class CustomerAddressToDto : IDomainTranslator<CustomerAddress, CustomerAddressDto>
    {
        public CustomerAddressDto Map(CustomerAddress obj)
        {
            return new CustomerAddressDto().InjectFrom(obj) as CustomerAddressDto;
        }
    }
}