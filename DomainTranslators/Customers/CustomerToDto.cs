﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Domain.Enums;
using Omu.ValueInjecter;
using Services.BonusProgram.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Customers
{
    public class CustomerToDto : IDomainTranslator<Customer, CustomerDto>
    {
        private readonly ITranslatorBuilder _translator;
        private readonly IQueryBuilder _query;

        public CustomerToDto(ITranslatorBuilder translator, IQueryBuilder query)
        {
            _translator = translator;
            _query = query;
        }
        public CustomerDto Map(Customer obj)
        {
            var dto = new CustomerDto();
            dto.InjectFrom(obj);

            dto.Status = DescribedEnum<CustomerState>.GetDescription(obj.Status);

            if (obj.Avatar != null)
            {
                dto.AvatarImageId = obj.Avatar.Id;
                dto.AvatarImagePath = obj.Avatar.Path;
            }
            else
                dto.AvatarImagePath = string.Empty;

            if (obj.Addresses != null && obj.Addresses.Any())
                dto.Addresses =
                    obj.Addresses.Select(a => _translator.Map<CustomerAddress, CustomerAddressDto>(a)).ToList();

            if (obj.Favorites != null && obj.Favorites.Any())
                dto.FavoritesIds = obj.Favorites.Select(f => f.Id).ToList();

            if (obj.Id > 0)
                dto.BonusSums =
                    _query.For<ICollection<CustomerBonus>>()
                        .With(new CustomerBonusCriterion {CustomerId = obj.Id})
                        .ToDictionary(bc => bc.Program.Name, bc => bc.Sum);

            return dto;
        }
    }
}