﻿using Domain;

namespace DomainTranslators
{
    public interface ITranslatorBuilder
    {
        TResult Map<TSource, TResult>(TSource obj) where TSource : BaseObject where TResult : BaseObject;
    }
}