﻿using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Settings
{
    public class SettingsToDto : IDomainTranslator<Domain.Settings, SettingsDto>
    {
        private readonly ITranslatorBuilder _translator;

        public SettingsToDto(ITranslatorBuilder translator)
        {
            _translator = translator;
        }

        public SettingsDto Map(Domain.Settings obj)
        {
            var dto = new SettingsDto();
            dto.InjectFrom(obj);

            if (obj.LogoImage != null)
            {
                dto.LogoImageId = obj.LogoImage.Id;
                dto.LogoImagePath = obj.LogoImage.Path;
            }
            else
                dto.LogoImagePath = string.Empty;

            if (obj.ReviewDefaultImage != null)
            {
                dto.ReviewDefaultImageId = obj.ReviewDefaultImage.Id;
                dto.ReviewDefaultImagePath = obj.ReviewDefaultImage.Path;
            }
            else
                dto.ReviewDefaultImagePath = string.Empty;

            if (obj.CategoriesColors != null && obj.CategoriesColors.Any())
                dto.CategoriesColors =
                    obj.CategoriesColors.Select(cc => _translator.Map<CategoryColor, CategoryColorDto>(cc)).ToList();

            return dto;
        }
    }
}