﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Settings
{
    public class DtoToSettings : IDomainTranslator<SettingsDto, Domain.Settings>
    {
         private readonly ITranslatorBuilder _translator;
         private readonly IQueryBuilder _query;

         public DtoToSettings(ITranslatorBuilder translator,IQueryBuilder query)
        {
            _translator = translator;
             _query = query;
        }

        public Domain.Settings Map(SettingsDto dto)
        {
            var obj = new Domain.Settings();
            obj.InjectFrom(dto);

            obj.LogoImage =
                _query.For<ICollection<Image>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.LogoImageId}})
                    .FirstOrDefault();

            obj.ReviewDefaultImage =
                _query.For<ICollection<Image>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.ReviewDefaultImageId}})
                    .FirstOrDefault();

            obj.CategoriesColors = dto.CategoriesColors != null
                ? dto.CategoriesColors.Select(cc => _translator.Map<CategoryColorDto, CategoryColor>(cc)).ToList()
                : new List<CategoryColor>();

            return obj;
        }
    }
}