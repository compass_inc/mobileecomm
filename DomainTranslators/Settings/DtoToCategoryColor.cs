﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Dto;
using Omu.ValueInjecter;
using Services.Base.Criteria;
using Services.Infrastructure.Interfaces;

namespace DomainTranslators.Settings
{
    public class DtoToCategoryColor : IDomainTranslator<CategoryColorDto, CategoryColor>
    {
        private readonly IQueryBuilder _query;

        public DtoToCategoryColor(IQueryBuilder query)
        {
            _query = query;
        }

        public CategoryColor Map(CategoryColorDto dto)
        {
            var obj = new CategoryColor();
            obj.InjectFrom(dto);

            obj.Category =
                _query.For<ICollection<Category>>()
                    .With(new BaseObjectCriterion {Ids = new[] {dto.CategoryId}})
                    .FirstOrDefault();

            return obj;
        }
    }
}