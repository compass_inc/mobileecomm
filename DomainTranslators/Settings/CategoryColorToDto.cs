﻿using Domain;
using Domain.Dto;
using Omu.ValueInjecter;

namespace DomainTranslators.Settings
{
    public class CategoryColorToDto : IDomainTranslator<CategoryColor, CategoryColorDto>
    {
        public CategoryColorDto Map(CategoryColor obj)
        {
            var dto = new CategoryColorDto();
            dto.InjectFrom(obj);

            dto.CategoryId = obj.Category.Id;
            dto.CategoryName = obj.Category.Name;

            return dto;
        }
    }
}