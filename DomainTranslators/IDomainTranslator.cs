﻿using Domain;

namespace DomainTranslators
{
    public interface IDomainTranslator<in TSource, out TResult>
        where TSource : BaseObject
        where TResult : BaseObject 
    {
        TResult Map(TSource obj);
    }
}